﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class EntCorrida
    {
        public string Id { get; set; }
        public string IdUser { get; set; }
        public string Id_Sondaje { get; set; }
        public double Desde { get; set; } // 2 decimales
        public double Hasta { get; set; } // 2 decimales
        public double Perforacion { get; set; } // 2 decimales
        public double Recuperacion { get; set; } // 2 decimales
        public double Rqd { get; set; }
        public double Lrf { get; set; }
        public int Nfn { get; set; }
        public string Litologia_1 { get; set; }
        public string Litologia_2 { get; set; }
        public string Re { get; set; }
        public string TipoEstruct_1 { get; set; }
        public string TipoEstruct_2 { get; set; }
        public string Jn { get; set; }
        public string Jr { get; set; }
        public string Ja { get; set; }
        public string Jw { get; set; }
        public string Srf { get; set; }
        public double Q_Barton { get; set; }
        public string Geotecnico { get; set; }
        public string Comentario { get; set; }
        public double Rmr { get; set; }
        public double Adicional { get; set; }
        public string State { get; set; }
        public int Validar { get; set; }
    }
}
