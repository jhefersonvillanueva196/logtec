﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class DatosGlobales
    {
        public static string Id_Usuario { get; set; }
        public static string Id_Sondaje { get; set; }
        public static string Id_Corrida { get; set; }
        public static string Id_Estructura { get; set; }
        public static string Id_EstructuraTemp { get; set; }
        public static string Iniciales { get; set; }
        public static double ProUltCorrida { get; set; }
        public static double ProCorrida { get; set; }
        public static double Profundidad { get; set; }
        public static double newMin { get; set; }
        public static double Max { get; set; }
        public static double Min { get; set; }
        public static string Nom_sondaje { get; set; }
        public static string Rol { get; set; }
        public static int num_Estructura { get; set; }
        public static double Dip { get; set; }
        public static double Azimut { get; set; }
        public static double NewDip { get; set; }
        public static double NewAzimut { get; set; }
        public static double Alpha { get; set; }
        public static double Beta { get; set; }
    }
}
