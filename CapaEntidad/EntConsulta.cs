﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class EntConsulta
    {
        public string Nom_sondaje { get; set; }
        public DateTime Fecha { get; set; }
        public string Linea_Core { get; set; }
        public double Dip { get; set; }
        public double Azimut { get; set; }
        public double Desde { get; set; } 
        public double Hasta { get; set; } 
        public string Num_estructura { get; set; }
        public double Profundidad_e { get; set; } 
        public string Tipo_e { get; set; }
        public double T_B { get; set; }
        public double Alpha { get; set; }
        public double Beta { get; set; } 
        public double Ref_to_top { get; set; } 
        public double Cond_fractura { get; set; }
        public double Jrc { get; set; }
        public double Jr { get; set; }
        public double Ja { get; set; }
        public double Espesor { get; set; }
        public string Relleno_1 { get; set; }
        public string Relleno_2 { get; set; }
        public string Comentario { get; set; }
        public string Geotecnico { get; set; } 
        public DateTime FechaEs { get; set; }
        public double Dip_cal { get; set; }
        public double Dip_dir_cal { get; set; }
        public double Dips { get; set; }
        public double Strike { get; set; }
    }
}
