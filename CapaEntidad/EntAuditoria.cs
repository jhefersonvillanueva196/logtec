﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class EntAuditoria
    {
        public string Id { get; set; }
        public string Id_User { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Elemento { get; set; }
        public string Accion { get; set; }
        public string Fecha { get; set; }
    }
}
