﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class EntSondaje
    {
        public string Id { get; set; }
        public string Id_User { get; set; }
        public string Cliente { get; set; }
        public string Proyecto { get; set; }
        public string Geotecnico { get; set; }
        public string Nom_sondaje { get; set; }
        public string Fecha { get; set; } // null
        public double Profundidad { get; set; } // null, 2 decimales
        public double Norte { get; set; } // 3 decimales
        public double Este { get; set; } // 3 decimales
        public double Cota { get; set; } // 3 decimales
        public string Maquina { get; set; } // null
        public string Linea_Core { get; set; } // cbo, recordarle a Christian, falta agregar
        public double Dip { get; set; } // null
        public double Azimut { get; set; } // null
        public string State { get; set; }
        public int Validar { get; set; }
    }
}
