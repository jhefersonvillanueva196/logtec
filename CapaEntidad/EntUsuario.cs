﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class EntUsuario
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Dni { get; set; }
        public string Cargo { get; set; }
        public string Iniciales { get; set; }
        public string Contrasenia { get; set; }
        public string Rol { get; set; }
    }
}
