﻿using CapaDato;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class LogAuditoria
    {
        private List<EntAuditoria> lista = new List<EntAuditoria>();
        private DbProyect db = new DbProyect();

        private static readonly LogAuditoria _instancia = new LogAuditoria();
        public static LogAuditoria Instancia
        {
            get { return LogAuditoria._instancia; }
        }

        public List<EntAuditoria> ListarAuditorias()
        {
            lista = db.GetAuditorias();
            return lista;
        }

        public void InsertarAuditoria(EntAuditoria aud)
        {
            try
            {
                aud.Id = ReturnId();
                db.InsertAuditoria(aud);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private string ReturnId()
        {
            string id = "";
            List<string> list = new List<string>();
            var lista = db.GetAuditorias();           
            if (lista.Count() <= 0)
                id = "A" + DatosGlobales.Iniciales + "-1";
            else
            {
                var temp = lista.Where(t => t.Id_User == DatosGlobales.Id_Usuario);
                foreach (EntAuditoria auditoria in temp)
                {
                    list.Add(auditoria.Id);
                }
                id = "A" + HelpIdData.NewIdString(DatosGlobales.Iniciales, list);
            }
            return id;
        }
    }
}
