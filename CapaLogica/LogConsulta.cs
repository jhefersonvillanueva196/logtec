﻿using CapaDato;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class LogConsulta
    {
        private DbProyect db = new DbProyect();

        private static readonly LogConsulta _instancia = new LogConsulta();
        public static LogConsulta Instancia
        {
            get { return LogConsulta._instancia; }
        }

        public List<EntConsulta> ListarConsultas()
        {
            List<EntConsulta> lista = new List<EntConsulta>();
            lista = db.GetConsultas();
            return lista;
        }

        public List<EntConsulta> ConsultarFecha(DateTime fechaI, DateTime fechaF)
        {
            List<EntConsulta> lista = new List<EntConsulta>();
            lista = db.GetConsultas();
            var listan = lista.Where(c => c.FechaEs >= fechaI && c.FechaEs <= fechaF).ToList();
            return listan;
        }

        public List<EntConsulta> ConsultarTramo(double tramoI, double tramoF)
        {
            List<EntConsulta> lista = new List<EntConsulta>();
            lista = db.GetConsultas();
            var listan = lista.Where(c => c.Profundidad_e >= tramoI && c.Profundidad_e <= tramoF).ToList();
            return listan;
        }

        public List<EntConsulta> ConsultarFechaTramo(DateTime fechaI, DateTime fechaF, double tramoI, double tramoF)
        {
            List<EntConsulta> lista = new List<EntConsulta>();
            lista = db.GetConsultas();
            var listan = lista.Where(c => c.Profundidad_e >= tramoI && c.Profundidad_e <= tramoF && c.FechaEs >= fechaI && c.FechaEs <= fechaF).ToList();
            return listan;
        }
    }
}
