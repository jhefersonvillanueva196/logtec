﻿using CapaDato;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class LogUsuario
    {
        private static readonly LogUsuario _instancia = new LogUsuario();
        private DbProyect db = new DbProyect();

        public static LogUsuario Instancia
        {
            get { return LogUsuario._instancia; }
        }

        public List<EntUsuario> ListarUsuario()
        {
            var lista = db.GetUsuario();
            return lista;
        }

        public bool Verifica(string dni, string contrasenia)
        {
            var lista = db.GetUsuario();
            var verificar = lista.FirstOrDefault(D => D.Dni == dni && D.Contrasenia == contrasenia);
            if (verificar != null)
            {
                DatosGlobales.Id_Usuario = verificar.Id;
                DatosGlobales.Iniciales = verificar.Iniciales;
                DatosGlobales.Rol = verificar.Rol;
                return true;
            }
            return false;
        }

        public void EditarUsuario(EntUsuario es)
        {
            try
            {
                db.UpdateUsuario(es);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public EntUsuario Usuario()
        {
            var lista = db.GetUsuario();
            var usuario = lista.FirstOrDefault(D => D.Id == DatosGlobales.Id_Usuario);
            return usuario;
        }
    }
}
