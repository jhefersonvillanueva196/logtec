﻿using CapaDato;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class LogCorrida
    {
        private List<EntCorrida> lista = new List<EntCorrida>();
        private DbProyect db = new DbProyect();

        private static readonly LogCorrida _instancia = new LogCorrida();
        public static LogCorrida Instancia
        {
            get { return LogCorrida._instancia; }
        }

        public List<EntCorrida> ListarCorridas()
        {
            lista = db.GetCorridasOrderByDesde();
            string numero = DatosGlobales.Id_Sondaje;
            var listn = lista.Where(C => C.Id_Sondaje == numero).ToList();
            return listn;
        }

        public List<EntCorrida> ListarCorridasByIdSondaje(string idSondaje)
        {
            lista = db.GetCorridasOrderByDesde();
            var listn = lista.Where(C => C.Id_Sondaje == idSondaje).ToList();
            return listn;
        }

        public void InsertarCorrida(EntCorrida cor)
        {
            try
            {
                cor.Id = ReturnId();
                db.InsertCorrida(cor);
            }
            catch (Exception e)
            {
                throw e;

            }
        }

        public void EditarCorrida(EntCorrida cor)
        {
            try
            {
                db.UpdateCorrida(cor);
            }
            catch (Exception e)
            {
                throw e;

            }
        }

        public void EliminarCorrida(string id)
        {
            try
            {
                db.DeleteCorrida(id);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private string ReturnId()
        {
            string id = "";
            List<string> list = new List<string>();
            
            if (lista.Count() <= 0)
                id = "C" + DatosGlobales.Iniciales + "-1";
            else
            {
                var temp = lista.Where(t => t.IdUser == DatosGlobales.Id_Usuario);//retorna ultimo sondaje del usuario
                foreach (EntCorrida corrida in temp)
                {
                    list.Add(corrida.Id);
                }
                id = "C" + HelpIdData.NewIdString(DatosGlobales.Iniciales, list);
            }
            return id;
        }
    }
}
