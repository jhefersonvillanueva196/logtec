﻿using CapaDato;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class LogEstructura
    {
        private DbProyect db = new DbProyect();

        private static readonly LogEstructura _instancia = new LogEstructura();
        public static LogEstructura Instancia
        {
            get { return LogEstructura._instancia; }
        }

        public List<EntEstructura> ListarEstructuras()
        {
            List<EntEstructura> lista = new List<EntEstructura>();
            lista = db.GetEstructurasOrderByProfundidad();
            string numero = DatosGlobales.Id_Corrida;
            var listn = lista.Where(C => C.Id_Corrida == numero).ToList();
            return listn;
        }

        public List<EntEstructura> ListarEstructuraTramo(string son, double tramoI, double tramoF)
        {
            List<EntEstructura> lista = new List<EntEstructura>();
            lista = ListarEstructurasSondaje(son);
            var listn = lista.Where(e => e.Profundidad_e >= tramoI && e.Profundidad_e <= tramoF).ToList();
            return listn;
        }
        public List<EntEstructura> ListarEstructuraTipoEstruc(string son, List<string> tipoestructuras)
        {
            List<EntEstructura> lista = new List<EntEstructura>();
            List<EntEstructura> listn = new List<EntEstructura>();
            lista = ListarEstructurasSondaje(son);
            foreach (var item in tipoestructuras)
            {
                foreach (var estructura in lista)
                {
                    if(estructura.Tipo_e == item)
                    {
                        listn.Add(estructura);
                    }
                }
            }
            return listn;
        }

        public List<EntEstructura> ListarEstructuraTramoTipoEstruc(string son, double tramoI, double tramoF, List<string> tipoestructuras)
        {
            List<EntEstructura> lista = new List<EntEstructura>();
            List<EntEstructura> listn = new List<EntEstructura>();
            lista = ListarEstructurasSondaje(son);
            foreach (var item in tipoestructuras)
            {
                foreach (var estructura in lista)
                {
                    if (estructura.Tipo_e == item)
                    {
                        listn.Add(estructura);
                    }
                }
            }
           var list = listn.Where(e => e.Profundidad_e >= tramoI && e.Profundidad_e <= tramoF).ToList();
            return list;
        }

        public List<EntEstructura> ListarEstructurasSond()
        {
            List<EntEstructura> lista = new List<EntEstructura>();
            lista = db.GetEstructurasOrderByProfundidad();
            string id = DatosGlobales.Id_Sondaje;
            List<EntEstructura> listn = lista.Where(C => C.Id_Sondaje == id).ToList();
            return listn;
        }

        public List<EntEstructura> ListarEstructurasSondaje(string idSondaje)
        {
            List<EntEstructura> lista = new List<EntEstructura>();
            lista = db.GetEstructurasOrderByProfundidad();
            List<EntEstructura> listn = lista.Where(C => C.Id_Sondaje == idSondaje).ToList();
            return listn;
        }

        public void InsertarEstructura(EntEstructura es)
        {
            try
            {
                es.Id = ReturnId();
                db.InsertEstructura(es);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void EditarEstructura(EntEstructura es)
        {
            try
            {
                db.UpdateEstructura(es);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void EliminarEstructura(string id)
        {
            try
            {
                db.DeleteEstructura(id);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private string ReturnId()
        {
            List<EntEstructura> lista = new List<EntEstructura>();
            lista = db.GetEstructurasOrderByNumEstructura();
            List<string> list = new List<string>();
            string id = "";
            
            if (lista.Count() <= 0)
                id = "E" + DatosGlobales.Iniciales + "-1";
            else
            {
                var temp = lista.Where(t => t.Id_Usuario == DatosGlobales.Id_Usuario);//retorna ultimo sondaje del usuario
                foreach (EntEstructura estructura in temp)
                {
                    list.Add(estructura.Id);
                }
                id = "E" + HelpIdData.NewIdString(DatosGlobales.Iniciales, list);
            }             
            return id;
        }
    }
}
