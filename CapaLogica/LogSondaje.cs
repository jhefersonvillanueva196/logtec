﻿using CapaDato;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class LogSondaje
    {
        double y;
        private List<EntSondaje> lista = new List<EntSondaje>();
        private DbProyect db = new DbProyect();

        private static readonly LogSondaje _instancia = new LogSondaje();
        public static LogSondaje Instancia
        {
            get { return LogSondaje._instancia; }
        }

        public List<EntSondaje> ListarSondajes()
        {
            lista = db.GetSondajesOrderByFecha();
            return lista;
        }

        public void InsertarSondaje(EntSondaje son)
        {
            try
            {
                son.Id = ReturnId();
                db.InsertSondaje(son);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void EditarSondaje(EntSondaje son, List<EntEstructura> lista)
        {
            double Alpha, Beta, TB, RefToTop;
            try
            {
                db.UpdateSondaje(son);
                foreach(EntEstructura temp in lista)
                {
                    Alpha = temp.Alpha;
                    Beta = temp.Beta;
                    TB = temp.T_B;
                    RefToTop = temp.Ref_to_top;
                    temp.FechaEs = DateTime.Now.ToLongDateString();
                    if (Alpha == -1)
                    {
                        temp.Dip_cal = -1;
                    }
                    else
                    {
                        if (Math.Round(Calculos(Alpha, Beta, TB, RefToTop)) > 90)
                        {
                            temp.Dip_cal = 180 - Math.Round(Calculos(Alpha, Beta, TB, RefToTop));
                        }
                        else
                        {
                            temp.Dip_cal = Math.Round(Calculos(Alpha, Beta, TB, RefToTop));
                        }                        
                    }
                    if (Beta == -1)
                    {
                        temp.Dip_dir_cal = -1;
                    }
                    else
                    {
                        if (Math.Round(y) > 360)
                        {
                            temp.Dip_dir_cal = Math.Round(y) - 360;
                        }
                        else
                        {
                            temp.Dip_dir_cal = Math.Round(y);
                        }
                        
                    }
                    if (Alpha == -1)
                    {
                        temp.Dips = -1;
                    }
                    else
                    {
                        temp.Dips = temp.Dip_cal;
                    }
                    if (Beta == -1)
                    {
                        temp.Strike = -1;
                    }
                    else
                    {
                        if (temp.Dip_dir_cal < 180)
                        {
                            temp.Strike = Math.Abs(temp.Dip_dir_cal - 90);
                        }
                        else
                        {
                            if (temp.Dip_dir_cal > 180)
                            {
                                double nuevo = temp.Dip_dir_cal + 90;
                                if (nuevo > 360)
                                {
                                    temp.Strike = nuevo - 360;
                                }
                                else
                                {
                                    temp.Strike = nuevo;
                                }
                            }
                        }
                    }
                    temp.State = "Editado";
                    db.UpdateEstructura(temp);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void EliminarSondaje(string id)
        {
            try
            {
                db.DeleteSondaje(id);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public double Calculos(double Alpha, double Beta, double TB , double RefToTop)
        {
            double a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x;
            double DR = Math.PI / 180;
            a = 90 - DatosGlobales.NewDip;
            if (TB == 0)
            {
                b = Beta + RefToTop;
            }
            else
            {
                b = Beta - RefToTop;
            }
            if (b >= 360)
            {
                c = b - 360;
            }
            else
            {
                c = b;
            }
            if (c < 0)
            {
                d = c + 360;
            }
            else
            {
                d = c;
            }
            if (TB == 1)
            {
                e = 180 - d;
            }
            else
            {
                e = d;
            }
            if (e < 0)
            {
                f = 360 - e;
            }
            else
            {
                f = e;
            }
            if (f >= 360)
            {
                g = f - 360;
            }
            else
            {
                g = f;
            }
            h = g + 180;
            if (h >= 360)
            {
                i = h - 360;
            }
            else
            {
                i = h;
            }
            j = 90 - Alpha;
            k = Math.Sin(i * DR);
            l = Math.Cos(i * DR);
            m = Math.Sin(j * DR);
            n = Math.Cos(j * DR);
            o = (l * m * Math.Cos(a * DR) + n * Math.Sin(a * DR)) * Math.Cos(DatosGlobales.NewAzimut * DR) - k * m * Math.Sin(DatosGlobales.NewAzimut * DR);
            p = k * m * Math.Cos(DatosGlobales.NewAzimut * DR) + (l * m * Math.Cos(a * DR) + n * Math.Sin(a * DR)) * Math.Sin(DatosGlobales.NewAzimut * DR);
            q = n * Math.Cos(a * DR) - l * m * Math.Sin(a * DR);
            r = Math.Sqrt(Math.Pow(o, 2) + Math.Pow(p, 2) + Math.Pow(q, 2));
            s = o / r;
            t = p / r;
            u = q / r;
            //double temp = Math.Atan(t / s);
            v = (Math.Abs(Math.Atan(t / s)) / DR);
            if (t < 0)
            {
                if (s < 0)
                {
                    w = v;
                }
                else
                {
                    w = 180 - v;
                }
            }
            else
            {
                if (s < 0)
                {
                    w = 360 - v;
                }
                else
                {
                    w = v + 180;
                }
            }
            x = (Math.Acos(u)) / DR;
            if (x > 90)
            {
                y = w + 180;
            }
            else
            {
                y = w;
            }
            return x;
        }
           
        private string ReturnId()
        {
            string id = "";
            List<string> list = new List<string>();
            
            if (lista.Count() <= 0)
                id = "S" + DatosGlobales.Iniciales + "-1";
            else
            {
                var temp = lista.Where(t => t.Id_User == DatosGlobales.Id_Usuario);
                foreach (EntSondaje sondaje in temp)
                {
                    list.Add(sondaje.Id);
                }
                id = "S" + HelpIdData.NewIdString(DatosGlobales.Iniciales, list);
            }
            return id;
        }
    }
}
