﻿using CapaDato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class LogDataBase
    {
        private static readonly LogDataBase _instancia = new LogDataBase();
        public static LogDataBase Instancia
        {
            get { return LogDataBase._instancia; }
        }
        public void InicializarBD()
        {
            DbContextSQLite.Up();
        }
    }
}
