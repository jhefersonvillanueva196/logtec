﻿using CapaDato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    public class LogSincronizacion
    {
        private static readonly LogSincronizacion _instancia = new LogSincronizacion();
        public static LogSincronizacion Instancia
        {
            get { return LogSincronizacion._instancia; }
        }

        public void Sinc_Remota()
        {
            DbProyect sinc = new DbProyect();
            sinc.SincSondajeRemote();
            sinc.SincCorridaRemote();
            sinc.SincEstructuraRemote();
        }

        public void Sinc_Local()
        {
            DbProyect sinc = new DbProyect();
            sinc.SincSondajeLocal();
            sinc.SincCorridaLocal();
            sinc.SincEstructuraLocal();
        }
    }
}
