﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDato
{
    public class HelpIdData
    {
        public static int NewNumberId(string idString)//devuelve número para poner en el ID
        {
            string[] arrayId = idString.Split('-');
            int numId = int.Parse(arrayId[1]);
            numId++;
            return numId;
        }
        public static string NewIdString(string idUser, List<string> list)//devuelve el nuevo id de usuario para la tabla
        {
            List<int> lista = new List<int>();
            foreach (string text in list)
            {
                string[] arrayNum = text.Split('-');
                lista.Add(int.Parse(arrayNum[1]));
            }
            var arr = lista.ToArray();
            int t;
            for (int a = 1; a < arr.Length; a++)
            {
                for (int b = arr.Length - 1; b >= a; b--)
                {
                    if (arr[b - 1] > arr[b])
                    {
                        t = arr[b - 1];
                        arr[b - 1] = arr[b];
                        arr[b] = t;
                    }
                }
            }
            int num = arr[arr.Length - 1];
            string newId = idUser + "-" + (num + 1);
            return newId;
        }
    }
}
