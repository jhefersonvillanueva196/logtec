﻿using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDato
{
    public class Sincronizacion
    {
        private ControlDataSQLite DbLocal = new ControlDataSQLite();
        private ControlDataMySql DbRemote = new ControlDataMySql();

        public int PushSondaje()//no se actualizó, //1 se actualizó, // 2 No hay datos que subir //
        {
            List<EntSondaje> local = DbLocal.GetSondajesOrderByFecha();
            List<EntSondaje> remote = DbRemote.GetSondajesOrderByFecha();
            int datos = 0;
            //si local tiene datos
            //Si hay datos en el remote sube solo nuevos, de lo contrario todos
            if (local.Count > 0)
            {
                if (remote.Count > 0)
                {
                    foreach (EntSondaje temp in local)
                    {
                        if (temp.State.Equals("Nuevo") && temp.Validar.Equals(1))
                        {
                            temp.State = "Subido";
                            DbRemote.InsertSondaje(temp);
                            DbLocal.UpdateSondajeSinc(temp);
                        }
                        else
                        {
                            var aux = remote.FirstOrDefault(e => e.Id == temp.Id);
                            if (aux != null)
                            {
                                if (temp.State.Equals("Editado") && temp.Validar.Equals(1))
                                {
                                    temp.State = "Subido";
                                    DbRemote.UpdateSondajeSinc(temp);
                                    DbLocal.UpdateSondajeSinc(temp);
                                    datos++;
                                }
                            }
                            else
                            {
                                if (temp.State.Equals("Editado") && temp.Validar.Equals(1))
                                {
                                    temp.State = "Subido";
                                    DbRemote.InsertSondaje(temp);
                                    DbLocal.UpdateSondajeSinc(temp);

                                    datos++;
                                }
                            }
                        }                   
                    }
                    if (datos == 0) return 2;
                    else return 1;
                }
                else
                {
                    foreach (EntSondaje temp in local)
                    {
                        if (temp.Validar.Equals(1))
                        {
                            temp.State = "Subido";
                            DbRemote.InsertSondaje(temp);
                            DbLocal.UpdateSondajeSinc(temp);
                            datos++;
                        }
                    }
                    if (datos == 0) return 2;
                    else return 1;
                }
            }
            else if (local.Count == 0) return 2;
            return 0;
        }
        public int PushCorrida()//no se actualizó, //1 se actualizó, // 2 No hay datos que subir //
        {
            List<EntCorrida> local = DbLocal.GetCorridasOrderByDesde();
            List<EntCorrida> remote = DbRemote.GetCorridasOrderByDesde();
            int datos = 0;
            //si local tiene datos
            //Si hay datos en el remote sube solo nuevos, de lo contrario todos
            if (local.Count > 0)
            {
                if (remote.Count > 0)
                {
                    foreach (EntCorrida temp in local)
                    {
                        if (temp.State.Equals("Nuevo") && temp.Validar.Equals(1))
                        {
                            temp.State = "Subido";
                            DbRemote.InsertCorrida(temp);
                            DbLocal.UpdateCorridaSinc(temp);
                            datos++;
                        }
                        else
                        {
                            var aux = remote.FirstOrDefault(e => e.Id == temp.Id);
                            if (aux != null)
                            {
                                if (temp.State.Equals("Editado") && temp.Validar.Equals(1))
                                {
                                    temp.State = "Subido";
                                    DbRemote.UpdateCorridaSinc(temp);
                                    DbLocal.UpdateCorridaSinc(temp);
                                    datos++;
                                }
                            }
                            else
                            {
                                if (temp.State.Equals("Editado") && temp.Validar.Equals(1))
                                {
                                    temp.State = "Subido";
                                    DbRemote.InsertCorrida(temp);
                                    DbLocal.UpdateCorridaSinc(temp);

                                    datos++;
                                }
                            }
                        }
                    }
                    if (datos == 0) return 2;
                    else return 1;
                }
                else
                {
                    foreach (EntCorrida temp in local)
                    {
                        if (temp.Validar.Equals(1))
                        {
                            temp.State = "Subido";
                            DbRemote.InsertCorrida(temp);
                            DbLocal.UpdateCorridaSinc(temp);                 
                            datos++;
                        }
                    }
                    if (datos == 0) return 2;
                    else return 1;
                }
            }
            else if (local.Count == 0) return 2;
            return 0;
        }
        public int PushEstructura()//no se actualizó, //1 se actualizó, // 2 No hay datos que subir //
        {
            List<EntEstructura> local = DbLocal.GetEstructurasOrderByProfundidad();
            List<EntEstructura> remote = DbRemote.GetEstructurasOrderByProfundidad();
            int datos = 0;
            //si local tiene datos
            //Si hay datos en el remote sube solo nuevos, de lo contrario todos
            if (local.Count > 0)
            {
                if (remote.Count > 0)
                {
                    foreach (EntEstructura temp in local)
                    {
                        if (temp.State.Equals("Nuevo") && temp.Validar.Equals(1))
                        {
                            temp.State = "Subido";
                            DbRemote.InsertEstructura(temp);
                            DbLocal.UpdateEstructuraSinc(temp);

                            datos++;
                        }
                        else
                        {
                            var aux = remote.FirstOrDefault(e => e.Id == temp.Id);
                            if (aux != null)
                            {
                                if (temp.State.Equals("Editado") && temp.Validar.Equals(1))
                                {
                                    temp.State = "Subido";
                                    DbRemote.UpdateEstructuraSinc(temp);
                                    DbLocal.UpdateEstructuraSinc(temp);
                                    datos++;
                                }
                            }
                            else
                            {
                                if (temp.State.Equals("Editado") && temp.Validar.Equals(1))
                                {
                                    temp.State = "Subido";
                                    DbRemote.InsertEstructura(temp);
                                    DbLocal.UpdateEstructuraSinc(temp);

                                    datos++;
                                }
                            }
                        }
                    }
                    if (datos == 0) return 2;
                    else return 1;
                }
                else
                {
                    foreach (EntEstructura temp in local)
                    {
                        if (temp.Validar.Equals(1))
                        {
                            temp.State = "Subido";
                            DbRemote.InsertEstructura(temp);
                            DbLocal.UpdateEstructuraSinc(temp);
                            datos++;
                        }
                    }
                    if (datos == 0) return 2;
                    else return 1;
                }
            }
            else if (local.Count == 0) return 2;
            return 0;
        }

        ////Local
        //public int PushLocalDataUser()
        //{
        //    List<EntUsuario> local = DbLocal.GetUsuarios();
        //    List<EntUsuario> remote = DbRemote.GetUsuarios();
        //    List<EntUsuario> down = new List<EntUsuario>();//lista a descargar
        //    int datos = 0;//Codigos

        //    if (local.Count == 0)//local vacía
        //    {
        //        if (remote.Count > 0)
        //        {
        //            foreach (EntUsuario temp in remote)
        //            {
        //                if (temp.Estado != 100)//activo
        //                {
        //                    if (temp.State.Equals("Editado"))
        //                    {
        //                        temp.State = "Subido";
        //                        DbLocal.UpdateUsuario(temp);
        //                    }
        //                    else
        //                    {
        //                        temp.State = "Subido";
        //                        DbLocal.InsertUser(temp);
        //                    }
        //                }

        //            }
        //        }
        //    }
        //    else//local no vacía
        //    {
        //        if (remote.Count > 0)
        //        {
        //            foreach (EntUsuario temp in remote)
        //            {
        //                if (!down.Contains(temp) || temp.Estado == 100)
        //                {
        //                    down.Add(temp);
        //                }
        //            }
        //            if (down.Count == 0) return 2;
        //            else
        //            {
        //                foreach (EntUsuario temp in down)
        //                {
        //                    if (temp.Estado != 100)
        //                    {
        //                        if (temp.State.Equals("Editado"))
        //                        {
        //                            temp.State = "Subido";
        //                            DbLocal.UpdateUsuario(temp);
        //                        }
        //                        else
        //                        {
        //                            temp.State = "Subido";
        //                            DbLocal.InsertUser(temp);
        //                            datos++;
        //                        }
        //                    }
        //                    else if (temp.Estado == 100)
        //                    {
        //                        DbLocal.DeleteUsuario(temp);
        //                    }
        //                }
        //                return 1;
        //            }
        //        }
        //        else return 2;

        //    }

        //    return 0;
        //}
        public int PushLocalDataSondaje()//no se actualizó, //1 se actualizó, // 2 No hay datos que subir //
        {
            List<EntSondaje> local = DbLocal.GetSondajesOrderByFecha();
            List<EntSondaje> remote = DbRemote.GetSondajesOrderByFecha();

            if (local.Count == 0)//local vacía
            {
                if (remote.Count > 0)
                {
                    foreach (EntSondaje temp in remote)
                    {
                        DbLocal.InsertSondaje(temp);
                    }
                }
            }
            else//local no vacía
            {
                if (remote.Count > 0)
                {
                    foreach (EntSondaje temp in remote)
                    {
                        var nuevo = local.FirstOrDefault(l => l.Id == temp.Id);
                        if (nuevo == null)
                        {
                            DbLocal.InsertSondaje(temp);
                        }
                        else
                        {
                            if (nuevo.State != "Editado" && nuevo.State != "Nuevo")
                            {
                                DbLocal.UpdateSondajeSinc(temp);
                            }
                        }

                    }                    
                }
                else return 2;
            }
            return 0;
        }
        public int PushLocalDataCorrida()//no se actualizó, //1 se actualizó, // 2 No hay datos que subir //
        {
            List<EntCorrida> local = DbLocal.GetCorridasOrderByDesde();
            List<EntCorrida> remote = DbRemote.GetCorridasOrderByDesde();

            if (local.Count == 0)//local vacía
            {
                if (remote.Count > 0)
                {
                    foreach (EntCorrida temp in remote)
                    {
                        DbLocal.InsertCorrida(temp);
                    }
                }
            }
            else//local no vacía
            {
                if (remote.Count > 0)
                {
                    foreach (EntCorrida temp in remote)
                    {
                        var nuevo = local.FirstOrDefault(l => l.Id == temp.Id);
                        if (nuevo == null)
                        {
                            DbLocal.InsertCorrida(temp);
                        }
                        else
                        {
                            if (nuevo.State != "Editado" && nuevo.State != "Nuevo")
                            {
                                DbLocal.UpdateCorridaSinc(temp);
                            }
                        }
                    }
                }
                else return 2;
            }
            return 0;
        }
        public int PushLocalDataEstructura()//no se actualizó, //1 se actualizó, // 2 No hay datos que subir //
        {
            List<EntEstructura> local = DbLocal.GetEstructurasOrderByProfundidad();
            List<EntEstructura> remote = DbRemote.GetEstructurasOrderByProfundidad();

            if (local.Count == 0)//local vacía
            {
                if (remote.Count > 0)
                {
                    foreach (EntEstructura temp in remote)
                    {
                        DbLocal.InsertEstructura(temp);                   
                    }
                }
            }
            else//local no vacía
            {
                if (remote.Count > 0)
                {
                    foreach (EntEstructura temp in remote)
                    {
                        var nuevo = local.FirstOrDefault(l => l.Id == temp.Id);
                        if (nuevo == null)
                        {
                            DbLocal.InsertEstructura(temp);
                        }
                        else
                        {
                            if (nuevo.State != "Editado" && nuevo.State != "Nuevo")
                            {
                                DbLocal.UpdateEstructuraSinc(temp);
                            }
                        }
                    }
                }
                else return 2;
            }
            return 0;
        }
    }
}
