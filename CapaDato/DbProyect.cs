﻿using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDato
{
    public class DbProyect
    {
        private Sincronizacion sincronizar = new Sincronizacion();
        private ControlDataSQLite local = new ControlDataSQLite();

        #region Get

        public List<EntUsuario> GetUsuario()
        {
            return local.GetUsuarios();
        }
        public List<EntSondaje> GetSondajesOrderByFecha()
        {
            return local.GetSondajesOrderByFecha();
        }
        public List<EntCorrida> GetCorridasOrderByDesde()
        {
            return local.GetCorridasOrderByDesde();
        }
        public List<EntEstructura> GetEstructurasOrderByProfundidad()
        {
            return local.GetEstructurasOrderByProfundidad();
        }
        public List<EntEstructura> GetEstructurasOrderByNumEstructura()
        {
            return local.GetEstructurasOrderByNumEstructura();
        }
        public List<EntAuditoria> GetAuditorias()
        {
            return local.GetAuditorias();
        }

        public List<EntConsulta> GetConsultas()
        {
            return local.GetConsultas();
        }

        #endregion

        #region Insert

        public bool InsertSondaje(EntSondaje data)
        {
            local.InsertSondaje(data);
            return true;
        }
        public bool InsertCorrida(EntCorrida data)
        {
            local.InsertCorrida(data);
            return true;
        }
        public bool InsertEstructura(EntEstructura data)
        {
            local.InsertEstructura(data);
            return true;
        }
        public bool InsertAuditoria(EntAuditoria data)
        {
            local.InsertAuditoria(data);
            return true;
        }

        #endregion

        # region Update

        public bool UpdateUsuario(EntUsuario data)
        {
            local.UpdateUsuario(data);
            return true;
        }

        public bool UpdateSondaje(EntSondaje data)
        {
            local.UpdateSondaje(data);
            return true;
        }
        public bool UpdateCorrida(EntCorrida data)
        {
            local.UpdateCorrida(data);
            return true;
        }
        public bool UpdateEstructura(EntEstructura data)
        {
            local.UpdateEstructura(data);
            return true;
        }

        #endregion

        #region Delete

        public bool DeleteSondaje(string id)
        {
            local.DeleteSondaje(id);
            return true;
        }
        public bool DeleteCorrida(string id)
        {
            local.DeleteCorrida(id);
            return true;
        }
        public bool DeleteEstructura(string id)
        {
            local.DeleteEstructura(id);
            return true;
        }

        #endregion

        //sincronizar remote
        public bool SincSondajeRemote()
        {
            sincronizar.PushSondaje();
            return true;
        }
        public bool SincCorridaRemote()
        {
            sincronizar.PushCorrida();
            return true;
        }
        public bool SincEstructuraRemote()
        {
            sincronizar.PushEstructura();
            return true;
        }

        //sincronizar Local
        public bool SincUserLocal()
        {
            //sincronizar.PushUsuario();
            return true;
        }

        public bool SincSondajeLocal()
        {
            sincronizar.PushLocalDataSondaje();
            return true;
        }
        public bool SincCorridaLocal()
        {
            sincronizar.PushLocalDataCorrida();
            return true;
        }
        public bool SincEstructuraLocal()
        {
            sincronizar.PushLocalDataEstructura();
            return true;
        }

    }
}
