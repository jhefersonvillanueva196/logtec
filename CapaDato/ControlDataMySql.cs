﻿using CapaEntidad;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDato
{
    class ControlDataMySql
    {
        MySqlConnection cn;

        #region Gets

        public List<EntUsuario> GetUsuarios()
        {
            List<EntUsuario> users = new List<EntUsuario>();
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            MySqlCommand cmd = new MySqlCommand(ConsultasSQL.SelectUsuario(), cn);
            MySqlDataReader dr = cmd.ExecuteReader();
            if (dr != null)
            {
                while (dr.Read())
                {
                    EntUsuario user = new EntUsuario()
                    {
                        Id = dr["Id"].ToString(),
                        Nombre = dr["Nombre"].ToString(),
                        Apellido = dr["Apellido"].ToString(),
                        Dni = dr["DNI"].ToString(),
                        Cargo = dr["Cargo"].ToString(),
                        Iniciales = dr["Iniciales"].ToString(),
                        Contrasenia = dr["Contrasenia"].ToString(),
                        Rol = dr["Rol"].ToString(),
                    };
                    users.Add(user);
                }
            }
            cn.Close();
            return users;
        }

        public List<EntSondaje> GetSondajesOrderByFecha()
        {
            List<EntSondaje> sondajes = new List<EntSondaje>();            
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            MySqlCommand cmd = null;
            cmd = new MySqlCommand(ConsultasSQL.SelectSondajeOrderByFecha(), cn);
            MySqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {

                EntSondaje sondaje = new EntSondaje()
                {
                    Id = dr["Id"].ToString(),
                    Id_User = dr["Id_User"].ToString(),
                    Cliente = dr["Cliente"].ToString(),
                    Proyecto = dr["Proyecto"].ToString(),
                    Geotecnico = dr["Geotecnico"].ToString(),
                    Nom_sondaje = dr["Nom_sondaje"].ToString(),
                    Fecha = dr["Fecha"].ToString(),
                    Profundidad = double.Parse(dr["Profundidad"].ToString()),
                    Norte = double.Parse(dr["Norte"].ToString()),
                    Este = double.Parse(dr["Este"].ToString()),
                    Cota = double.Parse(dr["Cota"].ToString()),
                    Maquina = dr["Maquina"].ToString(),
                    Linea_Core = dr["Linea_Core"].ToString(),
                    Dip = double.Parse(dr["Dip"].ToString()),
                    Azimut = double.Parse(dr["Azimut"].ToString()),
                    State = dr["State"].ToString(),
                    Validar = int.Parse(dr["Validar"].ToString()),
                };
                sondajes.Add(sondaje);
            }
            cn.Close();
            return sondajes;
        }

        public List<EntCorrida> GetCorridasOrderByDesde()
        {
            List<EntCorrida> Corridas = new List<EntCorrida>();
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            MySqlCommand cmd = new MySqlCommand(ConsultasSQL.SelectCorridaOrderByDesde(), cn);
            MySqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                EntCorrida corrida = new EntCorrida()
                {
                    Id = dr["Id"].ToString(),
                    IdUser = dr["IdUser"].ToString(),
                    Id_Sondaje = dr["Id_Sondaje"].ToString(),
                    Desde = double.Parse(dr["Desde"].ToString()),
                    Hasta = double.Parse(dr["Hasta"].ToString()),
                    Perforacion = double.Parse(dr["Perforacion"].ToString()),
                    Recuperacion = double.Parse(dr["Recuperacion"].ToString()),
                    Rqd = double.Parse(dr["Rqd"].ToString()),
                    Lrf = double.Parse(dr["Lrf"].ToString()),
                    Nfn = int.Parse(dr["Nfn"].ToString()),
                    Litologia_1 = dr["Litologia_1"].ToString(),
                    Litologia_2 = dr["Litologia_2"].ToString(),
                    Re = dr["Re"].ToString(),
                    TipoEstruct_1 = dr["TipoEstruct_1"].ToString(),
                    TipoEstruct_2 = dr["TipoEstruct_1"].ToString(),
                    Jn = dr["Jn"].ToString(),
                    Jr = dr["Jr"].ToString(),
                    Ja = dr["Ja"].ToString(),
                    Jw = dr["Jw"].ToString(),
                    Srf = dr["Srf"].ToString(),
                    Q_Barton = double.Parse(dr["Q_Barton"].ToString()),
                    Geotecnico = dr["Geotecnico"].ToString(),
                    Comentario = dr["Comentario"].ToString(),
                    Rmr = double.Parse(dr["Rmr"].ToString()),
                    Adicional = double.Parse(dr["Adicional"].ToString()),
                    State = dr["State"].ToString(),
                    Validar = int.Parse(dr["Validar"].ToString())
                };
                Corridas.Add(corrida);
            }
            cn.Close();
            return Corridas;
        }

        public List<EntEstructura> GetEstructurasOrderByProfundidad()
        {
            List<EntEstructura> estructuras = new List<EntEstructura>();
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            MySqlCommand cmd = new MySqlCommand(ConsultasSQL.SelectEstructuraOrderByProfundidad(), cn);
            MySqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                EntEstructura estructura = new EntEstructura()
                {
                    Id = dr["Id"].ToString(),
                    Id_Sondaje = dr["Id_Sondaje"].ToString(),
                    Id_Corrida = dr["Id_Corrida"].ToString(),
                    Id_Usuario = dr["Id_Usuario"].ToString(),
                    Num_estructura = dr["Num_estructura"].ToString(),
                    Profundidad_e = double.Parse(dr["Profundidad_e"].ToString()),
                    Tipo_e = dr["Tipo_e"].ToString(),
                    T_B = double.Parse(dr["T_B"].ToString()),
                    Alpha = double.Parse(dr["Alpha"].ToString()),
                    Beta = double.Parse(dr["Beta"].ToString()),
                    Ref_to_top = double.Parse(dr["Ref_to_top"].ToString()),
                    Cond_fractura = int.Parse(dr["Cond_fractura"].ToString()),
                    Jrc = double.Parse(dr["Jrc"].ToString()),
                    Jr = double.Parse(dr["Jr"].ToString()),
                    Ja = double.Parse(dr["Ja"].ToString()),
                    Espesor = double.Parse(dr["Espesor"].ToString()),
                    Relleno_1 = dr["Relleno_1"].ToString(),
                    Relleno_2 = dr["Relleno_2"].ToString(),
                    Comentario = dr["Comentario"].ToString(),
                    Geotecnico = dr["Geotecnico"].ToString(),
                    FechaEs = dr["FechaEs"].ToString(),
                    Dip_cal = double.Parse(dr["Dip_cal"].ToString()),
                    Dip_dir_cal = double.Parse(dr["Dip_dir_cal"].ToString()),
                    Dips = double.Parse(dr["Dips"].ToString()),
                    Strike = double.Parse(dr["Strike"].ToString()),
                    State = dr["State"].ToString(),
                    Validar = int.Parse(dr["Validar"].ToString())
                };
                estructuras.Add(estructura);
            }
            cn.Close();
            return estructuras;
        }

        public List<EntAuditoria> GetAuditorias()
        {
            List<EntAuditoria> auditorias = new List<EntAuditoria>();
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            MySqlCommand cmd = new MySqlCommand(ConsultasSQL.SelectAuditoria(), cn);
            MySqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                EntAuditoria auditoria = new EntAuditoria()
                {
                    Id = dr["Id"].ToString(),
                    Id_User = dr["Id_User"].ToString(),
                    Nombre = dr["Nombre"].ToString(),
                    Apellido = dr["Apellido"].ToString(),
                    Elemento = dr["Elemento"].ToString(),
                    Accion = dr["Accion"].ToString(),
                    Fecha = dr["Fecha"].ToString(),
                };
                auditorias.Add(auditoria);
            }
            cn.Close();
            return auditorias;
        }

        public List<EntConsulta> GetConsultas()
        {
            List<EntConsulta> consultas = new List<EntConsulta>();
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            MySqlCommand cmd = new MySqlCommand(ConsultasSQL.SelectConsulta(), cn);
            MySqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                EntConsulta consulta = new EntConsulta()
                {
                    Nom_sondaje = dr["Nom_sondaje"].ToString(),
                    Fecha = DateTime.Parse(dr["Fecha"].ToString()),
                    Linea_Core = dr["Linea_Core"].ToString(),
                    Dip = double.Parse(dr["Dip"].ToString()),
                    Azimut = double.Parse(dr["Azimut"].ToString()),
                    Desde = double.Parse(dr["Desde"].ToString()),
                    Hasta = double.Parse(dr["Hasta"].ToString()),
                    Num_estructura = dr["Num_estructura"].ToString(),
                    Profundidad_e = double.Parse(dr["Profundidad_e"].ToString()),
                    Tipo_e = dr["Tipo_e"].ToString(),
                    T_B = double.Parse(dr["T_B"].ToString()),
                    Alpha = double.Parse(dr["Alpha"].ToString()),
                    Beta = double.Parse(dr["Beta"].ToString()),
                    Ref_to_top = double.Parse(dr["Ref_to_top"].ToString()),
                    Cond_fractura = double.Parse(dr["Cond_fractura"].ToString()),
                    Jrc = double.Parse(dr["Jrc"].ToString()),
                    Jr = double.Parse(dr["Jr"].ToString()),
                    Ja = double.Parse(dr["Ja"].ToString()),
                    Espesor = double.Parse(dr["Espesor"].ToString()),
                    Relleno_1 = dr["Relleno_1"].ToString(),
                    Relleno_2 = dr["Relleno_2"].ToString(),
                    Comentario = dr["Comentario"].ToString(),
                    Geotecnico = dr["Geotecnico"].ToString(),
                    Dip_cal = double.Parse(dr["Dip_cal"].ToString()),
                    Dip_dir_cal = double.Parse(dr["Dip_dir_cal"].ToString()),
                    Dips = double.Parse(dr["Dips"].ToString()),
                    Strike = double.Parse(dr["Strike"].ToString()),
                    FechaEs = DateTime.Parse(dr["FechaEs"].ToString()),
                };
                consultas.Add(consulta);
            }
            cn.Close();
            return consultas;
        }

        #endregion

        #region Insert

        public void InsertSondaje(EntSondaje data)
        {
            MySqlCommand cmd = null;
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            cmd = new MySqlCommand(ConsultasSQL.InsertSondaje(data), cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void InsertCorrida(EntCorrida data)
        {
            MySqlCommand cmd = null;
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            cmd = new MySqlCommand(ConsultasSQL.InsertCorridas(data), cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void InsertEstructura(EntEstructura data)
        {
            MySqlCommand cmd = null;
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            cmd = new MySqlCommand(ConsultasSQL.InsertEstructura(data), cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void InsertAuditoria(EntAuditoria data)
        {
            MySqlCommand cmd = null;
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            cmd = new MySqlCommand(ConsultasSQL.InsertAuditoria(data), cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        #endregion

        #region Update

        public void UpdateUsuario(EntUsuario data)
        {
            MySqlCommand cmd = null;
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            cmd = new MySqlCommand(ConsultasSQL.UpdateUsuario(data), cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void UpdateSondaje(EntSondaje data)
        {
            MySqlCommand cmd = null;
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            cmd = new MySqlCommand(ConsultasSQL.UpdateSondaje(data), cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void UpdateCorrida(EntCorrida data)
        {
            MySqlCommand cmd = null;
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            cmd = new MySqlCommand(ConsultasSQL.UpdateCorrida(data), cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void UpdateEstructura(EntEstructura data)
        {
            MySqlCommand cmd = null;
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            cmd = new MySqlCommand(ConsultasSQL.UpdateEstructura(data), cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void UpdateSondajeSinc(EntSondaje data)
        {
            MySqlCommand cmd = null;
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            cmd = new MySqlCommand(ConsultasSQL.UpdateSondajeSinc(data), cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void UpdateCorridaSinc(EntCorrida data)
        {
            MySqlCommand cmd = null;
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            cmd = new MySqlCommand(ConsultasSQL.UpdateCorridaSinc(data), cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void UpdateEstructuraSinc(EntEstructura data)
        {
            MySqlCommand cmd = null;
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            cmd = new MySqlCommand(ConsultasSQL.UpdateEstructuraSinc(data), cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        #endregion

        #region Delete

        public void DeleteSondaje(string id)
        {
            MySqlCommand cmd = null;
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            cmd = new MySqlCommand(ConsultasSQL.DeleteSondaje(id), cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void DeleteCorrida(string id)
        {
            MySqlCommand cmd = null;
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            cmd = new MySqlCommand(ConsultasSQL.DeleteCorrida(id), cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void DeleteEstructura(string id)
        {
            MySqlCommand cmd = null;
            cn = DbContextMySQL.GetInstance();
            cn.Open();
            cmd = new MySqlCommand(ConsultasSQL.DeleteEstructura(id), cn);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        #endregion

    }
}
