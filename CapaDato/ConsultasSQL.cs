﻿using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDato
{
    public class ConsultasSQL
    {

        #region Select

        public static string SelectUsuario()
        {
            string consulta = "SELECT `Id`, `Nombre`, `Apellido`, `Dni`, `Cargo`, `Iniciales`, `Contrasenia`, `Rol` FROM `Usuario`";
            return consulta;
        }

        public static string SelectSondajeOrderByFecha()
        {
            string consulta = "SELECT `Id`, `Id_User`, `Cliente`, `Proyecto`, `Geotecnico`, `Nom_sondaje`, `Fecha`, `Profundidad`, `Norte`, `Este`, `Cota`, `Maquina`, `Linea_Core`, `Dip`, `Azimut`, `State`, `Validar` FROM `Sondaje` ORDER BY `Fecha`";
            return consulta;
        }

        public static string SelectCorridaOrderByDesde()
        {
            string consulta = "SELECT `Id`, `IdUser`, `Id_Sondaje`, `Desde`, `Hasta`, `Perforacion`, `Recuperacion`," +
                " `Rqd`, `Lrf`, `Nfn`, `Litologia_1`, `Litologia_2`, `Re`, `TipoEstruct_1`, `TipoEstruct_2`, `Jn`," +
                " `Jr`, `Ja`, `Jw`, `Srf`, `Q_Barton`, `Comentario`, `Rmr`, `Adicional`, `State`, `Validar` FROM `Corrida` ORDER BY `Desde`";
            return consulta;
        }

        public static string SelectEstructuraOrderByProfundidad()
        {
            string consulta = "SELECT `Id`, `Id_Sondaje`, `Id_Corrida`, `Id_Usuario`, `Num_estructura`, `Profundidad_e`, `Tipo_e`, `T_B`, `Alpha`, `Beta`, `Ref_to_top`, `Cond_fractura`, `Jrc`, `Jr`, `Ja`, `Espesor`, `Relleno_1`, `Relleno_2`, `Comentario`, `Geotecnico`, `FechaEs`, `Dip_cal`, `Dip_dir_cal`, `Dips`, `Strike`, `State`, `Validar` FROM `Estructura` ORDER BY `Profundidad_e`";
            return consulta;
        }

        public static string SelectEstructuraOrderByNumEstructura()
        {
            string consulta = "SELECT `Id`, `Id_Sondaje`, `Id_Corrida`, `Id_Usuario`, `Num_estructura`, `Profundidad_e`, `Tipo_e`, `T_B`, `Alpha`, `Beta`, `Ref_to_top`, `Cond_fractura`, `Jrc`, `Jr`, `Ja`, `Espesor`, `Relleno_1`, `Relleno_2`, `Comentario`, `Geotecnico`, `FechaEs`, `Dip_cal`, `Dip_dir_cal`, `Dips`, `Strike`, `State`, `Validar` FROM `Estructura` ORDER BY `Num_estructura`";
            return consulta;
        }

        public static string SelectAuditoria()
        {
            string consulta = "SELECT `Id`, `Id_User`, `Nombre`, `Apellido`, `Elemento`, `Accion`, `Fecha` FROM `Auditoria`";
            return consulta;
        }

        public static string SelectConsulta()
        {
            string consulta = "SELECT * FROM Sondaje JOIN Corrida ON Corrida.Id_Sondaje = Sondaje.Id JOIN Estructura ON Estructura.Id_Corrida = Corrida.Id WHERE Sondaje.Id='"+ DatosGlobales.Id_Sondaje +"';";
            return consulta;
        }

        #endregion

        #region Insert

        public static string InsertSondaje(EntSondaje sondaje)
        {
            string cadInsert = "INSERT INTO `Sondaje` (`Id`,`Id_User`, `Cliente`, `Proyecto`, `Geotecnico`, `Nom_sondaje`, `Fecha`, `Profundidad`," +
                " `Norte`, `Este`, `Cota`, `Maquina`, `Linea_Core`, `Dip`, `Azimut`, `State` , `Validar`) VALUES ('" + sondaje.Id + "','" + sondaje.Id_User + "','" + sondaje.Cliente + "','" +
                sondaje.Proyecto + "','" + sondaje.Geotecnico + "','" + sondaje.Nom_sondaje + "','" + sondaje.Fecha + "'," + sondaje.Profundidad + "," +
                sondaje.Norte + "," + sondaje.Este + "," + sondaje.Cota + ",'" + sondaje.Maquina + "','" +
                sondaje.Linea_Core + "'," + sondaje.Dip + "," + sondaje.Azimut + ",'" + sondaje.State + "'," + sondaje.Validar + ")";
            return cadInsert;
        }

        public static string InsertCorridas(EntCorrida data)
        {
            string consulta = "INSERT INTO `Corrida`(`Id`, `IdUser`, `Id_Sondaje`, `Desde`, `Hasta`, `Perforacion`, `Recuperacion`," +
                " `Rqd`, `Lrf`, `Nfn`, `Litologia_1`, `Litologia_2`, `Re`, `TipoEstruct_1`, `TipoEstruct_2`, `Jn`," +
                " `Jr`, `Ja`, `Jw`, `Srf`, `Q_Barton`, `Comentario`, `Rmr`, `Adicional`, `State`, `Validar`) VALUES('"
                + data.Id + "','" + data.IdUser + "','" + data.Id_Sondaje + "'," + data.Desde + "," + data.Hasta + ","
                + data.Perforacion + "," + data.Recuperacion + ",'" + data.Rqd + ",'" + data.Lrf + ",'" + data.Nfn + ",'"
                + data.Litologia_1 + ",'" + data.Litologia_2 + ",'" + data.Re + ",'" + data.TipoEstruct_1 + ",'"
                + data.TipoEstruct_2 + ",'" + data.Jn + ",'" + data.Jr + ",'" + data.Ja + ",'" + data.Jw + ",'"
                + data.Srf + ",'" + data.Q_Barton + ",'" + data.Geotecnico + ",'" + data.Comentario + ",'" 
                + data.Rmr + ",'" + data.Adicional + ",'" + data.State + "'," + data.Validar + ")";
            return consulta;
        }

        public static string InsertEstructura(EntEstructura data)
        {
            string consulta = "INSERT INTO `Estructura` (`Id`, `Id_Sondaje`,`Id_Corrida`, `Id_Usuario`, `Num_estructura`, `Profundidad_e`, `Tipo_e`, `T_B`, `Alpha`, `Beta`, `Ref_to_top`, `Cond_fractura`, `Jrc`, `Jr`, `Ja`, `Espesor`, `Relleno_1`, `Relleno_2`, `Comentario`, `Geotecnico`, `FechaEs`, `Dip_cal`, `Dip_dir_cal`, `Dips`, `Strike`, `State`, `Validar`) VALUES ('" + data.Id + "','" + data.Id_Sondaje + "','" + data.Id_Corrida + "','" + data.Id_Usuario + "','"
                + data.Num_estructura + "','" + data.Profundidad_e + "','" + data.Tipo_e + "','" + data.T_B + "','"
                + data.Alpha + "','" + data.Beta + "','" + data.Ref_to_top + "','" + data.Cond_fractura + "','" + data.Jrc + "','" + data.Jr + "','" + data.Ja + "','"
                + data.Espesor + "','" + data.Relleno_1 + "','"
                + data.Relleno_2 + "','" + data.Comentario + "','" + data.Geotecnico + "','" + data.FechaEs + "','" + data.Dip_cal + "','" + data.Dip_dir_cal + "','"
                + data.Dips + "','" + data.Strike + "','" + data.State + "'," + data.Validar + ")";
            return consulta;
        }

        public static string InsertAuditoria(EntAuditoria data)
        {
            string consulta = "INSERT INTO `Auditoria` (`Id`, `Id_User`, `Nombre`, `Apellido`, `Elemento`, `Accion`, `Fecha`) VALUES ('" + data.Id + "','" + data.Id_User + "','" + data.Nombre + "','" + data.Apellido + "','" + data.Elemento + "','" + data.Accion + "','"
                + data.Fecha + "')";
            return consulta;
        }

        #endregion

        #region Update

        public static string UpdateUsuario(EntUsuario data)
        {
            string consulta = "UPDATE Usuario SET Nombre='" + data.Nombre + "',Apellido='" + data.Apellido + "',DNI='" + data.Dni + "'," +
                "Cargo='" + data.Cargo + "',Iniciales='" + data.Iniciales + "',Contrasenia='" + data.Contrasenia + "'," +
                "Rol='" + data.Rol + "' WHERE Id='" + DatosGlobales.Id_Usuario + "'";
            return consulta;
        }

        public static string UpdateSondaje(EntSondaje data)
        {
            string consulta = "UPDATE Sondaje SET Cliente='" + data.Cliente + "'," +
                "Proyecto='" + data.Proyecto + "',Geotecnico='" + data.Geotecnico + "',Nom_sondaje='" + data.Nom_sondaje + "'," +
                "Fecha='" + data.Fecha + "',Profundidad='" + data.Profundidad + "',Norte='" + data.Norte + "',Este='" + data.Este + "'," +
                "Cota='" + data.Cota + "',Maquina='" + data.Maquina + "',Linea_Core='" + data.Linea_Core + "'," +
                "Dip='" + data.Dip + "',Azimut='" + data.Azimut + "',State='" + data.State + "',Validar='" + data.Validar + "' WHERE Id='" + DatosGlobales.Id_Sondaje + "'";
            return consulta;
        }

        public static string UpdateCorrida(EntCorrida dat)
        {
            string consulta = "UPDATE `Corrida` SET `Desde`='" + dat.Desde + "',`Hasta`='" + dat.Hasta + "',`Perforacion`='" + dat.Perforacion +
                "',`Recuperacion`='" + dat.Recuperacion + "',Rqd='" + dat.Rqd + "',Lrf='" + dat.Lrf + "',Nfn='" + dat.Nfn +
                "',Litologia_1='" + dat.Litologia_1 + "',Litologia_2='" + dat.Litologia_2 + "',Re='" + dat.Re +
                "',TipoEstruct_1='" + dat.TipoEstruct_1 + "',TipoEstruct_2='" + dat.TipoEstruct_2 + "',Jn='" + dat.Jn +
                "',Jr='" + dat.Jr + "',Ja='" + dat.Ja + "',Jw='" + dat.Jw + "',Srf='" + dat.Srf +
                "',Q_Barton='" + dat.Q_Barton + "',Geotecnico='" + dat.Geotecnico + "',Comentario='" + dat.Comentario +
                "',Rmr='" + dat.Rmr + "',Adicional='" + dat.Adicional + "',Validar='" + dat.State +
                "',Validar='" + dat.Validar + "' WHERE `Id`='" + DatosGlobales.Id_Corrida + "'";
            return consulta;
        }

        public static string UpdateEstructura(EntEstructura data)
        {
            string consulta = "UPDATE `Estructura` SET `Num_Estructura`='" + data.Num_estructura + "',`Profundidad_e`='" + data.Profundidad_e + "',`Tipo_e`='" + data.Tipo_e +
                "',`T_B`='" + data.T_B + "',`Alpha`='" + data.Alpha + "',`Beta`='" + data.Beta + "',`Ref_to_top`='" + data.Ref_to_top + "'," +
                "`Cond_fractura`='" + data.Cond_fractura + "'," + "`Jrc`='" + data.Jrc + "',`Jr`='" + data.Jr + "',`Ja`='" + data.Ja + "'," +
                "`Espesor`='" + data.Espesor + "',`Relleno_1`='" + data.Relleno_1 + "',`Relleno_2`='" + data.Relleno_2 + "',`Comentario`='" + data.Comentario + "'," +
                "`Geotecnico`='" + data.Geotecnico + "',`FechaEs`='" + data.FechaEs + "',`Dip_cal`='" + data.Dip_cal + "',`Dip_dir_cal`='" + data.Dip_dir_cal + "'," +
                "`Dips`='" + data.Dips + "',`Strike`='" + data.Strike + "',`State`='" + data.State + "',Validar='" + data.Validar + "' WHERE Id ='" + DatosGlobales.Id_Estructura + "'";
            return consulta;
        }

        public static string UpdateSondajeSinc(EntSondaje data)
        {
            string consulta = "UPDATE Sondaje SET Cliente='" + data.Cliente + "'," +
                "Proyecto='" + data.Proyecto + "',Geotecnico='" + data.Geotecnico + "',Nom_sondaje='" + data.Nom_sondaje + "'," +
                "Fecha='" + data.Fecha + "',Profundidad='" + data.Profundidad + "',Norte='" + data.Norte + "',Este='" + data.Este + "'," +
                "Cota='" + data.Cota + "',Maquina='" + data.Maquina + "',Linea_Core='" + data.Linea_Core + "'," +
                "Dip='" + data.Dip + "',Azimut='" + data.Azimut + "',State='" + data.State + "',Validar='" + data.Validar + "' WHERE Id='" + data.Id + "'";
            return consulta;
        }

        public static string UpdateCorridaSinc(EntCorrida dat)
        {
            string consulta = "UPDATE `Corrida` SET `Desde`='" + dat.Desde + "',`Hasta`='" + dat.Hasta + "',`Perforacion`='" + dat.Perforacion + 
                "',`Recuperacion`='" + dat.Recuperacion + "',Rqd='" + dat.Rqd + "',Lrf='" + dat.Lrf + "',Nfn='" + dat.Nfn + 
                "',Litologia_1='" + dat.Litologia_1 + "',Litologia_2='" + dat.Litologia_2 + "',Re='" + dat.Re +
                "',TipoEstruct_1='" + dat.TipoEstruct_1 + "',TipoEstruct_2='" + dat.TipoEstruct_2 + "',Jn='" + dat.Jn +
                "',Jr='" + dat.Jr + "',Ja='" + dat.Ja + "',Jw='" + dat.Jw + "',Srf='" + dat.Srf + 
                "',Q_Barton='" + dat.Q_Barton + "',Geotecnico='" + dat.Geotecnico + "',Comentario='" + dat.Comentario + 
                "',Rmr='" + dat.Rmr + "',Adicional='" + dat.Adicional + "',Validar='" + dat.State + 
                "',Validar='" + dat.Validar + "' WHERE `Id`='" + dat.Id + "'";
            return consulta;
        }

        public static string UpdateEstructuraSinc(EntEstructura data)
        {
            string consulta = "UPDATE `Estructura` SET `Num_Estructura`='" + data.Num_estructura + "',`Profundidad_e`='" + data.Profundidad_e + "',`Tipo_e`='" + data.Tipo_e +
                "',`T_B`='" + data.T_B + "',`Alpha`='" + data.Alpha + "',`Beta`='" + data.Beta + "',`Ref_to_top`='" + data.Ref_to_top + "'," +
                "`Cond_fractura`='" + data.Cond_fractura + "'," + "`Jrc`='" + data.Jrc + "',`Jr`='" + data.Jr + "',`Ja`='" + data.Ja + "'," +
                "`Espesor`='" + data.Espesor + "',`Relleno_1`='" + data.Relleno_1 + "',`Relleno_2`='" + data.Relleno_2 + "',`Comentario`='" + data.Comentario + "'," +
                "`Geotecnico`='" + data.Geotecnico + "',`FechaEs`='" + data.FechaEs + "',`Dip_cal`='" + data.Dip_cal + "',`Dip_dir_cal`='" + data.Dip_dir_cal + "'," +
                "`Dips`='" + data.Dips + "',`Strike`='" + data.Strike + "',`State`='" + data.State + "',Validar='" + data.Validar + "' WHERE Id ='" + data.Id + "'";
            return consulta;
        }

        #endregion

        #region Delete

        public static string DeleteSondaje(string id)
        {
            string consulta = "DELETE FROM `Estructura` WHERE Estructura.Id_Sondaje ='" + id + "';" + "DELETE FROM `Corrida` WHERE Corrida.Id_Sondaje ='" + id + "';" + "DELETE FROM `Sondaje` WHERE Sondaje.Id ='" + id + "';";
            return consulta;
        }

        public static string DeleteCorrida(string id)
        {
            string consulta = "DELETE FROM `Estructura` WHERE Estructura.Id_Corrida ='" + id + "';" + "DELETE FROM `Corrida` WHERE Corrida.Id = '" + id + "';";
            return consulta;
        }

        public static string DeleteEstructura(string id)
        {
            string consulta = "DELETE FROM `Estructura` WHERE Estructura.Id ='" + id + "';";
            return consulta;
        }

        #endregion

    }
}
