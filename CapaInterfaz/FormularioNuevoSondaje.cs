﻿using CapaEntidad;
using CapaLogica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaInterfaz
{
    public partial class FormularioNuevoSondaje : Form
    {
        public FormularioNuevoSondaje(VistaLogueo vl)
        {
            InitializeComponent();
            txtGeotecnico.Text = DatosGlobales.Iniciales;
        }

        public delegate void UpdateDelegate(Object sender, UpdateEventArgs args);
        public event UpdateDelegate UpdateEventeHandler;
        public class UpdateEventArgs : EventArgs
        {
            public string Data { get; set; }
        }

        protected void Agregar()
        {
            UpdateEventArgs args = new UpdateEventArgs();
            UpdateEventeHandler.Invoke(this, args);
        }

        #region Eventos

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pbCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtProfundidad_Leave(object sender, EventArgs e)
        {
            Limitar_Decimales2(txtProfundidad);
        }

        private void txtNorte_Leave(object sender, EventArgs e)
        {
            Limitar_Decimales2(txtNorte);
        }

        private void txtEste_Leave(object sender, EventArgs e)
        {
            Limitar_Decimales2(txtEste);
        }

        private void txtCota_Leave(object sender, EventArgs e)
        {
            Limitar_Decimales2(txtCota);
        }

        private void txtDip_Leave(object sender, EventArgs e)
        {
            Limitar_Decimales2(txtDip);
        }

        private void txtAzimut_Leave(object sender, EventArgs e)
        {
            Limitar_Decimales2(txtAzimut);
        }

        private void txtProfundidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            string cadena = txtProfundidad.Text.ToString();
            string filtro = "1234567890";

            if (cadena.Length == 0)
            {
                filtro += "-";
            }
            else
            {
                if (cadena.Length > 0)
                {
                    filtro += ".";
                }
            }

            foreach (char caracter in filtro)
            {
                if (e.KeyChar == caracter)
                {
                    e.Handled = false;
                    break;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            if (e.KeyChar == '.' && cadena.Contains('.'))
            {
                e.Handled = true;
            }

            if (e.KeyChar == ',')
            {
                tips.SetToolTip(txtProfundidad, "No se permiten comas en el parametro Profundidad");
                e.Handled = true;
            }
        }

        private void txtNorte_KeyPress(object sender, KeyPressEventArgs e)
        {
            string cadena = txtNorte.Text.ToString();
            string filtro = "1234567890";

            if (cadena.Length == 0)
            {
                filtro += "-";
            }
            else
            {
                if (cadena.Length > 0)
                {
                    filtro += ".";
                }
            }

            foreach (char caracter in filtro)
            {
                if (e.KeyChar == caracter)
                {
                    e.Handled = false;
                    break;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            if (e.KeyChar == '.' && cadena.Contains('.'))
            {
                e.Handled = true;
            }

            if (e.KeyChar == ',')
            {
                tips.SetToolTip(txtNorte, "No se permiten comas en el parametro Norte");
                e.Handled = true;
            }
        }

        private void txtEste_KeyPress(object sender, KeyPressEventArgs e)
        {
            string cadena = txtEste.Text.ToString();
            string filtro = "1234567890";

            if (cadena.Length == 0)
            {
                filtro += "-";
            }
            else
            {
                if (cadena.Length > 0)
                {
                    filtro += ".";
                }
            }

            foreach (char caracter in filtro)
            {
                if (e.KeyChar == caracter)
                {
                    e.Handled = false;
                    break;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            if (e.KeyChar == '.' && cadena.Contains('.'))
            {
                e.Handled = true;
            }

            if (e.KeyChar == ',')
            {
                tips.SetToolTip(txtEste, "No se permiten comas en el parametro Este");
                e.Handled = true;
            }
        }

        private void txtCota_KeyPress(object sender, KeyPressEventArgs e)
        {
            string cadena = txtCota.Text.ToString();
            string filtro = "1234567890";

            if (cadena.Length == 0)
            {
                filtro += "-";
            }
            else
            {
                if (cadena.Length > 0)
                {
                    filtro += ".";
                }
            }

            foreach (char caracter in filtro)
            {
                if (e.KeyChar == caracter)
                {
                    e.Handled = false;
                    break;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            if (e.KeyChar == '.' && cadena.Contains('.'))
            {
                e.Handled = true;
            }

            if (e.KeyChar == ',')
            {
                tips.SetToolTip(txtAzimut, "No se permiten comas en el parametro azimut");
                e.Handled = true;
            }
        }

        private void txtDip_KeyPress(object sender, KeyPressEventArgs e)
        {
            string cadena = txtDip.Text.ToString();
            string filtro = "1234567890";

            if (cadena.Length == 0)
            {
                filtro += "-";
            }
            else
            {
                if (cadena.Length > 0)
                {
                    filtro += ".";
                }
            }

            foreach (char caracter in filtro)
            {
                if (e.KeyChar == caracter)
                {
                    e.Handled = false;
                    break;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            if (e.KeyChar == '.' && cadena.Contains('.'))
            {
                e.Handled = true;
            }

            if (e.KeyChar == ',')
            {
                tips.SetToolTip(txtDip, "No se permiten comas en el parametro Dip");
                e.Handled = true;
            }
        }

        private void txtAzimut_KeyPress(object sender, KeyPressEventArgs e)
        {
            string cadena = txtAzimut.Text.ToString();
            string filtro = "1234567890";

            if (cadena.Length == 0)
            {
                filtro += "-";
            }
            else
            {
                if (cadena.Length > 0)
                {
                    filtro += ".";
                }
            }

            foreach (char caracter in filtro)
            {
                if (e.KeyChar == caracter)
                {
                    e.Handled = false;
                    break;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            if (e.KeyChar == '.' && cadena.Contains('.'))
            {
                e.Handled = true;
            }

            if (e.KeyChar == ',')
            {
                tips.SetToolTip(txtAzimut, "No se permiten comas en el parametro azimut");
                e.Handled = true;
            }
        }
        
        private void btnCrear_Click(object sender, EventArgs e)
        {
            if (txtNombreSondaje.Text.Trim() != "")
            {
                if (txtProfundidad.Text.Trim() != "")
                {
                    if (verifica(txtNombreSondaje.Text.Trim()) == true)
                    {
                        if (txtDip.Text != "" && (double.Parse(txtDip.Text) > 90 || double.Parse(txtDip.Text) < -1))
                        {
                            MessageBox.Show("El rango del Dip va desde 0 a 90, no puedes ingresar este numero");
                            txtDip.Focus();
                        }
                        else
                        {
                            if(txtAzimut.Text != "" && (double.Parse(txtAzimut.Text) > 360 || double.Parse(txtAzimut.Text) < -1))
                            {
                                MessageBox.Show("El rango del Azimut va desde 0 a 360, no puedes ingresar este numero");
                                txtAzimut.Focus();
                            }
                            else
                            {
                                try
                                {
                                    EntSondaje s = new EntSondaje();
                                    s.Id_User = DatosGlobales.Id_Usuario;
                                    s.Cliente = txtCliente.Text.Trim();
                                    s.Proyecto = txtProyecto.Text.Trim();
                                    s.Geotecnico = txtGeotecnico.Text.Trim();
                                    s.Nom_sondaje = txtNombreSondaje.Text.Trim();
                                    s.Fecha = DateTime.Parse(dtpFecha.Text).ToShortDateString().ToString();
                                    s.Profundidad = double.Parse(txtProfundidad.Text.Trim());
                                    if (txtNorte.Text == "")
                                    {
                                        s.Norte = 0;
                                    }
                                    else
                                    {
                                        s.Norte = double.Parse(txtNorte.Text.Trim());
                                    }
                                    if (txtEste.Text == "")
                                    {
                                        s.Este = 0;
                                    }
                                    else
                                    {
                                        s.Este = double.Parse(txtEste.Text.Trim());
                                    }
                                    if (txtCota.Text == "")
                                    {
                                        s.Cota = 0;
                                    }
                                    else
                                    {
                                        s.Cota = double.Parse(txtCota.Text.Trim());
                                    }
                                    s.Maquina = txtMaquina.Text.Trim();
                                    s.Linea_Core = cboLineaCore.Text.Trim();
                                    if (txtDip.Text == "")
                                    {
                                        s.Dip = 0;
                                    }
                                    else
                                    {
                                        s.Dip = double.Parse(txtDip.Text.Trim());
                                    }
                                    if (txtAzimut.Text == "")
                                    {
                                        s.Azimut = 0;
                                    }
                                    else
                                    {
                                        s.Azimut = double.Parse(txtAzimut.Text.Trim());
                                    }
                                    s.State = "Nuevo";
                                    if (cbValidar.Checked == true)
                                    {
                                        s.Validar = 1;
                                    }
                                    else
                                    {
                                        s.Validar = 0;
                                    }
                                    LogSondaje.Instancia.InsertarSondaje(s);
                                    Agregar();
                                    this.Close();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Error" + ex);
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Este nombre de sondaje ya existe");
                    }
                }
                else
                {
                    MessageBox.Show("La profundidad de sondaje es obligatorio");
                }
            }
            else
            {
                MessageBox.Show("El nombre de sondaje es obligatorio");
            }

        }

        #endregion

        #region Utilidades

        public void Limitar_Decimales2(TextBox text)
        {
            int elimina = 0;
            string regreso = "";
            var reserva = text.Text;
            var texto = text.Text.ToArray();
            bool val = text.Text.Contains('.');
            if (val)
            {
                for (int i = 0; i < texto.Length; i++)
                {
                    if (texto[i] == '.')
                    {
                        elimina = i + 2;
                        break;
                    }
                }

                for (int i = texto.Length; i > elimina; i--)
                {
                    texto = texto.Where((source, index) => index != i).ToArray();
                }

                for (int i = 0; i < texto.Length; i++)
                {
                    regreso += texto[i].ToString();
                }
                text.Text = regreso;
            }
            else
            {
                text.Text = reserva;
            }
        }

        public bool verifica(string texto)
        {
            var lista = LogSondaje.Instancia.ListarSondajes();
            var verifica = lista.FirstOrDefault(s => s.Nom_sondaje == texto);
            if (verifica == null)
            {
                return true;
            }
            else
            {
                return false;
            }   
        }

        #endregion

    }
}
