﻿using CapaEntidad;
using CapaLogica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CapaInterfaz
{
    public partial class VistaEstadisticas : Form
    {
        List<EntSondaje> lista = LogSondaje.Instancia.ListarSondajes();
        EntSondaje sondaje;
        List<EntEstructura> estructuras;
        List<EntUsuario> usuarios = LogUsuario.Instancia.ListarUsuario();
        List<EntCorrida> corridas;
        

        string[] userarray;
        string[] tipoEstruc = new string[17] { "Jn","Vn","Vnll","F-10","F+10","Fl","Sr","Js","Fo","Bd","Cn","Dk","Sc",
            "Uc","Bh","Se","-1" };
        double[] sumas;
        int[] numEstruturas;
        int[] numeros;

        public VistaEstadisticas()
        {
            InitializeComponent();
            
            foreach (EntSondaje temp in lista)
            {
                cboSondaje.Items.Add(temp.Nom_sondaje);
            }
            DateTime v2 = new DateTime(2001, 02, 18);
            int dia = (int)v2.DayOfWeek;
            int x = System.Globalization.CultureInfo.CurrentUICulture.Calendar.GetWeekOfYear(v2, CalendarWeekRule.FirstDay, v2.DayOfWeek);
            MessageBox.Show("Fecha:" + v2 + " Dia: " + dia +" Semana:" + x.ToString());
        }

        private void grafTipoEstructura()
        {
            numEstruturas = new int[17];
            string nomSondaje = cboSondaje.Text.ToString();
            if (nomSondaje == "")
            {
                MessageBox.Show("Debe seleccionar un sondaje");
            }
            else
            {
                sondaje = lista.FirstOrDefault(s => s.Nom_sondaje == nomSondaje);
                estructuras = LogEstructura.Instancia.ListarEstructurasSondaje(sondaje.Id);
                var listan = estructuras.Where(e => e.Id_Usuario == DatosGlobales.Id_Usuario);
                foreach (EntEstructura estructura in listan)
                {
                    llenarNumEstruturas(estructura.Tipo_e);
                }
            }
        }

        private void llenarNumEstruturas(string tipo)
        {
            switch (tipo)
            {
                case "Jn":
                    numEstruturas[0] += 1;
                    break;
                case "Vn":
                    numEstruturas[1] += 1;
                    break;
                case "Vnll":
                    numEstruturas[2] += 1;
                    break;
                case "F - 10":
                    numEstruturas[3] += 1;
                    break;
                case "F + 10":
                    numEstruturas[4] += 1;
                    break;
                case "Fl":
                    numEstruturas[5] += 1;
                    break;
                case "Sr":
                    numEstruturas[6] += 1;
                    break;
                case "Js":
                    numEstruturas[7] += 1;
                    break;
                case "Fo":
                    numEstruturas[8] += 1;
                    break;
                case "Bd":
                    numEstruturas[9] += 1;
                    break;
                case "Cn":
                    numEstruturas[10] += 1;
                    break;
                case "Dk":
                    numEstruturas[11] += 1;
                    break;
                case "Sc":
                    numEstruturas[12] += 1;
                    break;
                case "Uc":
                    numEstruturas[13] += 1;
                    break;
                case "Bh":
                    numEstruturas[14] += 1;
                    break;
                case "Se":
                    numEstruturas[15] += 1;
                    break;
                case "- 1":
                    numEstruturas[16] += 1;
                    break;






            }
        }

        //public void Obtener_Datos(ComboBox data)
        //{
        //    int i = 0;
        //    string nombre = data.Text;
        //    userarray = new string[usuarios.Count];
        //    sondaje = lista.FirstOrDefault(s => s.Nom_sondaje == nombre);
        //    foreach (EntUsuario user in usuarios)
        //    {
        //        userarray[i] = user.Iniciales;
        //        i = i + 1;
        //    }
            
            
        //}

        private void btnConsulta_Click(object sender, EventArgs e)
        {
            if (cboSondaje.Text == "")
            {
                MessageBox.Show("Debe seleccionar un sondaje");
            }
            else
            {
                grafTipoEstructura();
                //Obtener_Datos(cboSondaje);
                CargarGrafico1();
                //CargarGrafico2();
            }
        }

        public void CargarGrafico1()
        {
            EstructurasSondaje.Series.Clear();
            EstructurasSondaje.Titles.Clear();
            EstructurasSondaje.Titles.Add("Estructuras encontradas por sondaje");
            

            for (int i = 0; i < tipoEstruc.Length; i++)
            {
                Series serie = EstructurasSondaje.Series.Add(tipoEstruc[i]);

                serie.Label = numEstruturas[i].ToString();

                serie.Points.AddXY(tipoEstruc[i], numEstruturas[i]);
            }
        }

        //public void CargarGrafico2()
        //{
        //    int j = 0;
        //    numeros = new int[usuarios.Count];
        //    estructuras = LogEstructura.Instancia.ListarEstructurasSondaje(sondaje.Id);
        //    foreach (EntUsuario usuario in usuarios)
        //    {
        //        var listE = estructuras.Where(C => C.Id_Usuario == usuario.Id);
        //        if (listE != null)
        //        {
        //            numeros[j] = listE.Count();
        //            j++;
        //        }
        //    }
        //    AvanceGeotecnico.Series.Clear();
        //    AvanceGeotecnico.Titles.Clear();
        //    AvanceGeotecnico.Titles.Add("Estructuras por geotécnico");            
        //    for (int i = 0; i < userarray.Length; i++)
        //    {
        //        Series serie = AvanceGeotecnico.Series.Add(userarray[i]);

        //        serie.Label = numeros[i].ToString();

        //        serie.Points.Add(numeros[i]);
        //    }
        //}
    }
}
