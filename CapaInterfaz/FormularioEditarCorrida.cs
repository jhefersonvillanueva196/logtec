﻿using CapaEntidad;
using CapaLogica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaInterfaz
{
    public partial class FormularioEditarCorrida : Form
    {
        public FormularioEditarCorrida(VistaLogueo vl)
        {
            InitializeComponent();
            if (DatosGlobales.Min == 0)
            {
                txtDesde.Enabled = false;
            }
            else
            {
                txtDesde.Enabled = true;
            }
        }

        public delegate void UpdateDelegate(Object sender, UpdateEventArgs args);
        public event UpdateDelegate UpdateEventeHandler;
        public class UpdateEventArgs : EventArgs
        {
            public string Data { get; set; }
        }

        protected void Agregar()
        {
            UpdateEventArgs args = new UpdateEventArgs();
            UpdateEventeHandler.Invoke(this, args);
        }

        #region Eventos

        private void pbCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtDesde_Leave(object sender, EventArgs e)
        {
            double perfo = 0;
            Limitar_Decimales2(txtDesde);
            try
            {
                perfo = double.Parse(txtHasta.Text) - double.Parse(txtDesde.Text);
                txtPerforacion.Text = perfo.ToString();
            }
            catch (Exception)
            {

            }
        }

        private void txtHasta_Leave(object sender, EventArgs e)
        {
            double perfo = 0;
            Limitar_Decimales2(txtHasta);
            try
            {
                perfo = double.Parse(txtHasta.Text) - double.Parse(txtDesde.Text);
                txtPerforacion.Text = perfo.ToString();
            }
            catch(Exception)
            {

            }
        }

        private void txtRecuperacion_Leave(object sender, EventArgs e)
        {
            Cambia_comas(txtRecuperacion);
            Limitar_Decimales2(txtRecuperacion);
        }
        private void txtDesde_KeyPress(object sender, KeyPressEventArgs e)
        {
            string cadena = txtDesde.Text.ToString();
            string filtro = "1234567890";

            if (cadena.Length == 0)
            {
                filtro += "-";
            }
            else
            {
                if (cadena.Length > 0)
                {
                    filtro += ".";
                }
            }

            foreach (char caracter in filtro)
            {
                if (e.KeyChar == caracter)
                {
                    e.Handled = false;
                    break;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            if (e.KeyChar == '.' && cadena.Contains('.'))
            {
                e.Handled = true;
            }

            if (e.KeyChar == ',')
            {
                tips.SetToolTip(txtDesde, "No se permiten comas en este parametro");
                e.Handled = true;
            }
        }

        private void txtHasta_KeyPress(object sender, KeyPressEventArgs e)
        {
            string cadena = txtHasta.Text.ToString();
            string filtro = "1234567890";

            if (cadena.Length == 0)
            {
                filtro += "-";
            }
            else
            {
                if (cadena.Length > 0)
                {
                    filtro += ".";
                }
            }

            foreach (char caracter in filtro)
            {
                if (e.KeyChar == caracter)
                {
                    e.Handled = false;
                    break;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            if (e.KeyChar == '.' && cadena.Contains('.'))
            {
                e.Handled = true;
            }

            if (e.KeyChar == ',')
            {
                tips.SetToolTip(txtHasta, "No se permiten comas en este parametro");
                e.Handled = true;
            }
        }

        private void txtRecuperacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            string cadena = txtRecuperacion.Text.ToString();
            string filtro = "1234567890";

            if (cadena.Length == 0)
            {
                filtro += "-";
            }
            else
            {
                if (cadena.Length > 0)
                {
                    filtro += ".";
                }
            }

            foreach (char caracter in filtro)
            {
                if (e.KeyChar == caracter)
                {
                    e.Handled = false;
                    break;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            if (e.KeyChar == '.' && cadena.Contains('.'))
            {
                e.Handled = true;
            }

            if (e.KeyChar == ',')
            {
                tips.SetToolTip(txtRecuperacion, "No se permiten comas en este parametro");
                e.Handled = true;
            }
        }


        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if(txtDesde.Text == "" || txtHasta.Text == "")
            {
                MessageBox.Show("Debe ingresar los limites de la corrida");
                txtDesde.Focus();
            }
            else
            {
                if(txtRecuperacion.Text == "")
                {
                    MessageBox.Show("Debes ingresar los datos de la recuperacion");
                    txtRecuperacion.Focus();
                }
                else
                {
                    if (double.Parse(txtDesde.Text.Trim()) >= DatosGlobales.Profundidad)
                    {
                        MessageBox.Show("La corrida no puede tener un inicio mayor o igual a la profundidad del sondaje");
                    }
                    else
                    {
                        if (double.Parse(txtHasta.Text.Trim()) > DatosGlobales.Profundidad)
                        {
                            MessageBox.Show("La corrida no puede tener un final mayor a la profundidad del sondaje");
                        }
                        else
                        {
                            if (double.Parse(txtPerforacion.Text) < double.Parse(txtRecuperacion.Text))
                            {
                                MessageBox.Show("La recuperacion no puede ser mayor a la perforacion");
                            }
                            else
                            {
                                if (DatosGlobales.ProUltCorrida > double.Parse(txtDesde.Text))
                                {
                                    MessageBox.Show("La corrida no puede iniciar en medio de otra, verifica tus datos");
                                }
                                else
                                {
                                    if (double.Parse(txtDesde.Text) > double.Parse(txtHasta.Text))
                                    {
                                        MessageBox.Show("El inicio de la corrida no puede ser mayor al final");
                                    }
                                    else
                                    {
                                        try
                                        {
                                            EntCorrida c = new EntCorrida();
                                            c.Desde = double.Parse(txtDesde.Text.Trim());
                                            c.Hasta = double.Parse(txtHasta.Text.Trim());
                                            c.Perforacion = double.Parse(txtHasta.Text.Trim()) - double.Parse(txtDesde.Text.Trim());
                                            c.Recuperacion = double.Parse(txtRecuperacion.Text.Trim());
                                            c.State = "Editado";
                                            if (cbValidar.Checked == true)
                                            {
                                                c.Validar = 1;
                                            }
                                            else
                                            {
                                                c.Validar = 0;
                                            }
                                            var usuario = LogUsuario.Instancia.Usuario();
                                            EntAuditoria a = new EntAuditoria();
                                            a.Id_User = usuario.Id;
                                            a.Nombre = usuario.Nombre;
                                            a.Apellido = usuario.Apellido;
                                            a.Elemento = DatosGlobales.Id_Corrida;
                                            a.Accion = "Editado";
                                            a.Fecha = DateTime.Now.ToLongDateString();
                                            LogCorrida.Instancia.EditarCorrida(c);
                                            LogAuditoria.Instancia.InsertarAuditoria(a);
                                            Agregar();
                                            this.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show("Error" + ex);
                                        }
                                    }
                                }
                            }
                        }
                    }
            }
            
            }
        }

        #endregion

        #region Utilidades

        public void Cambia_comas(TextBox text)
        {
            var nuevo = "";
            var aux = text.Text;
            bool val = text.Text.Contains(',');
            if (val)
            {
                nuevo = text.Text.Trim();
                nuevo = nuevo.Replace(',', '.');
                text.Text = nuevo;
            }
            else
            {
                text.Text = aux;
            }
        }

        public void Limitar_Decimales2(TextBox text)
        {
            int elimina = 0;
            string regreso = "";
            var reserva = text.Text;
            var texto = text.Text.ToArray();
            bool val = text.Text.Contains('.');
            if (val)
            {
                for (int i = 0; i < texto.Length; i++)
                {
                    if (texto[i] == '.')
                    {
                        elimina = i + 2;
                        break;
                    }
                }

                for (int i = texto.Length; i > elimina; i--)
                {
                    texto = texto.Where((source, index) => index != i).ToArray();
                }

                for (int i = 0; i < texto.Length; i++)
                {
                    regreso += texto[i].ToString();
                }
                text.Text = regreso;
            }
            else
            {
                text.Text = reserva;
            }
        }

        #endregion

        
    }
}
