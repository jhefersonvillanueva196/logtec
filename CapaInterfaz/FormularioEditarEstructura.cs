﻿using CapaEntidad;
using CapaLogica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaInterfaz
{
    public partial class FormularioEditarEstructura : Form
    {
        Ayuda a;
        string nombre;
        double y;

        public FormularioEditarEstructura(VistaLogueo vl)
        {
            InitializeComponent();
            txtDesde.Text = DatosGlobales.Min.ToString();
            txtHasta.Text = DatosGlobales.Max.ToString();
        }

        public delegate void UpdateDelegate(Object sender, UpdateEventArgs args);
        public event UpdateDelegate UpdateEventeHandler;
        public class UpdateEventArgs : EventArgs
        {
            public string Data { get; set; }
        }

        protected void Agregar()
        {
            UpdateEventArgs args = new UpdateEventArgs();
            UpdateEventeHandler.Invoke(this, args);
        }

        #region Eventos

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (txtProfundidadEstructura.Text != "")
            {
                if (double.Parse(txtProfundidadEstructura.Text) < DatosGlobales.Min || double.Parse(txtProfundidadEstructura.Text) > DatosGlobales.Max)
                {
                    MessageBox.Show("La profundidad esta fuera de los limites de la corrida");
                    txtProfundidadEstructura.Focus();
                }
                else
                {
                    if (verifica(double.Parse(txtProfundidadEstructura.Text)) == false)
                    {
                        MessageBox.Show("Esta profundidad ya esta registrada, verifica tus datos");
                        txtProfundidadEstructura.Focus();
                    }
                    else
                    {
                        if (cboTipoEstructura.Text == "")
                        {
                            MessageBox.Show("El parametro de tipo de estrcutura es obligatorio");
                        }
                        else
                        {
                            if ((txtTB.Text != "0" && txtTB.Text != "1") && txtTB.Text != "")
                            {
                                MessageBox.Show("No puedes ingresar este dato TB solo acepta 0 y 1");
                                txtTB.Focus();
                            }
                            else
                            {
                                if ((txtRefToTop.Text != "360" && txtRefToTop.Text != "180" && txtRefToTop.Text != "0") && txtRefToTop.Text != "")
                                {
                                    MessageBox.Show("Ref To Top solo acepta 0, 180 y 360");
                                    txtRefToTop.Focus();
                                }
                                else
                                {
                                    if (txtAlpha.Text != "" && (double.Parse(txtAlpha.Text.Trim()) > 90 || double.Parse(txtAlpha.Text.Trim()) < -1))
                                    {
                                        MessageBox.Show("El rango de Alpha va desde 0 a 90, no puedes ingresar este numero");
                                        txtAlpha.Focus();
                                    }
                                    else
                                    {
                                        if (txtBeta.Text != "" && (double.Parse(txtBeta.Text.Trim()) > 360 || double.Parse(txtBeta.Text.Trim()) < -1))
                                        {
                                            MessageBox.Show("El rango de Beta va desde 0 a 360, no puedes ingresar este numero");
                                            txtBeta.Focus();
                                        }
                                        else
                                        {
                                            try
                                            {
                                                EntEstructura es = new EntEstructura();
                                                es.Id_Sondaje = DatosGlobales.Id_Sondaje;
                                                es.Id_Corrida = DatosGlobales.Id_Corrida;
                                                es.Id_Usuario = DatosGlobales.Id_Usuario;
                                                es.Num_estructura = txtNumeroEstructura.Text.Trim();
                                                es.Profundidad_e = double.Parse(txtProfundidadEstructura.Text.Trim());
                                                es.Tipo_e = cboTipoEstructura.Text.ToString();
                                                if (txtTB.Text == "")
                                                {
                                                    es.T_B = 0;
                                                }
                                                else
                                                {
                                                    es.T_B = double.Parse(txtTB.Text.Trim());
                                                }
                                                if (txtAlpha.Text == "")
                                                {
                                                    es.Alpha = 0;
                                                }
                                                else
                                                {
                                                    es.Alpha = double.Parse(txtAlpha.Text.Trim());
                                                }
                                                if (txtBeta.Text == "")
                                                {
                                                    es.Beta = 0;
                                                }
                                                else
                                                {
                                                    es.Beta = double.Parse(txtBeta.Text.Trim());
                                                }
                                                if (txtRefToTop.Text == "")
                                                {
                                                    es.Ref_to_top = 0;
                                                }
                                                else
                                                {
                                                    es.Ref_to_top = double.Parse(txtRefToTop.Text.Trim());
                                                }
                                                if (cboCondicionFractura.Text == "")
                                                {
                                                    es.Cond_fractura = 0;
                                                }
                                                else
                                                {
                                                    es.Cond_fractura = double.Parse(cboCondicionFractura.Text.Trim());
                                                }
                                                if (cboJrc.Text == "")
                                                {
                                                    es.Jrc = 1;
                                                }
                                                else
                                                {
                                                    es.Jrc = double.Parse(cboJrc.Text.Trim());
                                                }
                                                if (cboJr.Text == "")
                                                {
                                                    es.Jr = 0.5;
                                                }
                                                else
                                                {
                                                    es.Jr = double.Parse(cboJr.Text.Trim());
                                                }
                                                if (cboJa.Text == "")
                                                {
                                                    es.Ja = 0.75;
                                                }
                                                else
                                                {
                                                    es.Ja = double.Parse(cboJa.Text.Trim());
                                                }
                                                if (txtEspesor.Text == "")
                                                {
                                                    es.Espesor = 0;
                                                }
                                                else
                                                {
                                                    es.Espesor = double.Parse(txtEspesor.Text.Trim());
                                                }
                                                es.Relleno_1 = cboRelleno1.Text.ToString();
                                                es.Relleno_2 = cboRelleno2.Text.ToString();
                                                es.Comentario = txtComentario.Text.Trim();
                                                es.Geotecnico = DatosGlobales.Iniciales;
                                                es.FechaEs = txtFecha.Text.Trim();
                                                if (txtAlpha.Text.Trim() == "-1")
                                                {
                                                    es.Dip_cal = -1;
                                                }
                                                else
                                                {
                                                    if (Math.Round(Calculos()) > 90)
                                                    {
                                                        es.Dip_cal = 180 - Math.Round(Calculos());
                                                    }
                                                    else
                                                    {
                                                        es.Dip_cal = Math.Round(Calculos());
                                                    }
                                                }
                                                if (txtBeta.Text.Trim() == "-1")
                                                {
                                                    es.Dip_dir_cal = -1;
                                                }
                                                else
                                                {
                                                    if (Math.Round(y) > 360)
                                                    {
                                                        es.Dip_dir_cal = Math.Round(y) - 360;
                                                    }
                                                    else
                                                    {
                                                        es.Dip_dir_cal = Math.Round(y);
                                                    }

                                                }
                                                if (txtAlpha.Text.Trim() == "-1")
                                                {
                                                    es.Dips = -1;
                                                }
                                                else
                                                {
                                                    es.Dips = es.Dip_cal;
                                                }
                                                if (txtBeta.Text.Trim() == "-1")
                                                {
                                                    es.Strike = -1;
                                                }
                                                else
                                                {
                                                    if (es.Dip_dir_cal < 180)
                                                    {
                                                        es.Strike = Math.Abs(es.Dip_dir_cal - 90);
                                                    }
                                                    else
                                                    {
                                                        if (es.Dip_dir_cal > 180)
                                                        {
                                                            double temp = es.Dip_dir_cal + 90;
                                                            if (temp > 360)
                                                            {
                                                                es.Strike = temp - 360;
                                                            }
                                                            else
                                                            {
                                                                es.Strike = temp;
                                                            }
                                                        }
                                                    }
                                                }
                                                es.State = "Editado";
                                                if (cbValidar.Checked == true)
                                                {
                                                    es.Validar = 1;
                                                }
                                                else
                                                {
                                                    es.Validar = 0;
                                                }
                                                var usuario = LogUsuario.Instancia.Usuario();
                                                EntAuditoria a = new EntAuditoria();
                                                a.Id_User = usuario.Id;
                                                a.Nombre = usuario.Nombre;
                                                a.Apellido = usuario.Apellido;
                                                a.Elemento = DatosGlobales.Id_Estructura;
                                                a.Accion = "Editado";
                                                a.Fecha = DateTime.Now.ToLongDateString();
                                                LogAuditoria.Instancia.InsertarAuditoria(a);
                                                LogEstructura.Instancia.EditarEstructura(es);
                                                Agregar();
                                                this.Close();
                                            }
                                            catch (Exception ex)
                                            {
                                                MessageBox.Show("Error" + ex);
                                            }
                                        }
                                    }
                                }
                            }
                        }                        
                    }
                }
            }
            else
            {
                MessageBox.Show("El campo profundiddad es obligatorio");
                txtProfundidadEstructura.Focus();
            }
        }

        private void txtAlpha_Leave(object sender, EventArgs e)
        {
            Limitar_Decimales2(txtAlpha);
        }

        private void txtBeta_Leave(object sender, EventArgs e)
        {
            Limitar_Decimales2(txtBeta);
        }

        private void txtProfundidadEstructura_Leave(object sender, EventArgs e)
        {
            Limitar_Decimales2(txtProfundidadEstructura);
        }

        private void txtEspesor_Leave(object sender, EventArgs e)
        {
            if (txtEspesor.Text != " ")
            {
                Limitar_Decimales2(txtEspesor);
            }
        }

        private void pbAlpha_Click(object sender, EventArgs e)
        {
            nombre = pbAlpha.Name;
            a = new Ayuda(nombre);
            pbAlpha.Focus();
            a.Show();
        }

        private void pbBeta_Click(object sender, EventArgs e)
        {
            nombre = pbBeta.Name;
            a = new Ayuda(nombre);
            pbBeta.Focus();
            a.Show();
        }

        private void pbCondicionFractura_Click(object sender, EventArgs e)
        {
            nombre = pbCondicionFractura.Name;
            pbCondicionFractura.Focus();
            a = new Ayuda(nombre);
            a.Show();
        }

        private void pbJrc_Click(object sender, EventArgs e)
        {
            nombre = pbJrc.Name;
            pbJrc.Focus();
            a = new Ayuda(nombre);
            a.Show();
        }

        private void pbJr_Click(object sender, EventArgs e)
        {
            nombre = pbJr.Name;
            a = new Ayuda(nombre);
            pbJr.Focus();
            a.Show();
        }

        private void pbJa_Click(object sender, EventArgs e)
        {
            nombre = pbJa.Name;
            a = new Ayuda(nombre);
            pbJa.Focus();
            a.Show();
        }

        private void pbRelleno1_Click(object sender, EventArgs e)
        {
            nombre = pbRelleno1.Name;
            a = new Ayuda(nombre);
            pbRelleno1.Focus();
            a.Show();
        }

        private void pbRelleno2_Click(object sender, EventArgs e)
        {
            nombre = pbRelleno2.Name;
            a = new Ayuda(nombre);
            pbRelleno2.Focus();
            a.Show();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pbCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtProfundidadEstructura_KeyPress(object sender, KeyPressEventArgs e)
        {
            string cadena = txtProfundidadEstructura.Text.ToString();
            string filtro = "1234567890";

            if (cadena.Length == 0)
            {
                filtro += "-";
            }
            else
            {
                if (cadena.Length > 0)
                {
                    filtro += ".";
                }
            }

            foreach (char caracter in filtro)
            {
                if (e.KeyChar == caracter)
                {
                    e.Handled = false;
                    break;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            if (e.KeyChar == '.' && cadena.Contains('.'))
            {
                e.Handled = true;
            }

            if (e.KeyChar == ',')
            {
                tips.SetToolTip(txtProfundidadEstructura, "No se permiten comas en este parametro");
                e.Handled = true;
            }
        }
        
        private void txtAlpha_KeyPress(object sender, KeyPressEventArgs e)
        {
            string cadena = txtAlpha.Text.ToString();
            string filtro = "1234567890";

            if (cadena.Length == 0)
            {
                filtro += "-";
            }
            else
            {
                if (cadena.Length > 0)
                {
                    filtro += ".";
                }
            }

            foreach (char caracter in filtro)
            {
                if (e.KeyChar == caracter)
                {
                    e.Handled = false;
                    break;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            if (e.KeyChar == '.' && cadena.Contains('.'))
            {
                e.Handled = true;
            }

            if (e.KeyChar == ',')
            {
                tips.SetToolTip(txtAlpha, "No se permiten comas en este parametro");
                e.Handled = true;
            }
        }

        private void txtBeta_KeyPress(object sender, KeyPressEventArgs e)
        {
            string cadena = txtBeta.Text.ToString();
            string filtro = "1234567890";

            if (cadena.Length == 0)
            {
                filtro += "-";
            }
            else
            {
                if (cadena.Length > 0)
                {
                    filtro += ".";
                }
            }

            foreach (char caracter in filtro)
            {
                if (e.KeyChar == caracter)
                {
                    e.Handled = false;
                    break;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            if (e.KeyChar == '.' && cadena.Contains('.'))
            {
                e.Handled = true;
            }

            if (e.KeyChar == ',')
            {
                tips.SetToolTip(txtBeta, "No se permiten comas en este parametro");
                e.Handled = true;
            }
        }

        private void txtEspesor_KeyPress(object sender, KeyPressEventArgs e)
        {
            string cadena = txtEspesor.Text.ToString();
            string filtro = "1234567890";

            if (cadena.Length == 0)
            {
                filtro += "-";
            }
            else
            {
                if (cadena.Length > 0)
                {
                    filtro += ".";
                }
            }

            foreach (char caracter in filtro)
            {
                if (e.KeyChar == caracter)
                {
                    e.Handled = false;
                    break;
                }
                else
                {
                    e.Handled = true;
                }
            }

            if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            if (e.KeyChar == '.' && cadena.Contains('.'))
            {
                e.Handled = true;
            }

            if (e.KeyChar == ',')
            {
                tips.SetToolTip(txtEspesor, "No se permiten comas en este parametro");
                e.Handled = true;
            }
        }

        #endregion

        #region Utilidades

        public double Calculos()
        {
            double a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x;
            double DR = Math.PI / 180;
            a = 90 - DatosGlobales.Dip;
            if (double.Parse(txtTB.Text.Trim()) == 0)
            {
                b = double.Parse(txtBeta.Text.Trim()) + double.Parse(txtRefToTop.Text.Trim());
            }
            else
            {
                b = double.Parse(txtBeta.Text.Trim()) - double.Parse(txtRefToTop.Text.Trim());
            }
            if (b >= 360)
            {
                c = b - 360;
            }
            else
            {
                c = b;
            }
            if (c < 0)
            {
                d = c + 360;
            }
            else
            {
                d = c;
            }
            if (double.Parse(txtTB.Text.Trim()) == 1)
            {
                e = 180 - d;
            }
            else
            {
                e = d;
            }
            if (e < 0)
            {
                f = 360 + e;
            }
            else
            {
                f = e;
            }
            if (f >= 360)
            {
                g = f - 360;
            }
            else
            {
                g = f;
            }
            h = g + 180;
            if (h >= 360)
            {
                i = h - 360;
            }
            else
            {
                i = h;
            }
            j = 90 - double.Parse(txtAlpha.Text.Trim());
            k = Math.Sin(i * DR);
            l = Math.Cos(i * DR);
            m = Math.Sin(j * DR);
            n = Math.Cos(j * DR);
            o = (l * m * Math.Cos(a * DR) + n * Math.Sin(a * DR)) * Math.Cos(DatosGlobales.Azimut * DR) - k * m * Math.Sin(DatosGlobales.Azimut * DR);
            p = k * m * Math.Cos(DatosGlobales.Azimut * DR) + (l * m * Math.Cos(a * DR) + n * Math.Sin(a * DR)) * Math.Sin(DatosGlobales.Azimut * DR);
            q = n * Math.Cos(a * DR) - l * m * Math.Sin(a * DR);
            r = Math.Sqrt(Math.Pow(o, 2) + Math.Pow(p, 2) + Math.Pow(q, 2));
            s = o / r;
            t = p / r;
            u = q / r;
            //double temp = Math.Atan(t / s);
            v = (Math.Abs(Math.Atan(t / s)) / DR);
            if (t < 0)
            {
                if (s < 0)
                {
                    w = v;
                }
                else
                {
                    w = 180 - v;
                }
            }
            else
            {
                if (s < 0)
                {
                    w = 360 - v;
                }
                else
                {
                    w = v + 180;
                }
            }
            x = (Math.Acos(u)) / DR;
            if (x > 90)
            {
                y = w + 180;
            }
            else
            {
                y = w;
            }
            return x;
        }

        public void Limitar_Decimales2(TextBox text)
        {
            int elimina = 0;
            string regreso = "";
            var reserva = text.Text;
            var texto = text.Text.ToArray();
            bool val = text.Text.Contains('.');
            if (val)
            {
                for (int i = 0; i < texto.Length; i++)
                {
                    if (texto[i] == '.')
                    {
                        elimina = i + 2;
                        break;
                    }
                }

                for (int i = texto.Length; i > elimina; i--)
                {
                    texto = texto.Where((source, index) => index != i).ToArray();
                }

                for (int i = 0; i < texto.Length; i++)
                {
                    regreso += texto[i].ToString();
                }
                text.Text = regreso;
            }
            else
            {
                text.Text = reserva;
            }
        }

        public bool verifica(double profundidad)
        {
            var lista = LogEstructura.Instancia.ListarEstructuras();
            var band = lista.FirstOrDefault(s => s.Id == DatosGlobales.Id_Estructura);
            if (band.Profundidad_e != profundidad)
            {
                var verifica = lista.FirstOrDefault(s => s.Profundidad_e == profundidad);
                if (verifica == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

        }

        #endregion

        private void cboTipoEstructura_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cboTipoEstructura.Text.ToString() == "-1")
            {
                txtAlpha.Text = "-1";
                txtBeta.Text = "-1";
                cboCondicionFractura.Text = "-1";
                cboJrc.Text = "-1";
                cboJr.Text = "-1";
                cboJa.Text = "-1";
                txtEspesor.Text = "-1";
                cboRelleno1.Text = "-1";
                cboRelleno2.Text = "-1";

                txtAlpha.Enabled = false;
                txtBeta.Enabled = false;
                cboCondicionFractura.Enabled = false;
                cboJrc.Enabled = false;
                cboJr.Enabled = false;
                cboJa.Enabled = false;
                txtEspesor.Enabled = false;
                cboRelleno1.Enabled = false;
                cboRelleno2.Enabled = false;
            }
            else
            {
                txtAlpha.Text = "";
                txtBeta.Text = "";
                cboCondicionFractura.Text = "";
                cboJrc.Text = "";
                cboJr.Text = "";
                cboJa.Text = "";
                txtEspesor.Text = "";
                cboRelleno1.Text = "";
                cboRelleno2.Text = "";

                txtAlpha.Enabled = true;
                txtBeta.Enabled = true;
                cboCondicionFractura.Enabled = true;
                cboJrc.Enabled = true;
                cboJr.Enabled = true;
                cboJa.Enabled = true;
                txtEspesor.Enabled = true;
                cboRelleno1.Enabled = true;
                cboRelleno2.Enabled = true;
            }
        }
    }
}