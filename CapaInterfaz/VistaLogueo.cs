﻿using CapaEntidad;
using CapaLogica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaInterfaz
{
    public partial class VistaLogueo : Form
    {
        string id_Usuario;
        public VistaLogueo()
        {
            InitializeComponent();
            ListarSondaje();
        }

        #region UtilidadDinamismo

        public void ListarSondaje()
        {
            dgvSondaje.DataSource = LogSondaje.Instancia.ListarSondajes();
        }

        public void ListarCorrida()
        {
            dgvCorrida.DataSource = LogCorrida.Instancia.ListarCorridas();
        }

        public void ListarEstructura()
        {
            dgvEstructura.DataSource = LogEstructura.Instancia.ListarEstructuras();
        }

        private void AgregUpdateEventHandlerSondaje(object sender,FormularioNuevoSondaje.UpdateEventArgs args)
        {
            ListarSondaje();
        }

        private void EditUpdateEventHandlerSondaje(object sender, FormularioEditarSondaje.UpdateEventArgs args)
        {
            ListarSondaje();
        }

        private void AgregUpdateEventHandlerCorrida(object sender, FormularioNuevaCorrida.UpdateEventArgs args)
        {
            ListarCorrida();
        }

        private void EditUpdateEventHandlerCorrida(object sender, FormularioEditarCorrida.UpdateEventArgs args)
        {
            ListarCorrida();
        }

        private void AgregUpdateEventHandlerEstructura(object sender, FormularioNuevaEstructura.UpdateEventArgs args)
        {
            ListarEstructura();
        }

        private void EditUpdateEventHandlerEstructura(object sender, FormularioEditarEstructura.UpdateEventArgs args)
        {
            ListarEstructura();
        }

        private void pbCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region Sondaje

        private void btnNuevoSondaje_Click(object sender, EventArgs e)
        {
            var lista = LogSondaje.Instancia.ListarSondajes();
            if (lista.Count < 5)
            {
                FormularioNuevoSondaje ns = new FormularioNuevoSondaje(this);
                ns.UpdateEventeHandler += AgregUpdateEventHandlerSondaje;
                ns.Show();
            }
            else
            {
                MessageBox.Show("Ha llegado al limite de sondajes registrables, contactese con su proveedor para adquirir una licensia");
            }
        }

        private void btnEditarSondaje_Click(object sender, EventArgs e)
        {
            if (dgvSondaje.Rows.Count > 0)
            {
                int i = dgvSondaje.CurrentRow.Index;
                FormularioEditarSondaje es = new FormularioEditarSondaje(this);
                es.txtCliente.Text = dgvSondaje.Rows[i].Cells[2].Value.ToString();
                es.txtProyecto.Text = dgvSondaje.Rows[i].Cells[3].Value.ToString();
                es.txtGeotecnico.Text = dgvSondaje.Rows[i].Cells[4].Value.ToString();
                es.txtNombreSondaje.Text = dgvSondaje.Rows[i].Cells[5].Value.ToString();
                es.dtpFecha.Value = Convert.ToDateTime(dgvSondaje.Rows[i].Cells[6].Value.ToString());
                es.txtProfundidad.Text = dgvSondaje.Rows[i].Cells[7].Value.ToString();
                es.txtNorte.Text = dgvSondaje.Rows[i].Cells[8].Value.ToString();
                es.txtEste.Text = dgvSondaje.Rows[i].Cells[9].Value.ToString();
                es.txtCota.Text = dgvSondaje.Rows[i].Cells[10].Value.ToString();
                es.txtMaquina.Text = dgvSondaje.Rows[i].Cells[11].Value.ToString();
                es.cboLineaCore.Text = dgvSondaje.Rows[i].Cells[12].Value.ToString();
                es.txtDip.Text = dgvSondaje.Rows[i].Cells[13].Value.ToString();
                es.txtAzimut.Text = dgvSondaje.Rows[i].Cells[14].Value.ToString();                
                int valor = int.Parse(dgvSondaje.Rows[i].Cells[16].Value.ToString());
                if (valor == 1)
                {
                    es.cbValidar.Checked = true;
                }
                else
                {
                    es.cbValidar.Checked = false;
                }
                es.UpdateEventeHandler += EditUpdateEventHandlerSondaje;
                es.Show();
            }
                
        }

        private void btnEliminarSondaje_Click(object sender, EventArgs e)
        {
            if (dgvSondaje.Rows.Count > 0)
            {
                if (MessageBox.Show("Al eliminar un sondaje se perderan las corridas y estructuras que contenga, ¿Estas seguro?", "Verificacion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    int i = dgvSondaje.CurrentRow.Index;
                    string id = dgvSondaje.Rows[i].Cells[0].Value.ToString();
                    var usuario = LogUsuario.Instancia.Usuario();
                    EntAuditoria a = new EntAuditoria();
                    a.Id_User = usuario.Id;
                    a.Nombre = usuario.Nombre;
                    a.Apellido = usuario.Apellido;
                    a.Elemento = dgvSondaje.Rows[i].Cells[5].Value.ToString();
                    a.Accion = "Eliminado";
                    a.Fecha = DateTime.Now.ToLongDateString();
                    LogAuditoria.Instancia.InsertarAuditoria(a);
                    LogSondaje.Instancia.EliminarSondaje(id);
                    ListarSondaje();
                    dgvCorrida.DataSource = null;
                    dgvEstructura.DataSource = null;
                }  
            }
        }

        private void dgvSondaje_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dgvSondaje.Rows.Count > 0)
            {
                btnEditarCorrida.Enabled = false;
                btnEliminarCorrida.Enabled = false;
                btnEditarEstructura.Enabled = false;
                btnEliminarEstructura.Enabled = false;
                btnNuevaCorrida.Enabled = true;
                btnEliminarSondaje.Enabled = true;
                btnEditarSondaje.Enabled = true;
                int i = dgvSondaje.CurrentRow.Index;
                DatosGlobales.Id_Sondaje = dgvSondaje.Rows[i].Cells[0].Value.ToString();
                DatosGlobales.Nom_sondaje = dgvSondaje.Rows[i].Cells[5].Value.ToString();
                DatosGlobales.Profundidad = double.Parse(dgvSondaje.Rows[i].Cells[7].Value.ToString());
                DatosGlobales.Dip = double.Parse(dgvSondaje.Rows[i].Cells[13].Value.ToString());
                DatosGlobales.Azimut = double.Parse(dgvSondaje.Rows[i].Cells[14].Value.ToString());
                int select = int.Parse(dgvSondaje.Rows[i].Cells[16].Value.ToString());
                if (select == 1)
                {
                    btnEliminarSondaje.Enabled = false;
                }
                else
                {
                    btnEliminarSondaje.Enabled = true;
                }
                ListarCorrida();
                dgvEstructura.DataSource = null;
            }
        }

        #endregion

        #region Corrida

        private void btnNuevaCorrida_Click(object sender, EventArgs e)
        {            
            int cont = LogCorrida.Instancia.ListarCorridas().Count;
            if (cont == 0)
            {
                DatosGlobales.ProCorrida = 0;
            }
            else
            {
                DatosGlobales.ProCorrida = double.Parse(dgvCorrida.Rows[cont - 1].Cells[4].Value.ToString());
            }
            FormularioNuevaCorrida nc = new FormularioNuevaCorrida(this);
            nc.Show();
            nc.UpdateEventeHandler += AgregUpdateEventHandlerCorrida;
        }

        private void btnEditarCorrida_Click(object sender, EventArgs e)
        {
            if (dgvCorrida.Rows.Count > 0)
            {
                int i = dgvCorrida.CurrentRow.Index;
                if (i == 0)
                {
                    DatosGlobales.ProUltCorrida = 0;
                }
                else
                {
                    DatosGlobales.ProUltCorrida = double.Parse(dgvCorrida.Rows[i - 1].Cells[4].Value.ToString());
                }
                FormularioEditarCorrida ec = new FormularioEditarCorrida(this);
                ec.txtDesde.Text = dgvCorrida.Rows[i].Cells[3].Value.ToString();
                ec.txtHasta.Text = dgvCorrida.Rows[i].Cells[4].Value.ToString();
                ec.txtPerforacion.Text = dgvCorrida.Rows[i].Cells[5].Value.ToString();
                ec.txtRecuperacion.Text = dgvCorrida.Rows[i].Cells[6].Value.ToString();
                int valor = int.Parse(dgvCorrida.Rows[i].Cells[8].Value.ToString());
                if (valor == 1)
                {
                    ec.cbValidar.Checked = true;
                }
                else
                {
                    ec.cbValidar.Checked = false;
                }
                ec.Show();
                ec.UpdateEventeHandler += EditUpdateEventHandlerCorrida;
            }
        }

        private void btnEliminarCorrida_Click(object sender, EventArgs e)
        {
            if (dgvCorrida.Rows.Count > 0)
            {
                if (MessageBox.Show("Al eliminar una corrida se perderan todas las estructuras que contenga, ¿Estas seguro?", "Verificacion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    int i = dgvCorrida.CurrentRow.Index;
                    string id = dgvCorrida.Rows[i].Cells[0].Value.ToString();
                    var usuario = LogUsuario.Instancia.Usuario();
                    EntAuditoria a = new EntAuditoria();
                    a.Id_User = usuario.Id;
                    a.Nombre = usuario.Nombre;
                    a.Apellido = usuario.Apellido;
                    a.Elemento = id;
                    a.Accion = "Eliminado";
                    a.Fecha = DateTime.Now.ToLongDateString();
                    LogAuditoria.Instancia.InsertarAuditoria(a);
                    LogCorrida.Instancia.EliminarCorrida(id);
                    ListarCorrida();
                    dgvEstructura.DataSource = null;
                }                    
            }
        }

        private void dgvCorrida_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dgvCorrida.Rows.Count > 0)
            {
                if (btnEditarEstructura.Enabled == true)
                {
                    btnEditarEstructura.Enabled = false;
                }
                if (btnEliminarEstructura.Enabled == true)
                {
                    btnEliminarEstructura.Enabled = false;
                }
                btnEditarEstructura.Enabled = false;
                btnEliminarEstructura.Enabled = false;
                btnNuevaCorrida.Enabled = false;
                btnEliminarSondaje.Enabled = false;
                btnEditarSondaje.Enabled = false;
                btnNuevaEstructura.Enabled = true;
                btnEditarCorrida.Enabled = true;
                btnEliminarCorrida.Enabled = true;
                int i = dgvCorrida.CurrentRow.Index;
                DatosGlobales.Id_Corrida = dgvCorrida.Rows[i].Cells[0].Value.ToString();
                DatosGlobales.Min = double.Parse(dgvCorrida.Rows[i].Cells[3].Value.ToString());
                DatosGlobales.Max = double.Parse(dgvCorrida.Rows[i].Cells[4].Value.ToString());
                int select = int.Parse(dgvCorrida.Rows[i].Cells[8].Value.ToString());
                if (select == 1)
                {
                    btnEliminarCorrida.Enabled = false;
                }
                else
                {
                    btnEliminarCorrida.Enabled = true;
                }
                ListarEstructura();
            }    
        }

        #endregion

        #region Estructura

        private void btnNuevaEstructura_Click(object sender, EventArgs e)
        {

            DatosGlobales.num_Estructura = LogEstructura.Instancia.ListarEstructuras().Count;
            FormularioNuevaEstructura ne = new FormularioNuevaEstructura(this);
            ne.Show();
            ne.UpdateEventeHandler += AgregUpdateEventHandlerEstructura;
        }

        private void btnEditarEstructura_Click(object sender, EventArgs e)
        {
            if (dgvEstructura.Rows.Count > 0)
            {
                int i = dgvEstructura.CurrentRow.Index;
                FormularioEditarEstructura ee = new FormularioEditarEstructura(this);
                ee.txtNumeroEstructura.Text = dgvEstructura.Rows[i].Cells[4].Value.ToString();
                ee.txtProfundidadEstructura.Text = dgvEstructura.Rows[i].Cells[5].Value.ToString();
                ee.cboTipoEstructura.Text = dgvEstructura.Rows[i].Cells[6].Value.ToString();
                ee.txtTB.Text = dgvEstructura.Rows[i].Cells[7].Value.ToString();
                ee.txtAlpha.Text = dgvEstructura.Rows[i].Cells[8].Value.ToString();
                ee.txtBeta.Text = dgvEstructura.Rows[i].Cells[9].Value.ToString();
                ee.txtRefToTop.Text = dgvEstructura.Rows[i].Cells[10].Value.ToString();
                ee.cboCondicionFractura.Text = dgvEstructura.Rows[i].Cells[11].Value.ToString();
                ee.cboJrc.Text = dgvEstructura.Rows[i].Cells[12].Value.ToString();
                ee.cboJr.Text = dgvEstructura.Rows[i].Cells[13].Value.ToString();
                ee.cboJa.Text = dgvEstructura.Rows[i].Cells[14].Value.ToString();
                ee.txtEspesor.Text = dgvEstructura.Rows[i].Cells[15].Value.ToString();
                ee.cboRelleno1.Text = dgvEstructura.Rows[i].Cells[16].Value.ToString();
                ee.cboRelleno2.Text = dgvEstructura.Rows[i].Cells[17].Value.ToString();
                ee.txtComentario.Text = dgvEstructura.Rows[i].Cells[18].Value.ToString();
                ee.txtFecha.Text = dgvEstructura.Rows[i].Cells[20].Value.ToString();
                int valor = int.Parse(dgvEstructura.Rows[i].Cells[26].Value.ToString());
                if (valor == 1)
                {
                    ee.cbValidar.Checked = true;
                }
                else
                {
                    ee.cbValidar.Checked = false;
                }
                ee.Show();
                ee.UpdateEventeHandler += EditUpdateEventHandlerEstructura;
            }
        }

        private void btnEliminarEstructura_Click(object sender, EventArgs e)
        {
            if (dgvEstructura.Rows.Count > 0)
            {
                if (MessageBox.Show("¿Estas seguro?", "Verificacion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {

                    int i = dgvEstructura.CurrentRow.Index;
                    string id = dgvEstructura.Rows[i].Cells[0].Value.ToString();
                    var usuario = LogUsuario.Instancia.Usuario();
                    EntAuditoria a = new EntAuditoria();
                    a.Id_User = usuario.Id;
                    a.Nombre = usuario.Nombre;
                    a.Apellido = usuario.Apellido;
                    a.Elemento = id;
                    a.Accion = "Eliminado";
                    a.Fecha = DateTime.Now.ToLongDateString();
                    LogAuditoria.Instancia.InsertarAuditoria(a);
                    LogEstructura.Instancia.EliminarEstructura(id);
                    ListarEstructura();
                }                    
            }
        }

        private void dgvEstructura_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dgvEstructura.Rows.Count > 0)
            {
                int i = dgvEstructura.CurrentRow.Index;
                id_Usuario = dgvEstructura.Rows[i].Cells[3].Value.ToString();
                DatosGlobales.Id_Estructura = dgvEstructura.Rows[i].Cells[0].Value.ToString();
                if (id_Usuario == DatosGlobales.Id_Usuario)
                {
                    btnEditarEstructura.Enabled = true;
                    btnEliminarEstructura.Enabled = true;
                    btnNuevaEstructura.Enabled = false;
                    btnEditarCorrida.Enabled = false;
                    btnEliminarCorrida.Enabled = false;
                    int select = int.Parse(dgvEstructura.Rows[i].Cells[26].Value.ToString());
                    if (select == 1)
                    {
                        btnEliminarEstructura.Enabled = false;
                    }
                    else
                    {
                        btnEliminarEstructura.Enabled = true;
                    }
                }
                else
                {
                    MessageBox.Show("No tienes acceso a estos datos");
                }
            }
        }

        #endregion

    }
}
