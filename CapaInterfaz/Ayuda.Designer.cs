﻿
namespace CapaInterfaz
{
    partial class Ayuda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbAlpha = new System.Windows.Forms.PictureBox();
            this.pbBeta = new System.Windows.Forms.PictureBox();
            this.pbCondicionFractura = new System.Windows.Forms.PictureBox();
            this.pbJrc = new System.Windows.Forms.PictureBox();
            this.pbJr = new System.Windows.Forms.PictureBox();
            this.pbJa = new System.Windows.Forms.PictureBox();
            this.pbRelleno1 = new System.Windows.Forms.PictureBox();
            this.pbRelleno2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbAlpha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBeta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCondicionFractura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJrc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRelleno1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRelleno2)).BeginInit();
            this.SuspendLayout();
            // 
            // pbAlpha
            // 
            this.pbAlpha.Image = global::CapaInterfaz.Properties.Resources.Angulo_alfa;
            this.pbAlpha.Location = new System.Drawing.Point(65, 121);
            this.pbAlpha.Name = "pbAlpha";
            this.pbAlpha.Size = new System.Drawing.Size(100, 50);
            this.pbAlpha.TabIndex = 0;
            this.pbAlpha.TabStop = false;
            // 
            // pbBeta
            // 
            this.pbBeta.Image = global::CapaInterfaz.Properties.Resources.Angulo_Beta;
            this.pbBeta.Location = new System.Drawing.Point(265, 121);
            this.pbBeta.Name = "pbBeta";
            this.pbBeta.Size = new System.Drawing.Size(100, 50);
            this.pbBeta.TabIndex = 1;
            this.pbBeta.TabStop = false;
            // 
            // pbCondicionFractura
            // 
            this.pbCondicionFractura.Image = global::CapaInterfaz.Properties.Resources.Condicion_de_Fractura;
            this.pbCondicionFractura.Location = new System.Drawing.Point(465, 121);
            this.pbCondicionFractura.Name = "pbCondicionFractura";
            this.pbCondicionFractura.Size = new System.Drawing.Size(100, 50);
            this.pbCondicionFractura.TabIndex = 2;
            this.pbCondicionFractura.TabStop = false;
            // 
            // pbJrc
            // 
            this.pbJrc.Image = global::CapaInterfaz.Properties.Resources.Rugosidad;
            this.pbJrc.Location = new System.Drawing.Point(665, 121);
            this.pbJrc.Name = "pbJrc";
            this.pbJrc.Size = new System.Drawing.Size(100, 50);
            this.pbJrc.TabIndex = 3;
            this.pbJrc.TabStop = false;
            // 
            // pbJr
            // 
            this.pbJr.Image = global::CapaInterfaz.Properties.Resources.Jr_Q;
            this.pbJr.Location = new System.Drawing.Point(65, 241);
            this.pbJr.Name = "pbJr";
            this.pbJr.Size = new System.Drawing.Size(100, 50);
            this.pbJr.TabIndex = 4;
            this.pbJr.TabStop = false;
            // 
            // pbJa
            // 
            this.pbJa.Image = global::CapaInterfaz.Properties.Resources.Ja_Q;
            this.pbJa.Location = new System.Drawing.Point(265, 241);
            this.pbJa.Name = "pbJa";
            this.pbJa.Size = new System.Drawing.Size(100, 50);
            this.pbJa.TabIndex = 5;
            this.pbJa.TabStop = false;
            // 
            // pbRelleno1
            // 
            this.pbRelleno1.Image = global::CapaInterfaz.Properties.Resources.Registro_de_Relleno;
            this.pbRelleno1.Location = new System.Drawing.Point(465, 241);
            this.pbRelleno1.Name = "pbRelleno1";
            this.pbRelleno1.Size = new System.Drawing.Size(100, 50);
            this.pbRelleno1.TabIndex = 6;
            this.pbRelleno1.TabStop = false;
            // 
            // pbRelleno2
            // 
            this.pbRelleno2.Image = global::CapaInterfaz.Properties.Resources.Registro_de_Relleno;
            this.pbRelleno2.Location = new System.Drawing.Point(665, 241);
            this.pbRelleno2.Name = "pbRelleno2";
            this.pbRelleno2.Size = new System.Drawing.Size(100, 50);
            this.pbRelleno2.TabIndex = 7;
            this.pbRelleno2.TabStop = false;
            // 
            // Ayuda
            // 
            this.ClientSize = new System.Drawing.Size(831, 413);
            this.Controls.Add(this.pbRelleno2);
            this.Controls.Add(this.pbRelleno1);
            this.Controls.Add(this.pbJa);
            this.Controls.Add(this.pbJr);
            this.Controls.Add(this.pbJrc);
            this.Controls.Add(this.pbCondicionFractura);
            this.Controls.Add(this.pbBeta);
            this.Controls.Add(this.pbAlpha);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Ayuda";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Ayuda_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.pbAlpha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBeta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCondicionFractura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJrc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRelleno1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRelleno2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbAlpha;
        private System.Windows.Forms.PictureBox pbBeta;
        private System.Windows.Forms.PictureBox pbCondicionFractura;
        private System.Windows.Forms.PictureBox pbJrc;
        private System.Windows.Forms.PictureBox pbJr;
        private System.Windows.Forms.PictureBox pbJa;
        private System.Windows.Forms.PictureBox pbRelleno1;
        private System.Windows.Forms.PictureBox pbRelleno2;
    }
}