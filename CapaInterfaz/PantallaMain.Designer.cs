﻿
namespace CapaInterfaz
{
    partial class PantallaMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelFormularios = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnLogueo = new System.Windows.Forms.Button();
            this.btnReportes = new System.Windows.Forms.Button();
            this.btnEstereografia = new System.Windows.Forms.Button();
            this.btnEstadisticas = new System.Windows.Forms.Button();
            this.btnNosotros = new System.Windows.Forms.Button();
            this.btnSobreAplicacion = new System.Windows.Forms.Button();
            this.btnIniciarSesion = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtContrasenia = new System.Windows.Forms.TextBox();
            this.labelPerfil = new System.Windows.Forms.Label();
            this.labelSalir = new System.Windows.Forms.Label();
            this.panelLogin = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnSincLocal = new System.Windows.Forms.Button();
            this.btnSincRemota = new System.Windows.Forms.Button();
            this.panelFormularios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panelLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelFormularios
            // 
            this.panelFormularios.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelFormularios.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panelFormularios.Controls.Add(this.pictureBox3);
            this.panelFormularios.Controls.Add(this.pictureBox2);
            this.panelFormularios.Location = new System.Drawing.Point(199, 0);
            this.panelFormularios.Name = "panelFormularios";
            this.panelFormularios.Size = new System.Drawing.Size(466, 569);
            this.panelFormularios.TabIndex = 1;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(466, 569);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 1;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(466, 569);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // btnLogueo
            // 
            this.btnLogueo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(64)))), ((int)(((byte)(93)))));
            this.btnLogueo.FlatAppearance.BorderSize = 0;
            this.btnLogueo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogueo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogueo.ForeColor = System.Drawing.Color.White;
            this.btnLogueo.Image = global::CapaInterfaz.Properties.Resources.logeo;
            this.btnLogueo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogueo.Location = new System.Drawing.Point(0, 150);
            this.btnLogueo.Name = "btnLogueo";
            this.btnLogueo.Size = new System.Drawing.Size(200, 42);
            this.btnLogueo.TabIndex = 6;
            this.btnLogueo.Text = "Logueo                         ";
            this.btnLogueo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogueo.UseVisualStyleBackColor = false;
            this.btnLogueo.Click += new System.EventHandler(this.btnLogueo_Click);
            // 
            // btnReportes
            // 
            this.btnReportes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(64)))), ((int)(((byte)(93)))));
            this.btnReportes.FlatAppearance.BorderSize = 0;
            this.btnReportes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReportes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReportes.ForeColor = System.Drawing.Color.White;
            this.btnReportes.Image = global::CapaInterfaz.Properties.Resources.reportes;
            this.btnReportes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReportes.Location = new System.Drawing.Point(0, 192);
            this.btnReportes.Name = "btnReportes";
            this.btnReportes.Size = new System.Drawing.Size(200, 42);
            this.btnReportes.TabIndex = 7;
            this.btnReportes.Text = "Reportes                      ";
            this.btnReportes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReportes.UseVisualStyleBackColor = false;
            this.btnReportes.Click += new System.EventHandler(this.btnReportes_Click);
            // 
            // btnEstereografia
            // 
            this.btnEstereografia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(64)))), ((int)(((byte)(93)))));
            this.btnEstereografia.FlatAppearance.BorderSize = 0;
            this.btnEstereografia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstereografia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEstereografia.ForeColor = System.Drawing.Color.White;
            this.btnEstereografia.Image = global::CapaInterfaz.Properties.Resources.estereografia_opt;
            this.btnEstereografia.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEstereografia.Location = new System.Drawing.Point(0, 234);
            this.btnEstereografia.Name = "btnEstereografia";
            this.btnEstereografia.Size = new System.Drawing.Size(200, 42);
            this.btnEstereografia.TabIndex = 8;
            this.btnEstereografia.Text = "Estereografía              ";
            this.btnEstereografia.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEstereografia.UseVisualStyleBackColor = false;
            this.btnEstereografia.Click += new System.EventHandler(this.btnEstereografia_Click);
            // 
            // btnEstadisticas
            // 
            this.btnEstadisticas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(64)))), ((int)(((byte)(93)))));
            this.btnEstadisticas.FlatAppearance.BorderSize = 0;
            this.btnEstadisticas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstadisticas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEstadisticas.ForeColor = System.Drawing.Color.White;
            this.btnEstadisticas.Image = global::CapaInterfaz.Properties.Resources.estadisticas;
            this.btnEstadisticas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEstadisticas.Location = new System.Drawing.Point(0, 276);
            this.btnEstadisticas.Name = "btnEstadisticas";
            this.btnEstadisticas.Size = new System.Drawing.Size(200, 32);
            this.btnEstadisticas.TabIndex = 9;
            this.btnEstadisticas.Text = "Estadísticas                ";
            this.btnEstadisticas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEstadisticas.UseVisualStyleBackColor = false;
            this.btnEstadisticas.Click += new System.EventHandler(this.btnEstadisticas_Click);
            // 
            // btnNosotros
            // 
            this.btnNosotros.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(64)))), ((int)(((byte)(93)))));
            this.btnNosotros.FlatAppearance.BorderSize = 0;
            this.btnNosotros.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNosotros.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNosotros.ForeColor = System.Drawing.Color.White;
            this.btnNosotros.Image = global::CapaInterfaz.Properties.Resources.nosotros;
            this.btnNosotros.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNosotros.Location = new System.Drawing.Point(0, 308);
            this.btnNosotros.Name = "btnNosotros";
            this.btnNosotros.Size = new System.Drawing.Size(200, 42);
            this.btnNosotros.TabIndex = 10;
            this.btnNosotros.Text = "Nosotros                     ";
            this.btnNosotros.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNosotros.UseVisualStyleBackColor = false;
            this.btnNosotros.Click += new System.EventHandler(this.btnNosotros_Click);
            // 
            // btnSobreAplicacion
            // 
            this.btnSobreAplicacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(64)))), ((int)(((byte)(93)))));
            this.btnSobreAplicacion.FlatAppearance.BorderSize = 0;
            this.btnSobreAplicacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSobreAplicacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSobreAplicacion.ForeColor = System.Drawing.Color.White;
            this.btnSobreAplicacion.Image = global::CapaInterfaz.Properties.Resources.sobre_la_aplicacion;
            this.btnSobreAplicacion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSobreAplicacion.Location = new System.Drawing.Point(0, 350);
            this.btnSobreAplicacion.Name = "btnSobreAplicacion";
            this.btnSobreAplicacion.Size = new System.Drawing.Size(200, 42);
            this.btnSobreAplicacion.TabIndex = 11;
            this.btnSobreAplicacion.Text = "Sobre la aplicación    ";
            this.btnSobreAplicacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSobreAplicacion.UseVisualStyleBackColor = false;
            this.btnSobreAplicacion.Click += new System.EventHandler(this.btnSobreAplicacion_Click);
            // 
            // btnIniciarSesion
            // 
            this.btnIniciarSesion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnIniciarSesion.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(31)))), ((int)(((byte)(67)))));
            this.btnIniciarSesion.Location = new System.Drawing.Point(60, 219);
            this.btnIniciarSesion.Name = "btnIniciarSesion";
            this.btnIniciarSesion.Size = new System.Drawing.Size(91, 23);
            this.btnIniciarSesion.TabIndex = 1;
            this.btnIniciarSesion.Text = "Iniciar Sesión";
            this.btnIniciarSesion.UseVisualStyleBackColor = false;
            this.btnIniciarSesion.Click += new System.EventHandler(this.btnIniciarSesion_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(31)))), ((int)(((byte)(67)))));
            this.btnCancelar.Location = new System.Drawing.Point(68, 260);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 2;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(39, 141);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(133, 20);
            this.txtUsuario.TabIndex = 3;
            this.txtUsuario.Text = "Usuario";
            this.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtUsuario.Enter += new System.EventHandler(this.txtUsuario_Enter);
            this.txtUsuario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUsuario_KeyPress);
            this.txtUsuario.Leave += new System.EventHandler(this.txtUsuario_Leave);
            // 
            // txtContrasenia
            // 
            this.txtContrasenia.Location = new System.Drawing.Point(39, 180);
            this.txtContrasenia.Name = "txtContrasenia";
            this.txtContrasenia.Size = new System.Drawing.Size(133, 20);
            this.txtContrasenia.TabIndex = 4;
            this.txtContrasenia.Text = "Contraseña";
            this.txtContrasenia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtContrasenia.Enter += new System.EventHandler(this.txtContrasenia_Enter);
            this.txtContrasenia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContrasenia_KeyPress);
            this.txtContrasenia.Leave += new System.EventHandler(this.txtContrasenia_Leave);
            // 
            // labelPerfil
            // 
            this.labelPerfil.AutoSize = true;
            this.labelPerfil.ForeColor = System.Drawing.Color.White;
            this.labelPerfil.Location = new System.Drawing.Point(57, 124);
            this.labelPerfil.Name = "labelPerfil";
            this.labelPerfil.Size = new System.Drawing.Size(30, 13);
            this.labelPerfil.TabIndex = 12;
            this.labelPerfil.Text = "Perfil";
            this.labelPerfil.Click += new System.EventHandler(this.labelPerfil_Click);
            // 
            // labelSalir
            // 
            this.labelSalir.AutoSize = true;
            this.labelSalir.ForeColor = System.Drawing.Color.White;
            this.labelSalir.Location = new System.Drawing.Point(124, 124);
            this.labelSalir.Name = "labelSalir";
            this.labelSalir.Size = new System.Drawing.Size(27, 13);
            this.labelSalir.TabIndex = 13;
            this.labelSalir.Text = "Salir";
            this.labelSalir.Click += new System.EventHandler(this.labelSalir_Click);
            // 
            // panelLogin
            // 
            this.panelLogin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panelLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(31)))), ((int)(((byte)(67)))));
            this.panelLogin.Controls.Add(this.btnSincRemota);
            this.panelLogin.Controls.Add(this.btnSincLocal);
            this.panelLogin.Controls.Add(this.pictureBox1);
            this.panelLogin.Controls.Add(this.labelSalir);
            this.panelLogin.Controls.Add(this.labelPerfil);
            this.panelLogin.Controls.Add(this.txtContrasenia);
            this.panelLogin.Controls.Add(this.txtUsuario);
            this.panelLogin.Controls.Add(this.btnCancelar);
            this.panelLogin.Controls.Add(this.btnIniciarSesion);
            this.panelLogin.Controls.Add(this.btnSobreAplicacion);
            this.panelLogin.Controls.Add(this.btnNosotros);
            this.panelLogin.Controls.Add(this.btnEstadisticas);
            this.panelLogin.Controls.Add(this.btnEstereografia);
            this.panelLogin.Controls.Add(this.btnReportes);
            this.panelLogin.Controls.Add(this.btnLogueo);
            this.panelLogin.Location = new System.Drawing.Point(0, 0);
            this.panelLogin.Name = "panelLogin";
            this.panelLogin.Size = new System.Drawing.Size(200, 569);
            this.panelLogin.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CapaInterfaz.Properties.Resources.perfil;
            this.pictureBox1.Location = new System.Drawing.Point(80, 67);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // btnSincLocal
            // 
            this.btnSincLocal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(64)))), ((int)(((byte)(93)))));
            this.btnSincLocal.Enabled = false;
            this.btnSincLocal.FlatAppearance.BorderSize = 0;
            this.btnSincLocal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSincLocal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSincLocal.ForeColor = System.Drawing.Color.White;
            this.btnSincLocal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSincLocal.Location = new System.Drawing.Point(0, 477);
            this.btnSincLocal.Name = "btnSincLocal";
            this.btnSincLocal.Size = new System.Drawing.Size(200, 42);
            this.btnSincLocal.TabIndex = 15;
            this.btnSincLocal.Text = "Actualizar Datos";
            this.btnSincLocal.UseVisualStyleBackColor = false;
            this.btnSincLocal.Click += new System.EventHandler(this.btnSincLocal_Click);
            // 
            // btnSincRemota
            // 
            this.btnSincRemota.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(64)))), ((int)(((byte)(93)))));
            this.btnSincRemota.Enabled = false;
            this.btnSincRemota.FlatAppearance.BorderSize = 0;
            this.btnSincRemota.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSincRemota.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSincRemota.ForeColor = System.Drawing.Color.White;
            this.btnSincRemota.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSincRemota.Location = new System.Drawing.Point(0, 519);
            this.btnSincRemota.Name = "btnSincRemota";
            this.btnSincRemota.Size = new System.Drawing.Size(200, 42);
            this.btnSincRemota.TabIndex = 16;
            this.btnSincRemota.Text = "Subir Datos";
            this.btnSincRemota.UseVisualStyleBackColor = false;
            this.btnSincRemota.Click += new System.EventHandler(this.btnSincRemota_Click);
            // 
            // PantallaMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 569);
            this.Controls.Add(this.panelLogin);
            this.Controls.Add(this.panelFormularios);
            this.MaximizeBox = false;
            this.Name = "PantallaMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "LOGUEO GEOTÉCNICO";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panelFormularios.ResumeLayout(false);
            this.panelFormularios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panelLogin.ResumeLayout(false);
            this.panelLogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelFormularios;
        private System.Windows.Forms.Button btnLogueo;
        private System.Windows.Forms.Button btnReportes;
        private System.Windows.Forms.Button btnEstereografia;
        private System.Windows.Forms.Button btnEstadisticas;
        private System.Windows.Forms.Button btnNosotros;
        private System.Windows.Forms.Button btnSobreAplicacion;
        private System.Windows.Forms.Button btnIniciarSesion;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtContrasenia;
        private System.Windows.Forms.Label labelPerfil;
        private System.Windows.Forms.Label labelSalir;
        private System.Windows.Forms.Panel panelLogin;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnSincRemota;
        private System.Windows.Forms.Button btnSincLocal;
    }
}