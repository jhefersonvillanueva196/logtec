﻿using CapaEntidad;
using CapaLogica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace CapaInterfaz
{
    public partial class VistaReportes : Form
    {
        public VistaReportes()
        {
            InitializeComponent();
            ListarSondaje();
            ListarCabeceras();
            btnBuscar.Location = new Point(536, 160);
            if (DatosGlobales.Rol == "Supervisor")
            {
                btnAuditoria.Visible = true;
                btnAuditoria.Location = new Point(652, 160);
                btnBuscar.Location = new Point(420, 160);
            }
            else
            {
                btnBuscar.Location = new Point(546, 160);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ListarCabeceras()
        {
            foreach (DataGridViewColumn column in dgvReporte.Columns)
            {
                clbFiltros.Items.Add(column.HeaderText,true);
            }
        }

        public void ListarSondaje()
        {
            dgvSondaje.DataSource = LogSondaje.Instancia.ListarSondajes();
        }

        public void ListarCorrida()
        {
            dgvDesde.DataSource = LogCorrida.Instancia.ListarCorridas();
            dgvHasta.DataSource = LogCorrida.Instancia.ListarCorridas();
        }
        public void ListarReporte()
        {
            dgvReporte.DataSource = LogConsulta.Instancia.ListarConsultas();
        }
        public void ListarAuditoria()
        {

            dgvAuditoria.DataSource = LogAuditoria.Instancia.ListarAuditorias();
        }

        private void dgvSondaje_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dgvSondaje.Rows.Count > 0)
            {
                int i = dgvSondaje.CurrentRow.Index;
                DatosGlobales.Id_Sondaje = dgvSondaje.Rows[i].Cells[0].Value.ToString();
                DatosGlobales.Nom_sondaje = dgvSondaje.Rows[i].Cells[5].Value.ToString();
                DatosGlobales.Profundidad = double.Parse(dgvSondaje.Rows[i].Cells[7].Value.ToString());
                DatosGlobales.Dip = double.Parse(dgvSondaje.Rows[i].Cells[13].Value.ToString());
                DatosGlobales.Azimut = double.Parse(dgvSondaje.Rows[i].Cells[14].Value.ToString());
                txtCliente.Text = dgvSondaje.Rows[i].Cells[2].Value.ToString();
                txtProyecto.Text = dgvSondaje.Rows[i].Cells[3].Value.ToString();
                txtGeotecnico.Text = dgvSondaje.Rows[i].Cells[4].Value.ToString();
                txtSondaje.Text = dgvSondaje.Rows[i].Cells[5].Value.ToString();
                txtFecha.Text = dgvSondaje.Rows[i].Cells[6].Value.ToString();
                txtProfundidad.Text = dgvSondaje.Rows[i].Cells[7].Value.ToString();
                txtNorte.Text = dgvSondaje.Rows[i].Cells[8].Value.ToString();
                txtEste.Text = dgvSondaje.Rows[i].Cells[9].Value.ToString();
                txtCota.Text = dgvSondaje.Rows[i].Cells[10].Value.ToString();
                txtMaquina.Text = dgvSondaje.Rows[i].Cells[11].Value.ToString();
                ListarCorrida();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            int i, f;
            DateTime fechaI, fechaF;
            double tramoI, tramoF;            
            if (cbFecha.Checked == true && cbTramo.Checked == false)
            {
                fechaI = DateTime.Parse(dtpDesde.Text.ToString());
                fechaF = DateTime.Parse(dtpHasta.Text.ToString());
                if (fechaI > fechaF)
                {
                    MessageBox.Show("Las fechas no han sido ingresadas correctamente");
                }
                else
                {
                    dgvReporte.DataSource = null;
                    dgvReporte.DataSource = LogConsulta.Instancia.ConsultarFecha(fechaI, fechaF);

                    if (panelAuditoria.Visible == true)
                    {
                        panelAuditoria.Visible = false;
                    }
                    panelReporte.Visible = true;
                    panelReporte.Size = new Size(1150, 450);
                    panelReporte.Location = new Point(9, 253);
                }
            }
            else
            {
                if (cbTramo.Checked == true && cbFecha.Checked == false)
                {
                    i = dgvDesde.CurrentRow.Index;
                    f = dgvHasta.CurrentRow.Index;
                    tramoI = double.Parse(dgvDesde.Rows[i].Cells[3].Value.ToString());
                    tramoF = double.Parse(dgvHasta.Rows[f].Cells[4].Value.ToString());
                    if (tramoI > tramoF)
                    {
                        MessageBox.Show("El limite inferior no puede ser mayor que el limite superior verifica tu consulta");
                    }
                    else
                    {
                        if(tramoI == tramoF)
                        {
                            MessageBox.Show("No puedes escoger un solo punto");
                        }
                        else
                        {
                            dgvReporte.DataSource = null;
                            dgvReporte.DataSource = LogConsulta.Instancia.ConsultarTramo(tramoI, tramoF);

                            if (panelAuditoria.Visible == true)
                            {
                                panelAuditoria.Visible = false;
                            }
                            panelReporte.Visible = true;
                            panelReporte.Size = new Size(1150, 450);
                            panelReporte.Location = new Point(9, 253);
                        }
                    }
                }
                else
                {
                    if (cbTramo.Checked == true && cbFecha.Checked == true)
                    {
                        i = dgvDesde.CurrentRow.Index;
                        f = dgvHasta.CurrentRow.Index;
                        fechaI = DateTime.Parse(dtpDesde.Text.ToString());
                        fechaF = DateTime.Parse(dtpHasta.Text.ToString());
                        tramoI = double.Parse(dgvDesde.Rows[i].Cells[3].Value.ToString());
                        tramoF = double.Parse(dgvDesde.Rows[f].Cells[4].Value.ToString());
                        if (fechaI > fechaF)
                        {
                            MessageBox.Show("Las fechas no han sido ingresadas correctamente");
                        }
                        else
                        {
                            if (tramoI > tramoF)
                            {
                                MessageBox.Show("Los tramos no coinciden, verifica tus limites");
                            }
                            else
                            {
                                if (tramoI == tramoF)
                                {
                                    MessageBox.Show("No puedes escoger un solo punto");
                                }
                                else
                                {
                                    dgvReporte.DataSource = null;
                                    dgvReporte.DataSource = LogConsulta.Instancia.ConsultarFechaTramo(fechaI, fechaF, tramoI, tramoF);

                                    if (panelAuditoria.Visible == true)
                                    {
                                        panelAuditoria.Visible = false;
                                    }
                                    panelReporte.Visible = true;
                                    panelReporte.Size = new Size(1150, 450);
                                    panelReporte.Location = new Point(9, 253);
                                }
                            }
                        }                        
                    }
                    else
                    {
                        if (cbTramo.Checked == false && cbFecha.Checked == false)
                        {
                            ListarReporte();
                            if (panelAuditoria.Visible == true)
                            {
                                panelAuditoria.Visible = false;
                            }
                            panelReporte.Visible = true;
                            panelReporte.Size = new Size(1150, 450);
                            panelReporte.Location = new Point(9, 253);
                        }
                    }
                }                
            }            
        }

        private void btnAuditoria_Click(object sender, EventArgs e)
        {
            if (panelReporte.Visible == true)
            {
                panelReporte.Visible = false;
            }
            panelAuditoria.Visible = true;
            panelAuditoria.Size= new Size(1150, 450);
            panelAuditoria.Location = new Point(9, 253);
            ListarAuditoria();
        }

        private void cbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFecha.Checked == true)
            {
                panelFechas.Enabled = true;
            }
            else
            {
                panelFechas.Enabled = false;
            }
        }

        private void cbTramo_CheckedChanged(object sender, EventArgs e)
        {
            if (cbTramo.Checked == true)
            {
                panelLimites.Enabled = true;
            }
            else
            {
                panelLimites.Enabled = false;
            }
        }

        public void ExportData(DataGridView data)
        {
            Excel.Application oXL = new Excel.Application();
            Excel._Workbook oWB;
            Excel._Worksheet oSheet;
            Excel.Range oRng;

            try
            {
                oWB = oXL.Workbooks.Add(Missing.Value);
                oSheet = oWB.ActiveSheet;

                oSheet.Name = txtSondaje.Text.Trim();

                oRng = oSheet.get_Range("C1", "L1");
                oRng.Merge(true);
                oRng.Value2 = "FORMATO DE REGISTRO DE TALADROS ORIENTADOS";
                oRng.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                oSheet.Cells[2, 3] = "Cliente:";
                oSheet.Cells[2, 4] = txtCliente.Text.Trim();

                oSheet.Cells[2, 6] = "Proyecto:";
                oSheet.Cells[2, 7] = txtProyecto.Text.Trim();

                oSheet.Cells[2, 9] = "Geotécnico:";
                oSheet.Cells[2, 10] = txtGeotecnico.Text.Trim();

                oSheet.Cells[3, 3] = "Sondaje:";
                oSheet.Cells[3, 4] = txtSondaje.Text.Trim();

                oSheet.Cells[3, 6] = "Fecha:";
                oSheet.Cells[3, 7] = txtFecha.Text.Trim();


                oSheet.Cells[3, 9] = "Profundidad:";
                oSheet.Cells[3, 10] = txtProfundidad.Text.Trim();

                oSheet.Cells[4, 3] = "Norte:";
                oSheet.Cells[4, 4] = txtNorte.Text.Trim();

                oSheet.Cells[4, 6] = "Este:";
                oSheet.Cells[4, 7] = txtEste.Text.Trim();

                oSheet.Cells[4, 9] = "Cota:";
                oSheet.Cells[4, 10] = txtCota.Text.Trim();

                oSheet.Cells[4, 12] = "Maquina:";
                oSheet.Cells[4, 13] = txtMaquina.Text.Trim();

                int indicecolumna = 0;
                int indicefila = 6;

                foreach (DataGridViewColumn columna in data.Columns)
                {
                    indicecolumna++;
                    if (columna.Visible == true)
                    {
                        oSheet.Cells[7, indicecolumna] = columna.HeaderText;
                        oRng = oSheet.Cells[7, indicecolumna];
                        oRng.Interior.ColorIndex = 23;
                        oRng.EntireColumn.AutoFit();
                        oRng.Borders.Weight = Excel.XlBorderWeight.xlThin;
                        oRng.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    }
                    else
                    {
                        indicecolumna--;
                    }
                }

                foreach (DataGridViewRow fila in data.Rows)
                {
                    indicefila++;
                    indicecolumna = 0;
                    foreach (DataGridViewColumn columna in data.Columns)
                    {
                        indicecolumna++;
                        if (columna.Visible == true)
                        {
                            oSheet.Cells[indicefila + 1, indicecolumna] = fila.Cells[columna.Name].Value;

                            oRng = oSheet.Cells[indicefila + 1, indicecolumna];
                            oRng.EntireColumn.AutoFit();
                            oRng.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                        }
                        else
                        {
                            indicecolumna--;
                        }
                    }
                }


                oXL.Visible = true;
                oXL.UserControl = true;
            
            }
            catch
            {

            }

            oXL = null;
            oWB = null;
            oSheet = null;
            oRng = null;
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            ExportData(dgvReporte);
        }

        private void cbFiltros_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFiltros.Checked == true)
            {
                clbFiltros.Visible = true;
                dgvReporte.Size=new Size(962, 244);
            }
            else
            {
                clbFiltros.Visible = false;
                dgvReporte.Size = new Size(1122, 244);
            }
        }

        private void clbFiltros_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (!clbFiltros.GetItemChecked(e.Index))
            {
                dgvReporte.Columns[e.Index].Visible = true;
            }
            else
            {
                dgvReporte.Columns[e.Index].Visible = false;
            }
        }
    }
}
