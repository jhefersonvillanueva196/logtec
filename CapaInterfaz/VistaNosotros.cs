﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaInterfaz
{
    public partial class VistaNosotros : Form
    {
        public VistaNosotros()
        {
            InitializeComponent();
        }

        private void btnGeotekh_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://geotekhperu.com/");
        }

        private void btnYoutube_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.youtube.com/channel/UCxSWjcKm2fL7VSB6MJbihlA");
        }

        private void btnFacebook_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://geotekhperu.com/");
        }
    }
}
