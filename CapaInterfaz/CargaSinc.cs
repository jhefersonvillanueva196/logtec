﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaInterfaz
{
    public partial class CargaSinc : Form
    {
        public CargaSinc()
        {
            InitializeComponent();
        }

        private void Carga_Load(object sender, EventArgs e)
        {
            pbCarga.Location = new Point(this.Width / 2 - pbCarga.Width / 2, this.Height / 2 - pbCarga.Height / 2);
        }
    }
}
