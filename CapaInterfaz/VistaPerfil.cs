﻿using CapaEntidad;
using CapaLogica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaInterfaz
{
    public partial class VistaPerfil : Form
    {
        public VistaPerfil()
        {
            InitializeComponent();
            CargarTextos();
        }

        public void CargarTextos()
        {
            EntUsuario user =LogUsuario.Instancia.Usuario();
            txtDni.Text = user.Dni;
            txtNombre.Text = user.Nombre;
            txtApellido.Text = user.Apellido;
            txtIniciales.Text = user.Iniciales;
            txtContrasenia.Text = user.Contrasenia;
            txtRol.Text = user.Rol;
        }

        private void pbCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            EntUsuario u = new EntUsuario();
            u.Dni = txtDni.Text;
            u.Nombre = txtNombre.Text;
            u.Apellido = txtApellido.Text;
            u.Iniciales = txtIniciales.Text;
            u.Contrasenia = txtIniciales.Text;
            u.Rol = txtIniciales.Text;
            LogUsuario.Instancia.EditarUsuario(u);
            this.Close();
        }
    }
}
