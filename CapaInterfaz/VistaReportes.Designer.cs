﻿
namespace CapaInterfaz
{
    partial class VistaReportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbTramo = new System.Windows.Forms.CheckBox();
            this.cbFecha = new System.Windows.Forms.CheckBox();
            this.panelLimites = new System.Windows.Forms.Panel();
            this.dgvHasta = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idUserDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idSondajeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.desdeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hastaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perforacionDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recuperacionDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.entCorridaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dgvDesde = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idUserDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idSondajeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.desdeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hastaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perforacionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recuperacionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelFechas = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpHasta = new System.Windows.Forms.DateTimePicker();
            this.dtpDesde = new System.Windows.Forms.DateTimePicker();
            this.btnAuditoria = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.dgvSondaje = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idUserDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clienteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.proyectoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.geotecnicoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomsondajeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.profundidadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.norteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.esteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cotaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maquinaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lineaCoreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dipDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.azimutDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.entSondajeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panelReporte = new System.Windows.Forms.Panel();
            this.cbFiltros = new System.Windows.Forms.CheckBox();
            this.clbFiltros = new System.Windows.Forms.CheckedListBox();
            this.txtMaquina = new System.Windows.Forms.TextBox();
            this.txtCota = new System.Windows.Forms.TextBox();
            this.txtProfundidad = new System.Windows.Forms.TextBox();
            this.txtGeotecnico = new System.Windows.Forms.TextBox();
            this.txtEste = new System.Windows.Forms.TextBox();
            this.txtFecha = new System.Windows.Forms.TextBox();
            this.txtProyecto = new System.Windows.Forms.TextBox();
            this.txtNorte = new System.Windows.Forms.TextBox();
            this.txtSondaje = new System.Windows.Forms.TextBox();
            this.txtCliente = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnExportar = new System.Windows.Forms.Button();
            this.dgvReporte = new System.Windows.Forms.DataGridView();
            this.nomsondajeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lineaCoreDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dipDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.azimutDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.desdeDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hastaDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numestructuraDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.profundidadeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tBDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alphaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.betaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reftotopDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.condfracturaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jrcDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.espesorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.relleno1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.relleno2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comentarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.geotecnicoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaEsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dipcalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dipdircalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dipsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.strikeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.entConsultaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panelAuditoria = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.dgvAuditoria = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idUserDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellidoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.elementoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.entAuditoriaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panelLimites.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entCorridaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDesde)).BeginInit();
            this.panelFechas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSondaje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entSondajeBindingSource)).BeginInit();
            this.panelReporte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entConsultaBindingSource)).BeginInit();
            this.panelAuditoria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAuditoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entAuditoriaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(31)))), ((int)(((byte)(67)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1176, 33);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::CapaInterfaz.Properties.Resources.cerrar;
            this.pictureBox1.Location = new System.Drawing.Point(1148, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(545, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "REPORTES";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cbTramo);
            this.panel2.Controls.Add(this.cbFecha);
            this.panel2.Controls.Add(this.panelLimites);
            this.panel2.Controls.Add(this.panelFechas);
            this.panel2.Controls.Add(this.btnAuditoria);
            this.panel2.Controls.Add(this.btnBuscar);
            this.panel2.Controls.Add(this.dgvSondaje);
            this.panel2.Location = new System.Drawing.Point(9, 39);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1158, 208);
            this.panel2.TabIndex = 1;
            // 
            // cbTramo
            // 
            this.cbTramo.AutoSize = true;
            this.cbTramo.Location = new System.Drawing.Point(1091, 16);
            this.cbTramo.Name = "cbTramo";
            this.cbTramo.Size = new System.Drawing.Size(56, 17);
            this.cbTramo.TabIndex = 10;
            this.cbTramo.Text = "Tramo";
            this.cbTramo.UseVisualStyleBackColor = true;
            this.cbTramo.CheckedChanged += new System.EventHandler(this.cbTramo_CheckedChanged);
            // 
            // cbFecha
            // 
            this.cbFecha.AutoSize = true;
            this.cbFecha.Location = new System.Drawing.Point(674, 16);
            this.cbFecha.Name = "cbFecha";
            this.cbFecha.Size = new System.Drawing.Size(56, 17);
            this.cbFecha.TabIndex = 9;
            this.cbFecha.Text = "Fecha";
            this.cbFecha.UseVisualStyleBackColor = true;
            this.cbFecha.CheckedChanged += new System.EventHandler(this.cbFecha_CheckedChanged);
            // 
            // panelLimites
            // 
            this.panelLimites.Controls.Add(this.dgvHasta);
            this.panelLimites.Controls.Add(this.dgvDesde);
            this.panelLimites.Enabled = false;
            this.panelLimites.Location = new System.Drawing.Point(736, 16);
            this.panelLimites.Name = "panelLimites";
            this.panelLimites.Size = new System.Drawing.Size(352, 131);
            this.panelLimites.TabIndex = 8;
            // 
            // dgvHasta
            // 
            this.dgvHasta.AllowUserToAddRows = false;
            this.dgvHasta.AllowUserToDeleteRows = false;
            this.dgvHasta.AllowUserToResizeColumns = false;
            this.dgvHasta.AllowUserToResizeRows = false;
            this.dgvHasta.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvHasta.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvHasta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvHasta.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn2,
            this.idUserDataGridViewTextBoxColumn2,
            this.idSondajeDataGridViewTextBoxColumn1,
            this.desdeDataGridViewTextBoxColumn1,
            this.hastaDataGridViewTextBoxColumn1,
            this.perforacionDataGridViewTextBoxColumn1,
            this.recuperacionDataGridViewTextBoxColumn1});
            this.dgvHasta.DataSource = this.entCorridaBindingSource;
            this.dgvHasta.Location = new System.Drawing.Point(202, 10);
            this.dgvHasta.Name = "dgvHasta";
            this.dgvHasta.ReadOnly = true;
            this.dgvHasta.RowHeadersVisible = false;
            this.dgvHasta.Size = new System.Drawing.Size(138, 113);
            this.dgvHasta.TabIndex = 7;
            // 
            // idDataGridViewTextBoxColumn2
            // 
            this.idDataGridViewTextBoxColumn2.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn2.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn2.Name = "idDataGridViewTextBoxColumn2";
            this.idDataGridViewTextBoxColumn2.ReadOnly = true;
            this.idDataGridViewTextBoxColumn2.Visible = false;
            // 
            // idUserDataGridViewTextBoxColumn2
            // 
            this.idUserDataGridViewTextBoxColumn2.DataPropertyName = "IdUser";
            this.idUserDataGridViewTextBoxColumn2.HeaderText = "IdUser";
            this.idUserDataGridViewTextBoxColumn2.Name = "idUserDataGridViewTextBoxColumn2";
            this.idUserDataGridViewTextBoxColumn2.ReadOnly = true;
            this.idUserDataGridViewTextBoxColumn2.Visible = false;
            // 
            // idSondajeDataGridViewTextBoxColumn1
            // 
            this.idSondajeDataGridViewTextBoxColumn1.DataPropertyName = "Id_Sondaje";
            this.idSondajeDataGridViewTextBoxColumn1.HeaderText = "Id_Sondaje";
            this.idSondajeDataGridViewTextBoxColumn1.Name = "idSondajeDataGridViewTextBoxColumn1";
            this.idSondajeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.idSondajeDataGridViewTextBoxColumn1.Visible = false;
            // 
            // desdeDataGridViewTextBoxColumn1
            // 
            this.desdeDataGridViewTextBoxColumn1.DataPropertyName = "Desde";
            this.desdeDataGridViewTextBoxColumn1.HeaderText = "Desde";
            this.desdeDataGridViewTextBoxColumn1.Name = "desdeDataGridViewTextBoxColumn1";
            this.desdeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.desdeDataGridViewTextBoxColumn1.Visible = false;
            // 
            // hastaDataGridViewTextBoxColumn1
            // 
            this.hastaDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.hastaDataGridViewTextBoxColumn1.DataPropertyName = "Hasta";
            this.hastaDataGridViewTextBoxColumn1.HeaderText = "Hasta";
            this.hastaDataGridViewTextBoxColumn1.Name = "hastaDataGridViewTextBoxColumn1";
            this.hastaDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // perforacionDataGridViewTextBoxColumn1
            // 
            this.perforacionDataGridViewTextBoxColumn1.DataPropertyName = "Perforacion";
            this.perforacionDataGridViewTextBoxColumn1.HeaderText = "Perforacion";
            this.perforacionDataGridViewTextBoxColumn1.Name = "perforacionDataGridViewTextBoxColumn1";
            this.perforacionDataGridViewTextBoxColumn1.ReadOnly = true;
            this.perforacionDataGridViewTextBoxColumn1.Visible = false;
            // 
            // recuperacionDataGridViewTextBoxColumn1
            // 
            this.recuperacionDataGridViewTextBoxColumn1.DataPropertyName = "Recuperacion";
            this.recuperacionDataGridViewTextBoxColumn1.HeaderText = "Recuperacion";
            this.recuperacionDataGridViewTextBoxColumn1.Name = "recuperacionDataGridViewTextBoxColumn1";
            this.recuperacionDataGridViewTextBoxColumn1.ReadOnly = true;
            this.recuperacionDataGridViewTextBoxColumn1.Visible = false;
            // 
            // entCorridaBindingSource
            // 
            this.entCorridaBindingSource.DataSource = typeof(CapaEntidad.EntCorrida);
            // 
            // dgvDesde
            // 
            this.dgvDesde.AllowUserToAddRows = false;
            this.dgvDesde.AllowUserToDeleteRows = false;
            this.dgvDesde.AllowUserToResizeColumns = false;
            this.dgvDesde.AllowUserToResizeRows = false;
            this.dgvDesde.AutoGenerateColumns = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDesde.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDesde.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvDesde.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.idUserDataGridViewTextBoxColumn1,
            this.idSondajeDataGridViewTextBoxColumn,
            this.desdeDataGridViewTextBoxColumn,
            this.hastaDataGridViewTextBoxColumn,
            this.perforacionDataGridViewTextBoxColumn,
            this.recuperacionDataGridViewTextBoxColumn});
            this.dgvDesde.DataSource = this.entCorridaBindingSource;
            this.dgvDesde.Location = new System.Drawing.Point(13, 9);
            this.dgvDesde.Name = "dgvDesde";
            this.dgvDesde.ReadOnly = true;
            this.dgvDesde.RowHeadersVisible = false;
            this.dgvDesde.Size = new System.Drawing.Size(138, 113);
            this.dgvDesde.TabIndex = 6;
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            this.idDataGridViewTextBoxColumn1.Visible = false;
            // 
            // idUserDataGridViewTextBoxColumn1
            // 
            this.idUserDataGridViewTextBoxColumn1.DataPropertyName = "IdUser";
            this.idUserDataGridViewTextBoxColumn1.HeaderText = "IdUser";
            this.idUserDataGridViewTextBoxColumn1.Name = "idUserDataGridViewTextBoxColumn1";
            this.idUserDataGridViewTextBoxColumn1.ReadOnly = true;
            this.idUserDataGridViewTextBoxColumn1.Visible = false;
            // 
            // idSondajeDataGridViewTextBoxColumn
            // 
            this.idSondajeDataGridViewTextBoxColumn.DataPropertyName = "Id_Sondaje";
            this.idSondajeDataGridViewTextBoxColumn.HeaderText = "Id_Sondaje";
            this.idSondajeDataGridViewTextBoxColumn.Name = "idSondajeDataGridViewTextBoxColumn";
            this.idSondajeDataGridViewTextBoxColumn.ReadOnly = true;
            this.idSondajeDataGridViewTextBoxColumn.Visible = false;
            // 
            // desdeDataGridViewTextBoxColumn
            // 
            this.desdeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.desdeDataGridViewTextBoxColumn.DataPropertyName = "Desde";
            this.desdeDataGridViewTextBoxColumn.HeaderText = "Desde";
            this.desdeDataGridViewTextBoxColumn.Name = "desdeDataGridViewTextBoxColumn";
            this.desdeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // hastaDataGridViewTextBoxColumn
            // 
            this.hastaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.hastaDataGridViewTextBoxColumn.DataPropertyName = "Hasta";
            this.hastaDataGridViewTextBoxColumn.HeaderText = "Hasta";
            this.hastaDataGridViewTextBoxColumn.Name = "hastaDataGridViewTextBoxColumn";
            this.hastaDataGridViewTextBoxColumn.ReadOnly = true;
            this.hastaDataGridViewTextBoxColumn.Visible = false;
            // 
            // perforacionDataGridViewTextBoxColumn
            // 
            this.perforacionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.perforacionDataGridViewTextBoxColumn.DataPropertyName = "Perforacion";
            this.perforacionDataGridViewTextBoxColumn.HeaderText = "Perforacion";
            this.perforacionDataGridViewTextBoxColumn.Name = "perforacionDataGridViewTextBoxColumn";
            this.perforacionDataGridViewTextBoxColumn.ReadOnly = true;
            this.perforacionDataGridViewTextBoxColumn.Visible = false;
            // 
            // recuperacionDataGridViewTextBoxColumn
            // 
            this.recuperacionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.recuperacionDataGridViewTextBoxColumn.DataPropertyName = "Recuperacion";
            this.recuperacionDataGridViewTextBoxColumn.HeaderText = "Recuperacion";
            this.recuperacionDataGridViewTextBoxColumn.Name = "recuperacionDataGridViewTextBoxColumn";
            this.recuperacionDataGridViewTextBoxColumn.ReadOnly = true;
            this.recuperacionDataGridViewTextBoxColumn.Visible = false;
            // 
            // panelFechas
            // 
            this.panelFechas.Controls.Add(this.label13);
            this.panelFechas.Controls.Add(this.label12);
            this.panelFechas.Controls.Add(this.dtpHasta);
            this.panelFechas.Controls.Add(this.dtpDesde);
            this.panelFechas.Enabled = false;
            this.panelFechas.Location = new System.Drawing.Point(411, 16);
            this.panelFechas.Name = "panelFechas";
            this.panelFechas.Size = new System.Drawing.Size(260, 131);
            this.panelFechas.TabIndex = 7;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(31, 71);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 20);
            this.label13.TabIndex = 12;
            this.label13.Text = "Hasta";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(31, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 20);
            this.label12.TabIndex = 11;
            this.label12.Text = "Desde";
            // 
            // dtpHasta
            // 
            this.dtpHasta.Location = new System.Drawing.Point(31, 102);
            this.dtpHasta.Name = "dtpHasta";
            this.dtpHasta.Size = new System.Drawing.Size(199, 20);
            this.dtpHasta.TabIndex = 10;
            // 
            // dtpDesde
            // 
            this.dtpDesde.Location = new System.Drawing.Point(31, 40);
            this.dtpDesde.Name = "dtpDesde";
            this.dtpDesde.Size = new System.Drawing.Size(199, 20);
            this.dtpDesde.TabIndex = 9;
            // 
            // btnAuditoria
            // 
            this.btnAuditoria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnAuditoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuditoria.Location = new System.Drawing.Point(652, 160);
            this.btnAuditoria.Name = "btnAuditoria";
            this.btnAuditoria.Size = new System.Drawing.Size(85, 33);
            this.btnAuditoria.TabIndex = 6;
            this.btnAuditoria.Text = "Auditar";
            this.btnAuditoria.UseVisualStyleBackColor = false;
            this.btnAuditoria.Visible = false;
            this.btnAuditoria.Click += new System.EventHandler(this.btnAuditoria_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Location = new System.Drawing.Point(420, 160);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(85, 33);
            this.btnBuscar.TabIndex = 3;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = false;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // dgvSondaje
            // 
            this.dgvSondaje.AllowUserToAddRows = false;
            this.dgvSondaje.AllowUserToDeleteRows = false;
            this.dgvSondaje.AllowUserToResizeColumns = false;
            this.dgvSondaje.AllowUserToResizeRows = false;
            this.dgvSondaje.AutoGenerateColumns = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSondaje.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvSondaje.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvSondaje.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.idUserDataGridViewTextBoxColumn,
            this.clienteDataGridViewTextBoxColumn,
            this.proyectoDataGridViewTextBoxColumn,
            this.geotecnicoDataGridViewTextBoxColumn,
            this.nomsondajeDataGridViewTextBoxColumn,
            this.fechaDataGridViewTextBoxColumn,
            this.profundidadDataGridViewTextBoxColumn,
            this.norteDataGridViewTextBoxColumn,
            this.esteDataGridViewTextBoxColumn,
            this.cotaDataGridViewTextBoxColumn,
            this.maquinaDataGridViewTextBoxColumn,
            this.lineaCoreDataGridViewTextBoxColumn,
            this.dipDataGridViewTextBoxColumn,
            this.azimutDataGridViewTextBoxColumn});
            this.dgvSondaje.DataSource = this.entSondajeBindingSource;
            this.dgvSondaje.Location = new System.Drawing.Point(35, 16);
            this.dgvSondaje.Name = "dgvSondaje";
            this.dgvSondaje.ReadOnly = true;
            this.dgvSondaje.RowHeadersVisible = false;
            this.dgvSondaje.Size = new System.Drawing.Size(337, 131);
            this.dgvSondaje.TabIndex = 0;
            this.dgvSondaje.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSondaje_CellMouseClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // idUserDataGridViewTextBoxColumn
            // 
            this.idUserDataGridViewTextBoxColumn.DataPropertyName = "Id_User";
            this.idUserDataGridViewTextBoxColumn.HeaderText = "Id_User";
            this.idUserDataGridViewTextBoxColumn.Name = "idUserDataGridViewTextBoxColumn";
            this.idUserDataGridViewTextBoxColumn.ReadOnly = true;
            this.idUserDataGridViewTextBoxColumn.Visible = false;
            // 
            // clienteDataGridViewTextBoxColumn
            // 
            this.clienteDataGridViewTextBoxColumn.DataPropertyName = "Cliente";
            this.clienteDataGridViewTextBoxColumn.HeaderText = "Cliente";
            this.clienteDataGridViewTextBoxColumn.Name = "clienteDataGridViewTextBoxColumn";
            this.clienteDataGridViewTextBoxColumn.ReadOnly = true;
            this.clienteDataGridViewTextBoxColumn.Visible = false;
            // 
            // proyectoDataGridViewTextBoxColumn
            // 
            this.proyectoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.proyectoDataGridViewTextBoxColumn.DataPropertyName = "Proyecto";
            this.proyectoDataGridViewTextBoxColumn.HeaderText = "Proyecto";
            this.proyectoDataGridViewTextBoxColumn.Name = "proyectoDataGridViewTextBoxColumn";
            this.proyectoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // geotecnicoDataGridViewTextBoxColumn
            // 
            this.geotecnicoDataGridViewTextBoxColumn.DataPropertyName = "Geotecnico";
            this.geotecnicoDataGridViewTextBoxColumn.HeaderText = "Geotecnico";
            this.geotecnicoDataGridViewTextBoxColumn.Name = "geotecnicoDataGridViewTextBoxColumn";
            this.geotecnicoDataGridViewTextBoxColumn.ReadOnly = true;
            this.geotecnicoDataGridViewTextBoxColumn.Visible = false;
            // 
            // nomsondajeDataGridViewTextBoxColumn
            // 
            this.nomsondajeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nomsondajeDataGridViewTextBoxColumn.DataPropertyName = "Nom_sondaje";
            this.nomsondajeDataGridViewTextBoxColumn.HeaderText = "Nombre de sondaje";
            this.nomsondajeDataGridViewTextBoxColumn.Name = "nomsondajeDataGridViewTextBoxColumn";
            this.nomsondajeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fechaDataGridViewTextBoxColumn
            // 
            this.fechaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.fechaDataGridViewTextBoxColumn.DataPropertyName = "Fecha";
            this.fechaDataGridViewTextBoxColumn.HeaderText = "Fecha";
            this.fechaDataGridViewTextBoxColumn.Name = "fechaDataGridViewTextBoxColumn";
            this.fechaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // profundidadDataGridViewTextBoxColumn
            // 
            this.profundidadDataGridViewTextBoxColumn.DataPropertyName = "Profundidad";
            this.profundidadDataGridViewTextBoxColumn.HeaderText = "Profundidad";
            this.profundidadDataGridViewTextBoxColumn.Name = "profundidadDataGridViewTextBoxColumn";
            this.profundidadDataGridViewTextBoxColumn.ReadOnly = true;
            this.profundidadDataGridViewTextBoxColumn.Visible = false;
            // 
            // norteDataGridViewTextBoxColumn
            // 
            this.norteDataGridViewTextBoxColumn.DataPropertyName = "Norte";
            this.norteDataGridViewTextBoxColumn.HeaderText = "Norte";
            this.norteDataGridViewTextBoxColumn.Name = "norteDataGridViewTextBoxColumn";
            this.norteDataGridViewTextBoxColumn.ReadOnly = true;
            this.norteDataGridViewTextBoxColumn.Visible = false;
            // 
            // esteDataGridViewTextBoxColumn
            // 
            this.esteDataGridViewTextBoxColumn.DataPropertyName = "Este";
            this.esteDataGridViewTextBoxColumn.HeaderText = "Este";
            this.esteDataGridViewTextBoxColumn.Name = "esteDataGridViewTextBoxColumn";
            this.esteDataGridViewTextBoxColumn.ReadOnly = true;
            this.esteDataGridViewTextBoxColumn.Visible = false;
            // 
            // cotaDataGridViewTextBoxColumn
            // 
            this.cotaDataGridViewTextBoxColumn.DataPropertyName = "Cota";
            this.cotaDataGridViewTextBoxColumn.HeaderText = "Cota";
            this.cotaDataGridViewTextBoxColumn.Name = "cotaDataGridViewTextBoxColumn";
            this.cotaDataGridViewTextBoxColumn.ReadOnly = true;
            this.cotaDataGridViewTextBoxColumn.Visible = false;
            // 
            // maquinaDataGridViewTextBoxColumn
            // 
            this.maquinaDataGridViewTextBoxColumn.DataPropertyName = "Maquina";
            this.maquinaDataGridViewTextBoxColumn.HeaderText = "Maquina";
            this.maquinaDataGridViewTextBoxColumn.Name = "maquinaDataGridViewTextBoxColumn";
            this.maquinaDataGridViewTextBoxColumn.ReadOnly = true;
            this.maquinaDataGridViewTextBoxColumn.Visible = false;
            // 
            // lineaCoreDataGridViewTextBoxColumn
            // 
            this.lineaCoreDataGridViewTextBoxColumn.DataPropertyName = "Linea_Core";
            this.lineaCoreDataGridViewTextBoxColumn.HeaderText = "Linea_Core";
            this.lineaCoreDataGridViewTextBoxColumn.Name = "lineaCoreDataGridViewTextBoxColumn";
            this.lineaCoreDataGridViewTextBoxColumn.ReadOnly = true;
            this.lineaCoreDataGridViewTextBoxColumn.Visible = false;
            // 
            // dipDataGridViewTextBoxColumn
            // 
            this.dipDataGridViewTextBoxColumn.DataPropertyName = "Dip";
            this.dipDataGridViewTextBoxColumn.HeaderText = "Dip";
            this.dipDataGridViewTextBoxColumn.Name = "dipDataGridViewTextBoxColumn";
            this.dipDataGridViewTextBoxColumn.ReadOnly = true;
            this.dipDataGridViewTextBoxColumn.Visible = false;
            // 
            // azimutDataGridViewTextBoxColumn
            // 
            this.azimutDataGridViewTextBoxColumn.DataPropertyName = "Azimut";
            this.azimutDataGridViewTextBoxColumn.HeaderText = "Azimut";
            this.azimutDataGridViewTextBoxColumn.Name = "azimutDataGridViewTextBoxColumn";
            this.azimutDataGridViewTextBoxColumn.ReadOnly = true;
            this.azimutDataGridViewTextBoxColumn.Visible = false;
            // 
            // entSondajeBindingSource
            // 
            this.entSondajeBindingSource.DataSource = typeof(CapaEntidad.EntSondaje);
            // 
            // panelReporte
            // 
            this.panelReporte.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelReporte.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panelReporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelReporte.Controls.Add(this.cbFiltros);
            this.panelReporte.Controls.Add(this.clbFiltros);
            this.panelReporte.Controls.Add(this.txtMaquina);
            this.panelReporte.Controls.Add(this.txtCota);
            this.panelReporte.Controls.Add(this.txtProfundidad);
            this.panelReporte.Controls.Add(this.txtGeotecnico);
            this.panelReporte.Controls.Add(this.txtEste);
            this.panelReporte.Controls.Add(this.txtFecha);
            this.panelReporte.Controls.Add(this.txtProyecto);
            this.panelReporte.Controls.Add(this.txtNorte);
            this.panelReporte.Controls.Add(this.txtSondaje);
            this.panelReporte.Controls.Add(this.txtCliente);
            this.panelReporte.Controls.Add(this.label11);
            this.panelReporte.Controls.Add(this.label10);
            this.panelReporte.Controls.Add(this.label9);
            this.panelReporte.Controls.Add(this.label8);
            this.panelReporte.Controls.Add(this.label7);
            this.panelReporte.Controls.Add(this.label6);
            this.panelReporte.Controls.Add(this.label5);
            this.panelReporte.Controls.Add(this.label4);
            this.panelReporte.Controls.Add(this.label3);
            this.panelReporte.Controls.Add(this.label2);
            this.panelReporte.Controls.Add(this.btnExportar);
            this.panelReporte.Controls.Add(this.dgvReporte);
            this.panelReporte.Location = new System.Drawing.Point(9, 253);
            this.panelReporte.Name = "panelReporte";
            this.panelReporte.Size = new System.Drawing.Size(1158, 331);
            this.panelReporte.TabIndex = 2;
            this.panelReporte.Visible = false;
            // 
            // cbFiltros
            // 
            this.cbFiltros.AutoSize = true;
            this.cbFiltros.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFiltros.Location = new System.Drawing.Point(1063, 108);
            this.cbFiltros.Name = "cbFiltros";
            this.cbFiltros.Size = new System.Drawing.Size(71, 24);
            this.cbFiltros.TabIndex = 24;
            this.cbFiltros.Text = "Filtros";
            this.cbFiltros.UseVisualStyleBackColor = true;
            this.cbFiltros.CheckedChanged += new System.EventHandler(this.cbFiltros_CheckedChanged);
            // 
            // clbFiltros
            // 
            this.clbFiltros.CheckOnClick = true;
            this.clbFiltros.FormattingEnabled = true;
            this.clbFiltros.Location = new System.Drawing.Point(980, 151);
            this.clbFiltros.Name = "clbFiltros";
            this.clbFiltros.Size = new System.Drawing.Size(154, 244);
            this.clbFiltros.TabIndex = 23;
            this.clbFiltros.Visible = false;
            this.clbFiltros.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbFiltros_ItemCheck);
            // 
            // txtMaquina
            // 
            this.txtMaquina.Location = new System.Drawing.Point(968, 20);
            this.txtMaquina.Name = "txtMaquina";
            this.txtMaquina.Size = new System.Drawing.Size(144, 20);
            this.txtMaquina.TabIndex = 22;
            // 
            // txtCota
            // 
            this.txtCota.Location = new System.Drawing.Point(686, 109);
            this.txtCota.Name = "txtCota";
            this.txtCota.Size = new System.Drawing.Size(144, 20);
            this.txtCota.TabIndex = 21;
            // 
            // txtProfundidad
            // 
            this.txtProfundidad.Location = new System.Drawing.Point(686, 64);
            this.txtProfundidad.Name = "txtProfundidad";
            this.txtProfundidad.Size = new System.Drawing.Size(144, 20);
            this.txtProfundidad.TabIndex = 20;
            // 
            // txtGeotecnico
            // 
            this.txtGeotecnico.Location = new System.Drawing.Point(686, 20);
            this.txtGeotecnico.Name = "txtGeotecnico";
            this.txtGeotecnico.Size = new System.Drawing.Size(144, 20);
            this.txtGeotecnico.TabIndex = 19;
            // 
            // txtEste
            // 
            this.txtEste.Location = new System.Drawing.Point(391, 109);
            this.txtEste.Name = "txtEste";
            this.txtEste.Size = new System.Drawing.Size(144, 20);
            this.txtEste.TabIndex = 18;
            // 
            // txtFecha
            // 
            this.txtFecha.Location = new System.Drawing.Point(391, 64);
            this.txtFecha.Name = "txtFecha";
            this.txtFecha.Size = new System.Drawing.Size(144, 20);
            this.txtFecha.TabIndex = 17;
            // 
            // txtProyecto
            // 
            this.txtProyecto.Location = new System.Drawing.Point(391, 20);
            this.txtProyecto.Name = "txtProyecto";
            this.txtProyecto.Size = new System.Drawing.Size(144, 20);
            this.txtProyecto.TabIndex = 16;
            // 
            // txtNorte
            // 
            this.txtNorte.Location = new System.Drawing.Point(127, 109);
            this.txtNorte.Name = "txtNorte";
            this.txtNorte.Size = new System.Drawing.Size(144, 20);
            this.txtNorte.TabIndex = 15;
            // 
            // txtSondaje
            // 
            this.txtSondaje.Location = new System.Drawing.Point(127, 64);
            this.txtSondaje.Name = "txtSondaje";
            this.txtSondaje.Size = new System.Drawing.Size(144, 20);
            this.txtSondaje.TabIndex = 14;
            // 
            // txtCliente
            // 
            this.txtCliente.Location = new System.Drawing.Point(127, 20);
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.Size = new System.Drawing.Size(144, 20);
            this.txtCliente.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(870, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 18);
            this.label11.TabIndex = 12;
            this.label11.Text = "Máquina";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(571, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 18);
            this.label10.TabIndex = 11;
            this.label10.Text = "Cota";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(51, 108);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 18);
            this.label9.TabIndex = 10;
            this.label9.Text = "Norte";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(304, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 18);
            this.label8.TabIndex = 9;
            this.label8.Text = "Este";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(571, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 18);
            this.label7.TabIndex = 8;
            this.label7.Text = "Profundidad";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(304, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 18);
            this.label6.TabIndex = 7;
            this.label6.Text = "Fecha";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(51, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 18);
            this.label5.TabIndex = 6;
            this.label5.Text = "Sondaje";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(571, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 18);
            this.label4.TabIndex = 5;
            this.label4.Text = "Geotécnico";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(304, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Proyecto";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(51, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Cliente";
            // 
            // btnExportar
            // 
            this.btnExportar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnExportar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportar.Location = new System.Drawing.Point(527, 407);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(102, 33);
            this.btnExportar.TabIndex = 2;
            this.btnExportar.Text = "Exportar";
            this.btnExportar.UseVisualStyleBackColor = false;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // dgvReporte
            // 
            this.dgvReporte.AllowUserToAddRows = false;
            this.dgvReporte.AllowUserToDeleteRows = false;
            this.dgvReporte.AllowUserToResizeColumns = false;
            this.dgvReporte.AllowUserToResizeRows = false;
            this.dgvReporte.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvReporte.AutoGenerateColumns = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReporte.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvReporte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvReporte.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomsondajeDataGridViewTextBoxColumn1,
            this.fechaDataGridViewTextBoxColumn2,
            this.lineaCoreDataGridViewTextBoxColumn1,
            this.dipDataGridViewTextBoxColumn1,
            this.azimutDataGridViewTextBoxColumn1,
            this.desdeDataGridViewTextBoxColumn2,
            this.hastaDataGridViewTextBoxColumn2,
            this.numestructuraDataGridViewTextBoxColumn,
            this.profundidadeDataGridViewTextBoxColumn,
            this.tipoeDataGridViewTextBoxColumn,
            this.tBDataGridViewTextBoxColumn,
            this.alphaDataGridViewTextBoxColumn,
            this.betaDataGridViewTextBoxColumn,
            this.reftotopDataGridViewTextBoxColumn,
            this.condfracturaDataGridViewTextBoxColumn,
            this.jrcDataGridViewTextBoxColumn,
            this.jrDataGridViewTextBoxColumn,
            this.jaDataGridViewTextBoxColumn,
            this.espesorDataGridViewTextBoxColumn,
            this.relleno1DataGridViewTextBoxColumn,
            this.relleno2DataGridViewTextBoxColumn,
            this.comentarioDataGridViewTextBoxColumn,
            this.geotecnicoDataGridViewTextBoxColumn1,
            this.fechaEsDataGridViewTextBoxColumn,
            this.dipcalDataGridViewTextBoxColumn,
            this.dipdircalDataGridViewTextBoxColumn,
            this.dipsDataGridViewTextBoxColumn,
            this.strikeDataGridViewTextBoxColumn});
            this.dgvReporte.DataSource = this.entConsultaBindingSource;
            this.dgvReporte.Location = new System.Drawing.Point(12, 151);
            this.dgvReporte.Name = "dgvReporte";
            this.dgvReporte.ReadOnly = true;
            this.dgvReporte.RowHeadersVisible = false;
            this.dgvReporte.Size = new System.Drawing.Size(1122, 244);
            this.dgvReporte.TabIndex = 0;
            // 
            // nomsondajeDataGridViewTextBoxColumn1
            // 
            this.nomsondajeDataGridViewTextBoxColumn1.DataPropertyName = "Nom_sondaje";
            this.nomsondajeDataGridViewTextBoxColumn1.HeaderText = "Sondaje";
            this.nomsondajeDataGridViewTextBoxColumn1.Name = "nomsondajeDataGridViewTextBoxColumn1";
            this.nomsondajeDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // fechaDataGridViewTextBoxColumn2
            // 
            this.fechaDataGridViewTextBoxColumn2.DataPropertyName = "Fecha";
            this.fechaDataGridViewTextBoxColumn2.HeaderText = "Fecha";
            this.fechaDataGridViewTextBoxColumn2.Name = "fechaDataGridViewTextBoxColumn2";
            this.fechaDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // lineaCoreDataGridViewTextBoxColumn1
            // 
            this.lineaCoreDataGridViewTextBoxColumn1.DataPropertyName = "Linea_Core";
            this.lineaCoreDataGridViewTextBoxColumn1.HeaderText = "Linea de Core";
            this.lineaCoreDataGridViewTextBoxColumn1.Name = "lineaCoreDataGridViewTextBoxColumn1";
            this.lineaCoreDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dipDataGridViewTextBoxColumn1
            // 
            this.dipDataGridViewTextBoxColumn1.DataPropertyName = "Dip";
            this.dipDataGridViewTextBoxColumn1.HeaderText = "Dip";
            this.dipDataGridViewTextBoxColumn1.Name = "dipDataGridViewTextBoxColumn1";
            this.dipDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // azimutDataGridViewTextBoxColumn1
            // 
            this.azimutDataGridViewTextBoxColumn1.DataPropertyName = "Azimut";
            this.azimutDataGridViewTextBoxColumn1.HeaderText = "Azimut";
            this.azimutDataGridViewTextBoxColumn1.Name = "azimutDataGridViewTextBoxColumn1";
            this.azimutDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // desdeDataGridViewTextBoxColumn2
            // 
            this.desdeDataGridViewTextBoxColumn2.DataPropertyName = "Desde";
            this.desdeDataGridViewTextBoxColumn2.HeaderText = "Desde";
            this.desdeDataGridViewTextBoxColumn2.Name = "desdeDataGridViewTextBoxColumn2";
            this.desdeDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // hastaDataGridViewTextBoxColumn2
            // 
            this.hastaDataGridViewTextBoxColumn2.DataPropertyName = "Hasta";
            this.hastaDataGridViewTextBoxColumn2.HeaderText = "Hasta";
            this.hastaDataGridViewTextBoxColumn2.Name = "hastaDataGridViewTextBoxColumn2";
            this.hastaDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // numestructuraDataGridViewTextBoxColumn
            // 
            this.numestructuraDataGridViewTextBoxColumn.DataPropertyName = "Num_estructura";
            this.numestructuraDataGridViewTextBoxColumn.HeaderText = "Número de estructura";
            this.numestructuraDataGridViewTextBoxColumn.Name = "numestructuraDataGridViewTextBoxColumn";
            this.numestructuraDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // profundidadeDataGridViewTextBoxColumn
            // 
            this.profundidadeDataGridViewTextBoxColumn.DataPropertyName = "Profundidad_e";
            this.profundidadeDataGridViewTextBoxColumn.HeaderText = "Profundidad de estructura";
            this.profundidadeDataGridViewTextBoxColumn.Name = "profundidadeDataGridViewTextBoxColumn";
            this.profundidadeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tipoeDataGridViewTextBoxColumn
            // 
            this.tipoeDataGridViewTextBoxColumn.DataPropertyName = "Tipo_e";
            this.tipoeDataGridViewTextBoxColumn.HeaderText = "Tipo de estructura";
            this.tipoeDataGridViewTextBoxColumn.Name = "tipoeDataGridViewTextBoxColumn";
            this.tipoeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tBDataGridViewTextBoxColumn
            // 
            this.tBDataGridViewTextBoxColumn.DataPropertyName = "T_B";
            this.tBDataGridViewTextBoxColumn.HeaderText = "T/B";
            this.tBDataGridViewTextBoxColumn.Name = "tBDataGridViewTextBoxColumn";
            this.tBDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // alphaDataGridViewTextBoxColumn
            // 
            this.alphaDataGridViewTextBoxColumn.DataPropertyName = "Alpha";
            this.alphaDataGridViewTextBoxColumn.HeaderText = "Alpha";
            this.alphaDataGridViewTextBoxColumn.Name = "alphaDataGridViewTextBoxColumn";
            this.alphaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // betaDataGridViewTextBoxColumn
            // 
            this.betaDataGridViewTextBoxColumn.DataPropertyName = "Beta";
            this.betaDataGridViewTextBoxColumn.HeaderText = "Beta";
            this.betaDataGridViewTextBoxColumn.Name = "betaDataGridViewTextBoxColumn";
            this.betaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reftotopDataGridViewTextBoxColumn
            // 
            this.reftotopDataGridViewTextBoxColumn.DataPropertyName = "Ref_to_top";
            this.reftotopDataGridViewTextBoxColumn.HeaderText = "Ref to top";
            this.reftotopDataGridViewTextBoxColumn.Name = "reftotopDataGridViewTextBoxColumn";
            this.reftotopDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // condfracturaDataGridViewTextBoxColumn
            // 
            this.condfracturaDataGridViewTextBoxColumn.DataPropertyName = "Cond_fractura";
            this.condfracturaDataGridViewTextBoxColumn.HeaderText = "Condición de fractura";
            this.condfracturaDataGridViewTextBoxColumn.Name = "condfracturaDataGridViewTextBoxColumn";
            this.condfracturaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // jrcDataGridViewTextBoxColumn
            // 
            this.jrcDataGridViewTextBoxColumn.DataPropertyName = "Jrc";
            this.jrcDataGridViewTextBoxColumn.HeaderText = "Jrc";
            this.jrcDataGridViewTextBoxColumn.Name = "jrcDataGridViewTextBoxColumn";
            this.jrcDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // jrDataGridViewTextBoxColumn
            // 
            this.jrDataGridViewTextBoxColumn.DataPropertyName = "Jr";
            this.jrDataGridViewTextBoxColumn.HeaderText = "Jr";
            this.jrDataGridViewTextBoxColumn.Name = "jrDataGridViewTextBoxColumn";
            this.jrDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // jaDataGridViewTextBoxColumn
            // 
            this.jaDataGridViewTextBoxColumn.DataPropertyName = "Ja";
            this.jaDataGridViewTextBoxColumn.HeaderText = "Ja";
            this.jaDataGridViewTextBoxColumn.Name = "jaDataGridViewTextBoxColumn";
            this.jaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // espesorDataGridViewTextBoxColumn
            // 
            this.espesorDataGridViewTextBoxColumn.DataPropertyName = "Espesor";
            this.espesorDataGridViewTextBoxColumn.HeaderText = "Espesor";
            this.espesorDataGridViewTextBoxColumn.Name = "espesorDataGridViewTextBoxColumn";
            this.espesorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // relleno1DataGridViewTextBoxColumn
            // 
            this.relleno1DataGridViewTextBoxColumn.DataPropertyName = "Relleno_1";
            this.relleno1DataGridViewTextBoxColumn.HeaderText = "Relleno 1";
            this.relleno1DataGridViewTextBoxColumn.Name = "relleno1DataGridViewTextBoxColumn";
            this.relleno1DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // relleno2DataGridViewTextBoxColumn
            // 
            this.relleno2DataGridViewTextBoxColumn.DataPropertyName = "Relleno_2";
            this.relleno2DataGridViewTextBoxColumn.HeaderText = "Relleno 2";
            this.relleno2DataGridViewTextBoxColumn.Name = "relleno2DataGridViewTextBoxColumn";
            this.relleno2DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // comentarioDataGridViewTextBoxColumn
            // 
            this.comentarioDataGridViewTextBoxColumn.DataPropertyName = "Comentario";
            this.comentarioDataGridViewTextBoxColumn.HeaderText = "Comentario";
            this.comentarioDataGridViewTextBoxColumn.Name = "comentarioDataGridViewTextBoxColumn";
            this.comentarioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // geotecnicoDataGridViewTextBoxColumn1
            // 
            this.geotecnicoDataGridViewTextBoxColumn1.DataPropertyName = "Geotecnico";
            this.geotecnicoDataGridViewTextBoxColumn1.HeaderText = "Geotecnico";
            this.geotecnicoDataGridViewTextBoxColumn1.Name = "geotecnicoDataGridViewTextBoxColumn1";
            this.geotecnicoDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // fechaEsDataGridViewTextBoxColumn
            // 
            this.fechaEsDataGridViewTextBoxColumn.DataPropertyName = "FechaEs";
            this.fechaEsDataGridViewTextBoxColumn.HeaderText = "FechaEs";
            this.fechaEsDataGridViewTextBoxColumn.Name = "fechaEsDataGridViewTextBoxColumn";
            this.fechaEsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dipcalDataGridViewTextBoxColumn
            // 
            this.dipcalDataGridViewTextBoxColumn.DataPropertyName = "Dip_cal";
            this.dipcalDataGridViewTextBoxColumn.HeaderText = "Dip calculado";
            this.dipcalDataGridViewTextBoxColumn.Name = "dipcalDataGridViewTextBoxColumn";
            this.dipcalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dipdircalDataGridViewTextBoxColumn
            // 
            this.dipdircalDataGridViewTextBoxColumn.DataPropertyName = "Dip_dir_cal";
            this.dipdircalDataGridViewTextBoxColumn.HeaderText = "Dip direction calculado";
            this.dipdircalDataGridViewTextBoxColumn.Name = "dipdircalDataGridViewTextBoxColumn";
            this.dipdircalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dipsDataGridViewTextBoxColumn
            // 
            this.dipsDataGridViewTextBoxColumn.DataPropertyName = "Dips";
            this.dipsDataGridViewTextBoxColumn.HeaderText = "Dip";
            this.dipsDataGridViewTextBoxColumn.Name = "dipsDataGridViewTextBoxColumn";
            this.dipsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // strikeDataGridViewTextBoxColumn
            // 
            this.strikeDataGridViewTextBoxColumn.DataPropertyName = "Strike";
            this.strikeDataGridViewTextBoxColumn.HeaderText = "Strike";
            this.strikeDataGridViewTextBoxColumn.Name = "strikeDataGridViewTextBoxColumn";
            this.strikeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // entConsultaBindingSource
            // 
            this.entConsultaBindingSource.DataSource = typeof(CapaEntidad.EntConsulta);
            // 
            // panelAuditoria
            // 
            this.panelAuditoria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelAuditoria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panelAuditoria.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelAuditoria.Controls.Add(this.label14);
            this.panelAuditoria.Controls.Add(this.dgvAuditoria);
            this.panelAuditoria.Location = new System.Drawing.Point(9, 609);
            this.panelAuditoria.Name = "panelAuditoria";
            this.panelAuditoria.Size = new System.Drawing.Size(1158, 80);
            this.panelAuditoria.TabIndex = 23;
            this.panelAuditoria.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(536, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 24);
            this.label14.TabIndex = 4;
            this.label14.Text = "Auditoria";
            // 
            // dgvAuditoria
            // 
            this.dgvAuditoria.AllowUserToAddRows = false;
            this.dgvAuditoria.AllowUserToDeleteRows = false;
            this.dgvAuditoria.AllowUserToResizeColumns = false;
            this.dgvAuditoria.AllowUserToResizeRows = false;
            this.dgvAuditoria.AutoGenerateColumns = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAuditoria.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvAuditoria.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvAuditoria.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn3,
            this.idUserDataGridViewTextBoxColumn3,
            this.nombreDataGridViewTextBoxColumn,
            this.apellidoDataGridViewTextBoxColumn,
            this.elementoDataGridViewTextBoxColumn,
            this.accionDataGridViewTextBoxColumn,
            this.fechaDataGridViewTextBoxColumn1});
            this.dgvAuditoria.DataSource = this.entAuditoriaBindingSource;
            this.dgvAuditoria.Location = new System.Drawing.Point(12, 54);
            this.dgvAuditoria.Name = "dgvAuditoria";
            this.dgvAuditoria.ReadOnly = true;
            this.dgvAuditoria.RowHeadersVisible = false;
            this.dgvAuditoria.Size = new System.Drawing.Size(1132, 203);
            this.dgvAuditoria.TabIndex = 3;
            // 
            // idDataGridViewTextBoxColumn3
            // 
            this.idDataGridViewTextBoxColumn3.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn3.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn3.Name = "idDataGridViewTextBoxColumn3";
            this.idDataGridViewTextBoxColumn3.ReadOnly = true;
            this.idDataGridViewTextBoxColumn3.Visible = false;
            // 
            // idUserDataGridViewTextBoxColumn3
            // 
            this.idUserDataGridViewTextBoxColumn3.DataPropertyName = "Id_User";
            this.idUserDataGridViewTextBoxColumn3.HeaderText = "Id_User";
            this.idUserDataGridViewTextBoxColumn3.Name = "idUserDataGridViewTextBoxColumn3";
            this.idUserDataGridViewTextBoxColumn3.ReadOnly = true;
            this.idUserDataGridViewTextBoxColumn3.Visible = false;
            // 
            // nombreDataGridViewTextBoxColumn
            // 
            this.nombreDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nombreDataGridViewTextBoxColumn.DataPropertyName = "Nombre";
            this.nombreDataGridViewTextBoxColumn.HeaderText = "Nombre";
            this.nombreDataGridViewTextBoxColumn.Name = "nombreDataGridViewTextBoxColumn";
            this.nombreDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // apellidoDataGridViewTextBoxColumn
            // 
            this.apellidoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.apellidoDataGridViewTextBoxColumn.DataPropertyName = "Apellido";
            this.apellidoDataGridViewTextBoxColumn.HeaderText = "Apellido";
            this.apellidoDataGridViewTextBoxColumn.Name = "apellidoDataGridViewTextBoxColumn";
            this.apellidoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // elementoDataGridViewTextBoxColumn
            // 
            this.elementoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.elementoDataGridViewTextBoxColumn.DataPropertyName = "Elemento";
            this.elementoDataGridViewTextBoxColumn.HeaderText = "Elemento";
            this.elementoDataGridViewTextBoxColumn.Name = "elementoDataGridViewTextBoxColumn";
            this.elementoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // accionDataGridViewTextBoxColumn
            // 
            this.accionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.accionDataGridViewTextBoxColumn.DataPropertyName = "Accion";
            this.accionDataGridViewTextBoxColumn.HeaderText = "Accion";
            this.accionDataGridViewTextBoxColumn.Name = "accionDataGridViewTextBoxColumn";
            this.accionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fechaDataGridViewTextBoxColumn1
            // 
            this.fechaDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.fechaDataGridViewTextBoxColumn1.DataPropertyName = "Fecha";
            this.fechaDataGridViewTextBoxColumn1.HeaderText = "Fecha";
            this.fechaDataGridViewTextBoxColumn1.Name = "fechaDataGridViewTextBoxColumn1";
            this.fechaDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // entAuditoriaBindingSource
            // 
            this.entAuditoriaBindingSource.DataSource = typeof(CapaEntidad.EntAuditoria);
            // 
            // VistaReportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1176, 701);
            this.Controls.Add(this.panelAuditoria);
            this.Controls.Add(this.panelReporte);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "VistaReportes";
            this.Text = "VistaReportes";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelLimites.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entCorridaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDesde)).EndInit();
            this.panelFechas.ResumeLayout(false);
            this.panelFechas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSondaje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entSondajeBindingSource)).EndInit();
            this.panelReporte.ResumeLayout(false);
            this.panelReporte.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entConsultaBindingSource)).EndInit();
            this.panelAuditoria.ResumeLayout(false);
            this.panelAuditoria.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAuditoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entAuditoriaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panelReporte;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.DataGridView dgvSondaje;
        private System.Windows.Forms.DataGridView dgvReporte;
        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtMaquina;
        private System.Windows.Forms.TextBox txtCota;
        private System.Windows.Forms.TextBox txtProfundidad;
        private System.Windows.Forms.TextBox txtGeotecnico;
        private System.Windows.Forms.TextBox txtEste;
        private System.Windows.Forms.TextBox txtFecha;
        private System.Windows.Forms.TextBox txtProyecto;
        private System.Windows.Forms.TextBox txtNorte;
        private System.Windows.Forms.TextBox txtSondaje;
        private System.Windows.Forms.TextBox txtCliente;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource entSondajeBindingSource;
        private System.Windows.Forms.BindingSource entCorridaBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idUserDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clienteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn proyectoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn geotecnicoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomsondajeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn profundidadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn norteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn esteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cotaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maquinaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lineaCoreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dipDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn azimutDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnAuditoria;
        private System.Windows.Forms.Panel panelAuditoria;
        private System.Windows.Forms.DataGridView dgvAuditoria;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn idUserDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellidoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn elementoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn accionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource entAuditoriaBindingSource;
        private System.Windows.Forms.Panel panelLimites;
        private System.Windows.Forms.DataGridView dgvHasta;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idUserDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSondajeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn desdeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn hastaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn perforacionDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn recuperacionDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridView dgvDesde;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idUserDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSondajeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn desdeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hastaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn perforacionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn recuperacionDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel panelFechas;
        private System.Windows.Forms.CheckBox cbFecha;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtpHasta;
        private System.Windows.Forms.DateTimePicker dtpDesde;
        private System.Windows.Forms.CheckBox cbTramo;
        private System.Windows.Forms.DataGridViewTextBoxColumn rumboaziDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn buzamientoDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource entConsultaBindingSource;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckedListBox clbFiltros;
        private System.Windows.Forms.CheckBox cbFiltros;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomsondajeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn lineaCoreDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dipDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn azimutDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn desdeDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn hastaDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn numestructuraDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn profundidadeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tBDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn alphaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn betaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reftotopDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn condfracturaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jrcDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jrDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn espesorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn relleno1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn relleno2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn comentarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn geotecnicoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaEsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dipcalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dipdircalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dipsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn strikeDataGridViewTextBoxColumn;
    }
}