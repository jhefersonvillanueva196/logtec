﻿using CapaEntidad;
using CapaLogica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Runtime.InteropServices;

namespace CapaInterfaz
{
    public partial class VistaEstereografia : Form
    {
        public VistaEstereografia()
        {
            InitializeComponent();
            ListarSondaje();
            ListarCabeceras();
        }

        private void ListarCabeceras()
        {
            foreach (DataGridViewColumn column in dgvEstructura1.Columns)
            {
                if (column.Visible == true)
                {
                    clbFiltros.Items.Add(column.HeaderText, true);
                }
                else
                {
                    clbFiltros.Items.Add(column.HeaderText, false);
                }
                
            }
        }

        public void ListarSondaje()
        {
            var lista = LogSondaje.Instancia.ListarSondajes();
            foreach (EntSondaje sondaje in lista)
            {
                clbSondajes.Items.Add(sondaje.Nom_sondaje);
            }
        }

        public void ListarCorrida(string id)
        {
            dgvDesde.DataSource = LogCorrida.Instancia.ListarCorridasByIdSondaje(id);
            dgvHasta.DataSource = LogCorrida.Instancia.ListarCorridasByIdSondaje(id);
        }

        private void clbSondajes_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (clbSondajes.CheckedItems.Count > 4)
            {
                MessageBox.Show("Maximo puedes seleccionar 4 sondajes");
                e.NewValue = CheckState.Unchecked;
            }
        }

        public void ListarEstructura(DataGridView tabla, string son)
        {
            tabla.DataSource = LogEstructura.Instancia.ListarEstructurasSondaje(son);
        }

        private void clbSondajes_SelectedValueChanged(object sender, EventArgs e)
        {
            if (clbSondajes.CheckedItems.Count > 1)
            {
                cbTramo.Enabled = false;
                dgvDesde.DataSource = null;
                dgvHasta.DataSource = null;
            }
            else
            {
                if (clbSondajes.CheckedItems.Count == 1)
                {

                    var lista = LogSondaje.Instancia.ListarSondajes();
                    var nombre = clbSondajes.CheckedItems;
                    foreach (var item in nombre)
                    {
                        var sondaje = lista.FirstOrDefault(s => s.Nom_sondaje == item.ToString());
                        ListarCorrida(sondaje.Id);
                    }
                    cbTramo.Enabled = true;
                }
                else
                {
                    if (clbSondajes.CheckedItems.Count == 0)
                    {
                        cbTramo.Enabled = true;
                        dgvDesde.DataSource = null;
                        dgvHasta.DataSource = null;
                    }
                }
            }
        }        

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            buscar();
        }

        private void buscar()
        {
            cbFiltros.Checked = false;
            int band = 0;
            var marcados = clbSondajes.CheckedItems;
            if (marcados.Count == 1)
            {
                dgvEstructura1.Size = new Size(1120, 311);
                dgvEstructura1.Visible = true;
                lbSondaje1.Visible = true;
                dgvEstructura2.Visible = false;
                dgvEstructura3.Visible = false;
                dgvEstructura4.Visible = false;
                lbSondaje2.Visible = false;
                lbSondaje3.Visible = false;
                lbSondaje4.Visible = false;
            }
            else
            {
                if (marcados.Count == 2)
                {
                    dgvEstructura1.Size = new Size(1120, 136);
                    dgvEstructura1.Visible = true;
                    dgvEstructura2.Size = new Size(1120, 136);
                    dgvEstructura2.Location = new Point(19, 241);
                    dgvEstructura2.Visible = true;
                    dgvEstructura3.Visible = false;
                    dgvEstructura4.Visible = false;
                    lbSondaje1.Visible = true;
                    lbSondaje2.Location = new Point(15, 218);
                    lbSondaje2.Visible = true;
                    lbSondaje3.Visible = false;
                    lbSondaje4.Visible = false;
                }
                else
                {
                    if (marcados.Count == 3)
                    {
                        dgvEstructura1.Size = new Size(542, 136);
                        dgvEstructura1.Visible = true;
                        dgvEstructura2.Size = new Size(542, 136);
                        dgvEstructura2.Location = new Point(596, 66);
                        dgvEstructura2.Visible = true;
                        dgvEstructura3.Size = new Size(1120, 136);
                        dgvEstructura3.Location = new Point(19, 241);
                        dgvEstructura3.Visible = true;
                        dgvEstructura4.Visible = false;
                        lbSondaje1.Visible = true;
                        lbSondaje2.Location = new Point(592, 40);
                        lbSondaje2.Visible = true;
                        lbSondaje3.Location = new Point(15, 218);
                        lbSondaje3.Visible = true;
                        lbSondaje4.Visible = false;
                    }
                    else
                    {
                        if (marcados.Count == 4)
                        {
                            dgvEstructura1.Size = new Size(542, 136);
                            dgvEstructura1.Visible = true;
                            dgvEstructura2.Size = new Size(542, 136);
                            dgvEstructura2.Location = new Point(596, 66);
                            dgvEstructura2.Visible = true;
                            dgvEstructura3.Size = new Size(542, 136);
                            dgvEstructura3.Location = new Point(19, 241);
                            dgvEstructura3.Visible = true;
                            dgvEstructura4.Size = new Size(542, 136);
                            dgvEstructura4.Location = new Point(596, 241);
                            dgvEstructura4.Visible = true;
                            lbSondaje1.Visible = true;
                            lbSondaje2.Location = new Point(592, 40);
                            lbSondaje2.Visible = true;
                            lbSondaje3.Location = new Point(15, 218);
                            lbSondaje3.Visible = true;
                            lbSondaje4.Location = new Point(592, 218);
                            lbSondaje4.Visible = true;
                        }
                    }
                }
            }

            for (int i = 0; i < 4; i++)
            {
                bool aux = File.Exists(@"estructuras" + (i + 1).ToString() + ".txt");
                if (aux == true)
                {
                    File.Delete(@"estructuras" + (i + 1).ToString() + ".txt");
                }
            }

            foreach (var son in marcados)
            {
                band++;
                var lista = LogSondaje.Instancia.ListarSondajes();
                var sondaje = lista.First(s => s.Nom_sondaje == son.ToString());
                if (band == 1)
                {
                    Filtros(dgvEstructura1, sondaje.Id, band);
                    lbSondaje1.Text = "Sondaje: " + son;
                }
                else
                {
                    if (band == 2)
                    {
                        Filtros(dgvEstructura2, sondaje.Id, band);
                        lbSondaje2.Text = "Sondaje: " + son;
                    }
                    else
                    {
                        if (band == 3)
                        {
                            Filtros(dgvEstructura3, sondaje.Id, band);
                            lbSondaje3.Text = "Sondaje: " + son;
                        }
                        else
                        {
                            if (band == 4)
                            {
                                Filtros(dgvEstructura4, sondaje.Id, band);
                                lbSondaje4.Text = "Sondaje: " + son;
                            }
                        }
                    }
                }
            }
        }

        private void cbTramo_CheckedChanged(object sender, EventArgs e)
        {
            if (cbTramo.Checked == true)
            {
                panelLimites.Enabled = true;
            }
            else
            {
                panelLimites.Enabled = false;
            }
        }

        private void cbTipoEstructura_CheckedChanged(object sender, EventArgs e)
        {
            if (cbTipoEstructura.Checked == true)
            {
                clbTipoEstrcuturas.Enabled = true;
            }
            else
            {
                clbTipoEstrcuturas.Enabled = false;
            }
        }

        private async void btnGenerar_Click(object sender, EventArgs e)
        {
            if (cbTipoDiagrama.Text == "")
            {
                MessageBox.Show("Debe seleccionar un tipo de grafico");
            }
            else
            {
                if (cbTipoDiagrama.Text == "Diagrama de Planos")
                {
                    Task pltask = new Task(CrearPlanar);
                    pltask.Start();
                    await pltask;
                }
                else
                {
                    if (cbTipoDiagrama.Text == "Concentracion de polos")
                    {
                        Task potask = new Task(CrearPolos);
                        potask.Start();
                        await potask;
                    }
                    else
                    {
                        if (cbTipoDiagrama.Text == "Diagrama de Rosette")
                        {
                            Task rotask = new Task(CrearRoseta);
                            rotask.Start();
                            await rotask;
                        }
                    }
                }
            }
        }

        private void CrearPlanar()
        {
            var psi = new ProcessStartInfo();
            psi.FileName = @"Python310\python.exe";

            var scripts = @"Resources\planar.py";

            psi.Arguments = $"\"{scripts}\"";

            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;

            var errors = "";
            var result = "";

            using (var process = Process.Start(psi))
            {
                errors = process.StandardError.ReadToEnd();
                result = process.StandardOutput.ReadToEnd();
            }
        }

        private void CrearPolos()
        {
            var psi = new ProcessStartInfo();
            psi.FileName = @"Python310\python.exe";

            var scripts = @"Resources\polo.py";

            psi.Arguments = $"\"{scripts}\"";

            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;

            var errors = "";
            var result = "";

            using (var process = Process.Start(psi))
            {
                errors = process.StandardError.ReadToEnd();
                result = process.StandardOutput.ReadToEnd();
            }
        }

        private void CrearRoseta()
        {
            var psi = new ProcessStartInfo();
            psi.FileName = @"Python310\python.exe";

            var scripts = @"Resources\rose.py";

            psi.Arguments = $"\"{scripts}\"";

            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;

            var errors = "";
            var result = "";

            using (var process = Process.Start(psi))
            {
                errors = process.StandardError.ReadToEnd();
                result = process.StandardOutput.ReadToEnd();
            }
        }

        public void GenerarTxt(List<EntEstructura> lista, int ban)
        {            
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"estructuras" + ban.ToString() + ".txt"))
            {
                foreach (EntEstructura list in lista)
                {
                    if(list.Strike !=-1 || list.Dips != -1)
                    {
                        file.WriteLine(list.Strike + "!" + list.Dips + "!" + list.Tipo_e + "!");
                    }                    
                }
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            export();
            
        }

        private void export()
        {
            Excel.Application oXL = new Excel.Application();
            Excel._Workbook oWB;
            Excel._Worksheet oSheet;
            Excel.Range oRng;
            
            oWB = oXL.Workbooks.Add(Missing.Value);

            var marcados = clbSondajes.CheckedItems;

            int i = 0;
            var lista = LogSondaje.Instancia.ListarSondajes();

            foreach (var item in marcados)
            {
                DataGridView data = new DataGridView();

                i++;
                if (i == 1)
                {
                    data = dgvEstructura1;
                }
                else
                {
                    if (i == 2)
                    {
                        data = dgvEstructura2;
                    }
                    else
                    {

                        if (i == 3)
                        {
                            data = dgvEstructura3;
                        }
                        else
                        {
                            if (i == 4)
                            {
                                data = dgvEstructura4;
                            }
                        }
                    }
                }

                var sondaje = lista.FirstOrDefault(s => s.Nom_sondaje == item.ToString());

                try
                {
                    oSheet = oWB.Worksheets.Add();
                    oSheet.Name = sondaje.Nom_sondaje;

                    oRng = oSheet.get_Range("C1", "L1");
                    oRng.Merge(true);
                    oRng.Value2 = "REPORTE ESTEREOGRÁFICO";
                    oRng.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                    oSheet.Cells[2, 3] = "Cliente:";
                    oSheet.Cells[2, 4] = sondaje.Cliente;

                    oSheet.Cells[2, 6] = "Proyecto:";
                    oSheet.Cells[2, 7] = sondaje.Proyecto;

                    oSheet.Cells[2, 9] = "Geotécnico:";
                    oSheet.Cells[2, 10] = sondaje.Geotecnico;

                    oSheet.Cells[3, 3] = "Sondaje:";
                    oSheet.Cells[3, 4] = sondaje.Nom_sondaje;

                    oSheet.Cells[3, 6] = "Fecha:";
                    oSheet.Cells[3, 7] = sondaje.Fecha;


                    oSheet.Cells[3, 9] = "Profundidad:";
                    oSheet.Cells[3, 10] = sondaje.Profundidad;

                    oSheet.Cells[4, 3] = "Norte:";
                    oSheet.Cells[4, 4] = sondaje.Norte;

                    oSheet.Cells[4, 6] = "Este:";
                    oSheet.Cells[4, 7] = sondaje.Este;

                    oSheet.Cells[4, 9] = "Cota:";
                    oSheet.Cells[4, 10] = sondaje.Cota;

                    oSheet.Cells[4, 12] = "Maquina:";
                    oSheet.Cells[4, 13] = sondaje.Maquina;

                    int indicecolumna = 0;
                    int indicefila = 6;

                    foreach (DataGridViewColumn columna in data.Columns)
                    {
                        indicecolumna++;
                        if (columna.Visible == true)
                        {
                            oSheet.Cells[7, indicecolumna] = columna.HeaderText;
                            oRng = oSheet.Cells[7, indicecolumna];
                            oRng.Interior.ColorIndex = 23;
                            oRng.EntireColumn.AutoFit();
                            oRng.Borders.Weight = Excel.XlBorderWeight.xlThin;
                            oRng.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                        }
                        else
                        {
                            indicecolumna--;
                        }
                    }

                    foreach (DataGridViewRow fila in data.Rows)
                    {
                        indicefila++;
                        indicecolumna = 0;
                        foreach (DataGridViewColumn columna in data.Columns)
                        {
                            indicecolumna++;
                            if (columna.Visible == true)
                            {
                                oSheet.Cells[indicefila + 1, indicecolumna] = fila.Cells[columna.Name].Value;

                                oRng = oSheet.Cells[indicefila + 1, indicecolumna];
                                oRng.EntireColumn.AutoFit();
                                oRng.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                            }
                            else
                            {
                                indicecolumna--;
                            }
                        }
                    }
                }
                catch
                {

                }

            }

            oXL.Visible = true;
            oXL.UserControl = true;

            oXL = null;
            oWB = null;
            oSheet = null;
            oRng = null;

        }
        
        private void Filtros(DataGridView tabla, string sond, int band)
        {
            int i = 0, f = 0;
            double tramoI, tramoF;
            if (cbTipoEstructura.Checked == true && cbTramo.Checked == false)
            {
                List<string> tipoestructuras = new List<string>();
                var marcados = clbTipoEstrcuturas.CheckedItems;
                foreach (string item in marcados)
                {
                    tipoestructuras.Add(item);
                }

                tabla.DataSource = null;
                tabla.DataSource = LogEstructura.Instancia.ListarEstructuraTipoEstruc(sond, tipoestructuras);
                var lista = LogEstructura.Instancia.ListarEstructuraTipoEstruc(sond, tipoestructuras);
                GenerarTxt(lista, band);
                panelData.Visible = true;
            }
            else
            {
                if (cbTramo.Checked == true && cbTipoEstructura.Checked == false)
                {
                    i = dgvDesde.CurrentRow.Index;
                    f = dgvHasta.CurrentRow.Index;
                    tramoI = double.Parse(dgvDesde.Rows[i].Cells[3].Value.ToString());
                    tramoF = double.Parse(dgvHasta.Rows[f].Cells[4].Value.ToString());
                    if (tramoI > tramoF)
                    {
                        MessageBox.Show("El limite inferior no puede ser mayor que el limite superior verifica tu consulta");
                    }
                    else
                    {
                        if (tramoI == tramoF)
                        {
                            MessageBox.Show("No puedes escoger un solo punto");
                        }
                        else
                        {
                            tabla.DataSource = null;
                            tabla.DataSource = LogEstructura.Instancia.ListarEstructuraTramo(sond, tramoI, tramoF);
                            var lista = LogEstructura.Instancia.ListarEstructuraTramo(sond, tramoI, tramoF);
                            GenerarTxt(lista, band);
                            panelData.Visible = true;
                        }
                    }
                }
                else
                {
                    if (cbTramo.Checked == true && cbTipoEstructura.Checked == true)
                    {
                        i = dgvDesde.CurrentRow.Index;
                        f = dgvHasta.CurrentRow.Index;
                        tramoI = double.Parse(dgvDesde.Rows[i].Cells[3].Value.ToString());
                        tramoF = double.Parse(dgvDesde.Rows[f].Cells[4].Value.ToString());
                        if (tramoI > tramoF)
                        {
                            MessageBox.Show("Los tramos no coinciden, verifica tus limites");
                        }
                        else
                        {
                            if (tramoI == tramoF)
                            {
                                MessageBox.Show("No puedes escoger un solo punto");
                            }
                            else
                            {
                                List<string> tipoestructuras = new List<string>();
                                var marcados = clbTipoEstrcuturas.CheckedItems;
                                foreach (string item in marcados)
                                {
                                    tipoestructuras.Add(item);
                                }

                                tabla.DataSource = null;
                                tabla.DataSource = LogEstructura.Instancia.ListarEstructuraTramoTipoEstruc(sond, tramoI, tramoF, tipoestructuras);
                                var lista = LogEstructura.Instancia.ListarEstructuraTramoTipoEstruc(sond, tramoI, tramoF, tipoestructuras);
                                GenerarTxt(lista, band);
                                panelData.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        if (cbTramo.Checked == false && cbTipoEstructura.Checked == false)
                        {
                            ListarEstructura(tabla, sond);
                            var lista = LogEstructura.Instancia.ListarEstructurasSondaje(sond);
                            GenerarTxt(lista, band);
                            panelData.Visible = true;
                        }
                    }
                }
            }
        }

        private void cbFiltros_CheckedChanged(object sender, EventArgs e)
        {
            var marcados = clbSondajes.CheckedItems;
            if (cbFiltros.Checked == true)
            {
                clbFiltros.Visible = true;
                clbFiltros.Visible = true; if (marcados.Count == 1)
                {
                    dgvEstructura1.Size = new Size(983, 311);
                    dgvEstructura1.Visible = true;
                    lbSondaje1.Visible = true;
                    dgvEstructura2.Visible = false;
                    dgvEstructura3.Visible = false;
                    dgvEstructura4.Visible = false;
                    lbSondaje2.Visible = false;
                    lbSondaje3.Visible = false;
                    lbSondaje4.Visible = false;
                }
                else
                {
                    if (marcados.Count == 2)
                    {
                        dgvEstructura1.Size = new Size(983, 136);
                        dgvEstructura1.Visible = true;
                        dgvEstructura2.Size = new Size(983, 136);
                        dgvEstructura2.Location = new Point(19, 241);
                        dgvEstructura2.Visible = true;
                        dgvEstructura3.Visible = false;
                        dgvEstructura4.Visible = false;
                        lbSondaje1.Visible = true;
                        lbSondaje2.Location = new Point(15, 218);
                        lbSondaje2.Visible = true;
                        lbSondaje3.Visible = false;
                        lbSondaje4.Visible = false;
                    }
                    else
                    {
                        if (marcados.Count == 3)
                        {
                            dgvEstructura1.Size = new Size(477, 136);
                            dgvEstructura1.Visible = true;
                            dgvEstructura2.Size = new Size(477, 136);
                            dgvEstructura2.Location = new Point(525, 66);
                            dgvEstructura2.Visible = true;
                            dgvEstructura3.Size = new Size(983, 136);
                            dgvEstructura3.Location = new Point(19, 241);
                            dgvEstructura3.Visible = true;
                            dgvEstructura4.Visible = false;
                            lbSondaje1.Visible = true;
                            lbSondaje2.Location = new Point(521, 40);
                            lbSondaje2.Visible = true;
                            lbSondaje3.Location = new Point(15, 218);
                            lbSondaje3.Visible = true;
                            lbSondaje4.Visible = false;
                        }
                        else
                        {
                            if (marcados.Count == 4)
                            {
                                dgvEstructura1.Size = new Size(477, 136);
                                dgvEstructura1.Visible = true;
                                dgvEstructura2.Size = new Size(477, 136);
                                dgvEstructura2.Location = new Point(525, 66);
                                dgvEstructura2.Visible = true;
                                dgvEstructura3.Size = new Size(477, 136);
                                dgvEstructura3.Location = new Point(19, 241);
                                dgvEstructura3.Visible = true;
                                dgvEstructura4.Size = new Size(477, 136);
                                dgvEstructura4.Location = new Point(525, 241);
                                dgvEstructura4.Visible = true;
                                lbSondaje1.Visible = true;
                                lbSondaje2.Location = new Point(521, 40);
                                lbSondaje2.Visible = true;
                                lbSondaje3.Location = new Point(15, 218);
                                lbSondaje3.Visible = true;
                                lbSondaje4.Location = new Point(521, 218);
                                lbSondaje4.Visible = true;
                            }
                        }
                    }
                }

            }
            else
            {
                clbFiltros.Visible = false;
                clbFiltros.Visible = false; if (marcados.Count == 1)
                {
                    dgvEstructura1.Size = new Size(1120, 311);
                    dgvEstructura1.Visible = true;
                    lbSondaje1.Visible = true;
                    dgvEstructura2.Visible = false;
                    dgvEstructura3.Visible = false;
                    dgvEstructura4.Visible = false;
                    lbSondaje2.Visible = false;
                    lbSondaje3.Visible = false;
                    lbSondaje4.Visible = false;
                }
                else
                {
                    if (marcados.Count == 2)
                    {
                        dgvEstructura1.Size = new Size(1120, 136);
                        dgvEstructura1.Visible = true;
                        dgvEstructura2.Size = new Size(1120, 136);
                        dgvEstructura2.Location = new Point(19, 241);
                        dgvEstructura2.Visible = true;
                        dgvEstructura3.Visible = false;
                        dgvEstructura4.Visible = false;
                        lbSondaje1.Visible = true;
                        lbSondaje2.Location = new Point(15, 218);
                        lbSondaje2.Visible = true;
                        lbSondaje3.Visible = false;
                        lbSondaje4.Visible = false;
                    }
                    else
                    {
                        if (marcados.Count == 3)
                        {
                            dgvEstructura1.Size = new Size(542, 136);
                            dgvEstructura1.Visible = true;
                            dgvEstructura2.Size = new Size(542, 136);
                            dgvEstructura2.Location = new Point(596, 66);
                            dgvEstructura2.Visible = true;
                            dgvEstructura3.Size = new Size(1120, 136);
                            dgvEstructura3.Location = new Point(19, 241);
                            dgvEstructura3.Visible = true;
                            dgvEstructura4.Visible = false;
                            lbSondaje1.Visible = true;
                            lbSondaje2.Location = new Point(592, 40);
                            lbSondaje2.Visible = true;
                            lbSondaje3.Location = new Point(15, 218);
                            lbSondaje3.Visible = true;
                            lbSondaje4.Visible = false;
                        }
                        else
                        {
                            if (marcados.Count == 4)
                            {
                                dgvEstructura1.Size = new Size(542, 136);
                                dgvEstructura1.Visible = true;
                                dgvEstructura2.Size = new Size(542, 136);
                                dgvEstructura2.Location = new Point(596, 66);
                                dgvEstructura2.Visible = true;
                                dgvEstructura3.Size = new Size(542, 136);
                                dgvEstructura3.Location = new Point(19, 241);
                                dgvEstructura3.Visible = true;
                                dgvEstructura4.Size = new Size(542, 136);
                                dgvEstructura4.Location = new Point(596, 241);
                                dgvEstructura4.Visible = true;
                                lbSondaje1.Visible = true;
                                lbSondaje2.Location = new Point(592, 40);
                                lbSondaje2.Visible = true;
                                lbSondaje3.Location = new Point(15, 218);
                                lbSondaje3.Visible = true;
                                lbSondaje4.Location = new Point(592, 218);
                                lbSondaje4.Visible = true;
                            }
                        }
                    }
                }
            }
        }

        private void clbFiltros_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (!clbFiltros.GetItemChecked(e.Index))
            {
                dgvEstructura1.Columns[e.Index].Visible = true;
                dgvEstructura2.Columns[e.Index].Visible = true;
                dgvEstructura3.Columns[e.Index].Visible = true;
                dgvEstructura4.Columns[e.Index].Visible = true;
            }
            else
            {
                dgvEstructura1.Columns[e.Index].Visible = false;
                dgvEstructura2.Columns[e.Index].Visible = false;
                dgvEstructura3.Columns[e.Index].Visible = false;
                dgvEstructura4.Columns[e.Index].Visible = false;
            }
        }
    }
}
