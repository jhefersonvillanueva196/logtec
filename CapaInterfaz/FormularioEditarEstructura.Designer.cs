﻿
namespace CapaInterfaz
{
    partial class FormularioEditarEstructura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbValidar = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtFecha = new System.Windows.Forms.TextBox();
            this.cboJr = new System.Windows.Forms.ComboBox();
            this.cboJa = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.pbRelleno2 = new System.Windows.Forms.PictureBox();
            this.pbRelleno1 = new System.Windows.Forms.PictureBox();
            this.pbJa = new System.Windows.Forms.PictureBox();
            this.pbJr = new System.Windows.Forms.PictureBox();
            this.pbJrc = new System.Windows.Forms.PictureBox();
            this.pbCondicionFractura = new System.Windows.Forms.PictureBox();
            this.pbBeta = new System.Windows.Forms.PictureBox();
            this.pbAlpha = new System.Windows.Forms.PictureBox();
            this.cboRelleno2 = new System.Windows.Forms.ComboBox();
            this.cboRelleno1 = new System.Windows.Forms.ComboBox();
            this.cboJrc = new System.Windows.Forms.ComboBox();
            this.cboCondicionFractura = new System.Windows.Forms.ComboBox();
            this.cboTipoEstructura = new System.Windows.Forms.ComboBox();
            this.txtDesde = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtHasta = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtComentario = new System.Windows.Forms.TextBox();
            this.txtRefToTop = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.btnActualizar = new System.Windows.Forms.Button();
            this.txtEspesor = new System.Windows.Forms.TextBox();
            this.txtBeta = new System.Windows.Forms.TextBox();
            this.txtAlpha = new System.Windows.Forms.TextBox();
            this.txtTB = new System.Windows.Forms.TextBox();
            this.txtProfundidadEstructura = new System.Windows.Forms.TextBox();
            this.txtNumeroEstructura = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tips = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRelleno2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRelleno1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJrc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCondicionFractura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBeta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAlpha)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel1.Controls.Add(this.cbValidar);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.txtFecha);
            this.panel1.Controls.Add(this.cboJr);
            this.panel1.Controls.Add(this.cboJa);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.pbRelleno2);
            this.panel1.Controls.Add(this.pbRelleno1);
            this.panel1.Controls.Add(this.pbJa);
            this.panel1.Controls.Add(this.pbJr);
            this.panel1.Controls.Add(this.pbJrc);
            this.panel1.Controls.Add(this.pbCondicionFractura);
            this.panel1.Controls.Add(this.pbBeta);
            this.panel1.Controls.Add(this.pbAlpha);
            this.panel1.Controls.Add(this.cboRelleno2);
            this.panel1.Controls.Add(this.cboRelleno1);
            this.panel1.Controls.Add(this.cboJrc);
            this.panel1.Controls.Add(this.cboCondicionFractura);
            this.panel1.Controls.Add(this.cboTipoEstructura);
            this.panel1.Controls.Add(this.txtDesde);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.txtHasta);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtComentario);
            this.panel1.Controls.Add(this.txtRefToTop);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.btnCerrar);
            this.panel1.Controls.Add(this.btnActualizar);
            this.panel1.Controls.Add(this.txtEspesor);
            this.panel1.Controls.Add(this.txtBeta);
            this.panel1.Controls.Add(this.txtAlpha);
            this.panel1.Controls.Add(this.txtTB);
            this.panel1.Controls.Add(this.txtProfundidadEstructura);
            this.panel1.Controls.Add(this.txtNumeroEstructura);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(860, 501);
            this.panel1.TabIndex = 1;
            // 
            // cbValidar
            // 
            this.cbValidar.AutoSize = true;
            this.cbValidar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbValidar.Location = new System.Drawing.Point(710, 12);
            this.cbValidar.Name = "cbValidar";
            this.cbValidar.Size = new System.Drawing.Size(77, 24);
            this.cbValidar.TabIndex = 96;
            this.cbValidar.Text = "Validar";
            this.cbValidar.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label23.Location = new System.Drawing.Point(428, 202);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 13);
            this.label23.TabIndex = 95;
            this.label23.Text = "*";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label20.Location = new System.Drawing.Point(428, 156);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(11, 13);
            this.label20.TabIndex = 83;
            this.label20.Text = "*";
            // 
            // txtFecha
            // 
            this.txtFecha.Location = new System.Drawing.Point(77, 12);
            this.txtFecha.Name = "txtFecha";
            this.txtFecha.Size = new System.Drawing.Size(100, 20);
            this.txtFecha.TabIndex = 94;
            this.txtFecha.Visible = false;
            // 
            // cboJr
            // 
            this.cboJr.DropDownHeight = 90;
            this.cboJr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboJr.FormattingEnabled = true;
            this.cboJr.IntegralHeight = false;
            this.cboJr.Items.AddRange(new object[] {
            "0.5",
            "1",
            "1.5",
            "1.5",
            "2",
            "3",
            "4",
            "-1"});
            this.cboJr.Location = new System.Drawing.Point(661, 156);
            this.cboJr.Name = "cboJr";
            this.cboJr.Size = new System.Drawing.Size(126, 21);
            this.cboJr.TabIndex = 93;
            // 
            // cboJa
            // 
            this.cboJa.DropDownHeight = 90;
            this.cboJa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboJa.FormattingEnabled = true;
            this.cboJa.IntegralHeight = false;
            this.cboJa.Items.AddRange(new object[] {
            "0.75",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "-1"});
            this.cboJa.Location = new System.Drawing.Point(661, 204);
            this.cboJa.Name = "cboJa";
            this.cboJa.Size = new System.Drawing.Size(126, 21);
            this.cboJa.TabIndex = 92;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(428, 350);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(11, 13);
            this.label22.TabIndex = 91;
            this.label22.Text = "*";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(428, 302);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(11, 13);
            this.label21.TabIndex = 90;
            this.label21.Text = "*";
            // 
            // pbRelleno2
            // 
            this.pbRelleno2.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            this.pbRelleno2.Location = new System.Drawing.Point(793, 350);
            this.pbRelleno2.Name = "pbRelleno2";
            this.pbRelleno2.Size = new System.Drawing.Size(16, 16);
            this.pbRelleno2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbRelleno2.TabIndex = 89;
            this.pbRelleno2.TabStop = false;
            this.pbRelleno2.Click += new System.EventHandler(this.pbRelleno2_Click);
            // 
            // pbRelleno1
            // 
            this.pbRelleno1.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            this.pbRelleno1.Location = new System.Drawing.Point(793, 302);
            this.pbRelleno1.Name = "pbRelleno1";
            this.pbRelleno1.Size = new System.Drawing.Size(16, 16);
            this.pbRelleno1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbRelleno1.TabIndex = 88;
            this.pbRelleno1.TabStop = false;
            this.pbRelleno1.Click += new System.EventHandler(this.pbRelleno1_Click);
            // 
            // pbJa
            // 
            this.pbJa.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            this.pbJa.Location = new System.Drawing.Point(793, 206);
            this.pbJa.Name = "pbJa";
            this.pbJa.Size = new System.Drawing.Size(16, 16);
            this.pbJa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbJa.TabIndex = 87;
            this.pbJa.TabStop = false;
            this.pbJa.Click += new System.EventHandler(this.pbJa_Click);
            // 
            // pbJr
            // 
            this.pbJr.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            this.pbJr.Location = new System.Drawing.Point(793, 158);
            this.pbJr.Name = "pbJr";
            this.pbJr.Size = new System.Drawing.Size(16, 16);
            this.pbJr.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbJr.TabIndex = 86;
            this.pbJr.TabStop = false;
            this.pbJr.Click += new System.EventHandler(this.pbJr_Click);
            // 
            // pbJrc
            // 
            this.pbJrc.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            this.pbJrc.Location = new System.Drawing.Point(793, 110);
            this.pbJrc.Name = "pbJrc";
            this.pbJrc.Size = new System.Drawing.Size(16, 16);
            this.pbJrc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbJrc.TabIndex = 85;
            this.pbJrc.TabStop = false;
            this.pbJrc.Click += new System.EventHandler(this.pbJrc_Click);
            // 
            // pbCondicionFractura
            // 
            this.pbCondicionFractura.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            this.pbCondicionFractura.Location = new System.Drawing.Point(793, 60);
            this.pbCondicionFractura.Name = "pbCondicionFractura";
            this.pbCondicionFractura.Size = new System.Drawing.Size(16, 16);
            this.pbCondicionFractura.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbCondicionFractura.TabIndex = 84;
            this.pbCondicionFractura.TabStop = false;
            this.pbCondicionFractura.Click += new System.EventHandler(this.pbCondicionFractura_Click);
            // 
            // pbBeta
            // 
            this.pbBeta.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            this.pbBeta.Location = new System.Drawing.Point(442, 349);
            this.pbBeta.Name = "pbBeta";
            this.pbBeta.Size = new System.Drawing.Size(16, 16);
            this.pbBeta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbBeta.TabIndex = 83;
            this.pbBeta.TabStop = false;
            this.pbBeta.Click += new System.EventHandler(this.pbBeta_Click);
            // 
            // pbAlpha
            // 
            this.pbAlpha.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            this.pbAlpha.Location = new System.Drawing.Point(442, 301);
            this.pbAlpha.Name = "pbAlpha";
            this.pbAlpha.Size = new System.Drawing.Size(16, 16);
            this.pbAlpha.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbAlpha.TabIndex = 82;
            this.pbAlpha.TabStop = false;
            this.pbAlpha.Click += new System.EventHandler(this.pbAlpha_Click);
            // 
            // cboRelleno2
            // 
            this.cboRelleno2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRelleno2.FormattingEnabled = true;
            this.cboRelleno2.Items.AddRange(new object[] {
            "Ca",
            "Cl",
            "Kl",
            "De",
            "Y",
            "An",
            "Fe",
            "Pa",
            "Bf",
            "Ai",
            "Ar",
            "Ti",
            "Li",
            "Mi",
            "Qz",
            "Ox",
            "Evaporitas",
            "-1"});
            this.cboRelleno2.Location = new System.Drawing.Point(661, 348);
            this.cboRelleno2.Name = "cboRelleno2";
            this.cboRelleno2.Size = new System.Drawing.Size(126, 21);
            this.cboRelleno2.TabIndex = 78;
            // 
            // cboRelleno1
            // 
            this.cboRelleno1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRelleno1.DropDownWidth = 126;
            this.cboRelleno1.FormattingEnabled = true;
            this.cboRelleno1.IntegralHeight = false;
            this.cboRelleno1.Items.AddRange(new object[] {
            "Ca",
            "Cl",
            "Kl",
            "De",
            "Y",
            "An",
            "Fe",
            "Pa",
            "Bf",
            "Ai",
            "Ar",
            "Ti",
            "Li",
            "Mi",
            "Qz",
            "Ox",
            "Evaporitas",
            "-1"});
            this.cboRelleno1.Location = new System.Drawing.Point(661, 300);
            this.cboRelleno1.Name = "cboRelleno1";
            this.cboRelleno1.Size = new System.Drawing.Size(126, 21);
            this.cboRelleno1.TabIndex = 77;
            // 
            // cboJrc
            // 
            this.cboJrc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboJrc.FormattingEnabled = true;
            this.cboJrc.Items.AddRange(new object[] {
            "1",
            "3",
            "5",
            "7",
            "9",
            "11",
            "13",
            "15",
            "17",
            "19",
            "-1"});
            this.cboJrc.Location = new System.Drawing.Point(661, 108);
            this.cboJrc.Name = "cboJrc";
            this.cboJrc.Size = new System.Drawing.Size(126, 21);
            this.cboJrc.TabIndex = 76;
            // 
            // cboCondicionFractura
            // 
            this.cboCondicionFractura.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCondicionFractura.FormattingEnabled = true;
            this.cboCondicionFractura.Items.AddRange(new object[] {
            "24",
            "20",
            "16",
            "12",
            "9",
            "8",
            "7",
            "6",
            "5",
            "4",
            "3",
            "0",
            "25",
            "-1"});
            this.cboCondicionFractura.Location = new System.Drawing.Point(661, 60);
            this.cboCondicionFractura.Name = "cboCondicionFractura";
            this.cboCondicionFractura.Size = new System.Drawing.Size(126, 21);
            this.cboCondicionFractura.TabIndex = 75;
            // 
            // cboTipoEstructura
            // 
            this.cboTipoEstructura.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoEstructura.FormattingEnabled = true;
            this.cboTipoEstructura.Items.AddRange(new object[] {
            "Jn",
            "Vn",
            "Vnll",
            "F-10",
            "F+10",
            "Fl",
            "Sr",
            "Js",
            "Fo",
            "Bd",
            "Cn",
            "Dk",
            "Sc",
            "Uc",
            "Bh",
            "Se",
            "-1"});
            this.cboTipoEstructura.Location = new System.Drawing.Point(301, 204);
            this.cboTipoEstructura.Name = "cboTipoEstructura";
            this.cboTipoEstructura.Size = new System.Drawing.Size(126, 21);
            this.cboTipoEstructura.TabIndex = 74;
            this.cboTipoEstructura.SelectedValueChanged += new System.EventHandler(this.cboTipoEstructura_SelectedValueChanged);
            // 
            // txtDesde
            // 
            this.txtDesde.Enabled = false;
            this.txtDesde.Location = new System.Drawing.Point(234, 60);
            this.txtDesde.Name = "txtDesde";
            this.txtDesde.Size = new System.Drawing.Size(62, 20);
            this.txtDesde.TabIndex = 73;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(311, 63);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 72;
            this.label19.Text = "Hasta";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(178, 63);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(38, 13);
            this.label18.TabIndex = 71;
            this.label18.Text = "Desde";
            // 
            // txtHasta
            // 
            this.txtHasta.Enabled = false;
            this.txtHasta.Location = new System.Drawing.Point(365, 60);
            this.txtHasta.Name = "txtHasta";
            this.txtHasta.Size = new System.Drawing.Size(62, 20);
            this.txtHasta.TabIndex = 70;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(73, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 20);
            this.label1.TabIndex = 69;
            this.label1.Text = "Corrida";
            // 
            // txtComentario
            // 
            this.txtComentario.Location = new System.Drawing.Point(661, 396);
            this.txtComentario.Multiline = true;
            this.txtComentario.Name = "txtComentario";
            this.txtComentario.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtComentario.Size = new System.Drawing.Size(126, 30);
            this.txtComentario.TabIndex = 65;
            // 
            // txtRefToTop
            // 
            this.txtRefToTop.Location = new System.Drawing.Point(301, 396);
            this.txtRefToTop.Name = "txtRefToTop";
            this.txtRefToTop.Size = new System.Drawing.Size(126, 20);
            this.txtRefToTop.TabIndex = 64;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(488, 346);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 20);
            this.label17.TabIndex = 63;
            this.label17.Text = "Relleno 2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(77, 394);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 20);
            this.label16.TabIndex = 62;
            this.label16.Text = "Ref to top";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(488, 394);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 20);
            this.label15.TabIndex = 60;
            this.label15.Text = "Comentario";
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnCerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.ForeColor = System.Drawing.Color.White;
            this.btnCerrar.Location = new System.Drawing.Point(562, 453);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(102, 29);
            this.btnCerrar.TabIndex = 58;
            this.btnCerrar.Text = "Cancelar";
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // btnActualizar
            // 
            this.btnActualizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnActualizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualizar.ForeColor = System.Drawing.Color.White;
            this.btnActualizar.Location = new System.Drawing.Point(196, 453);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(102, 29);
            this.btnActualizar.TabIndex = 57;
            this.btnActualizar.Text = "Actualizar";
            this.btnActualizar.UseVisualStyleBackColor = false;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // txtEspesor
            // 
            this.txtEspesor.Location = new System.Drawing.Point(661, 252);
            this.txtEspesor.Name = "txtEspesor";
            this.txtEspesor.Size = new System.Drawing.Size(126, 20);
            this.txtEspesor.TabIndex = 55;
            this.txtEspesor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEspesor_KeyPress);
            this.txtEspesor.Leave += new System.EventHandler(this.txtEspesor_Leave);
            // 
            // txtBeta
            // 
            this.txtBeta.Location = new System.Drawing.Point(301, 348);
            this.txtBeta.Name = "txtBeta";
            this.txtBeta.Size = new System.Drawing.Size(126, 20);
            this.txtBeta.TabIndex = 50;
            this.txtBeta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBeta_KeyPress);
            this.txtBeta.Leave += new System.EventHandler(this.txtBeta_Leave);
            // 
            // txtAlpha
            // 
            this.txtAlpha.Location = new System.Drawing.Point(301, 300);
            this.txtAlpha.Name = "txtAlpha";
            this.txtAlpha.Size = new System.Drawing.Size(126, 20);
            this.txtAlpha.TabIndex = 49;
            this.txtAlpha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAlpha_KeyPress);
            this.txtAlpha.Leave += new System.EventHandler(this.txtAlpha_Leave);
            // 
            // txtTB
            // 
            this.txtTB.Location = new System.Drawing.Point(301, 252);
            this.txtTB.Name = "txtTB";
            this.txtTB.Size = new System.Drawing.Size(126, 20);
            this.txtTB.TabIndex = 48;
            // 
            // txtProfundidadEstructura
            // 
            this.txtProfundidadEstructura.Location = new System.Drawing.Point(301, 156);
            this.txtProfundidadEstructura.Name = "txtProfundidadEstructura";
            this.txtProfundidadEstructura.Size = new System.Drawing.Size(126, 20);
            this.txtProfundidadEstructura.TabIndex = 46;
            this.txtProfundidadEstructura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtProfundidadEstructura_KeyPress);
            this.txtProfundidadEstructura.Leave += new System.EventHandler(this.txtProfundidadEstructura_Leave);
            // 
            // txtNumeroEstructura
            // 
            this.txtNumeroEstructura.Enabled = false;
            this.txtNumeroEstructura.Location = new System.Drawing.Point(301, 108);
            this.txtNumeroEstructura.Name = "txtNumeroEstructura";
            this.txtNumeroEstructura.Size = new System.Drawing.Size(126, 20);
            this.txtNumeroEstructura.TabIndex = 45;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(358, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(145, 24);
            this.label14.TabIndex = 43;
            this.label14.Text = "Editar estructura";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(488, 298);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 20);
            this.label13.TabIndex = 42;
            this.label13.Text = "Relleno 1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(488, 250);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 20);
            this.label12.TabIndex = 41;
            this.label12.Text = "Espesor (mm)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(488, 154);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 20);
            this.label11.TabIndex = 40;
            this.label11.Text = "Jr (Q de Barton)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(488, 202);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 20);
            this.label10.TabIndex = 39;
            this.label10.Text = "Ja (Q de Barton)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(488, 106);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 20);
            this.label9.TabIndex = 38;
            this.label9.Text = "Jrc";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(488, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(160, 20);
            this.label8.TabIndex = 37;
            this.label8.Text = "Condición de fractura";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(73, 346);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 20);
            this.label7.TabIndex = 36;
            this.label7.Text = "Beta";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(73, 298);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 20);
            this.label6.TabIndex = 35;
            this.label6.Text = "Alpha";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(73, 250);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 20);
            this.label5.TabIndex = 34;
            this.label5.Text = "T/B";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(73, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 20);
            this.label4.TabIndex = 33;
            this.label4.Text = "Tipo de estructura";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(73, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(220, 20);
            this.label3.TabIndex = 32;
            this.label3.Text = "Profundidad de estructura (m)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(73, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(163, 20);
            this.label2.TabIndex = 31;
            this.label2.Text = "Número de estructura";
            // 
            // FormularioEditarEstructura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 501);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormularioEditarEstructura";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Formulario Editar Estructura";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRelleno2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRelleno1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJrc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCondicionFractura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBeta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAlpha)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Button btnActualizar;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDesde;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtHasta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbRelleno2;
        private System.Windows.Forms.PictureBox pbRelleno1;
        private System.Windows.Forms.PictureBox pbJa;
        private System.Windows.Forms.PictureBox pbJr;
        private System.Windows.Forms.PictureBox pbJrc;
        private System.Windows.Forms.PictureBox pbCondicionFractura;
        private System.Windows.Forms.PictureBox pbBeta;
        private System.Windows.Forms.PictureBox pbAlpha;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox txtComentario;
        public System.Windows.Forms.TextBox txtRefToTop;
        public System.Windows.Forms.TextBox txtEspesor;
        public System.Windows.Forms.TextBox txtBeta;
        public System.Windows.Forms.TextBox txtAlpha;
        public System.Windows.Forms.TextBox txtTB;
        public System.Windows.Forms.TextBox txtProfundidadEstructura;
        public System.Windows.Forms.ComboBox cboTipoEstructura;
        public System.Windows.Forms.ComboBox cboRelleno2;
        public System.Windows.Forms.ComboBox cboRelleno1;
        public System.Windows.Forms.ComboBox cboJrc;
        public System.Windows.Forms.ComboBox cboCondicionFractura;
        public System.Windows.Forms.TextBox txtNumeroEstructura;
        public System.Windows.Forms.ComboBox cboJr;
        public System.Windows.Forms.ComboBox cboJa;
        private System.Windows.Forms.ToolTip tips;
        public System.Windows.Forms.TextBox txtFecha;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        public System.Windows.Forms.CheckBox cbValidar;
    }
}