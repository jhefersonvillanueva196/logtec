﻿using CapaEntidad;
using CapaLogica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaInterfaz
{
    public partial class PantallaMain : Form
    {
        CargaSinc carga;

        public PantallaMain()
        {
            InitializeComponent();
            LogDataBase.Instancia.InicializarBD();
            btnLogueo.Visible = false;
            btnReportes.Visible = false;
            btnEstereografia.Visible = false;
            btnEstadisticas.Visible = false;
            btnNosotros.Visible = false;
            btnSobreAplicacion.Visible = false;
            labelPerfil.Visible = false;
            labelSalir.Visible = false;
            btnSincLocal.Visible = false;
            btnSincRemota.Visible = false;
            if (Test())
            {
                btnSincLocal.Enabled = true;
                btnSincRemota.Enabled = true;
            }
        }

        public bool Test()
        {
            System.Uri Url = new System.Uri("http://www.google.com/");
            System.Net.WebRequest WebRequest;
            WebRequest = System.Net.WebRequest.Create(Url);
            System.Net.WebResponse objResp;
            try
            {
                objResp = WebRequest.GetResponse();
                
                objResp.Close();
                WebRequest = null;
                return true;
            }
            catch (Exception)
            {
                
                WebRequest = null;
                return false;
            }
        }

        private void txtUsuario_Enter(object sender, EventArgs e)
        {
            txtUsuario.Text = "";
            txtUsuario.TextAlign = HorizontalAlignment.Left;
        }

        private void txtUsuario_Leave(object sender, EventArgs e)
        {
            if (txtUsuario.Text == "")
            {
                txtUsuario.Text = "Usuario";
                txtUsuario.TextAlign = HorizontalAlignment.Center;
            }
        }

        private void txtContrasenia_Enter(object sender, EventArgs e)
        {
            txtContrasenia.Text = "";
            txtContrasenia.TextAlign = HorizontalAlignment.Left;
            txtContrasenia.UseSystemPasswordChar = true;
        }

        private void txtContrasenia_Leave(object sender, EventArgs e)
        {
            if(txtContrasenia.Text == "")
            {
                txtContrasenia.UseSystemPasswordChar = false;
                txtContrasenia.Text = "Contraseña";
                txtContrasenia.TextAlign = HorizontalAlignment.Center;
            }
        }

        private void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            string dni = txtUsuario.Text.Trim();
            string contrasenia = txtContrasenia.Text.Trim();
            //btnLogueo.Visible = true;
            //btnReportes.Visible = true;
            //btnEstereografia.Visible = true;
            //btnEstadisticas.Visible = true;
            //btnNosotros.Visible = true;
            //btnSobreAplicacion.Visible = true;
            //labelPerfil.Visible = true;
            //labelSalir.Visible = true;
            //btnIniciarSesion.Visible = false;
            //btnCancelar.Visible = false;
            //txtUsuario.Visible = false;
            //txtContrasenia.Visible = false;
            if (txtUsuario.Text.Trim() == "")
            {
                MessageBox.Show("Ingrese su usuario");
                txtUsuario.Focus();
            }
            else
            {
                if (txtContrasenia.Text.Trim() == "")
                {
                    MessageBox.Show("Ingrese su contraseña");
                    txtContrasenia.Focus();
                }
                else
                {
                    if (LogUsuario.Instancia.Verifica(dni, contrasenia) == true)
                    {
                        btnLogueo.Visible = true;
                        btnReportes.Visible = true;
                        btnEstereografia.Visible = true;
                        btnEstadisticas.Visible = true;
                        btnNosotros.Visible = true;
                        btnSobreAplicacion.Visible = true;
                        labelPerfil.Visible = true;
                        labelSalir.Visible = true;
                        btnSincLocal.Visible = true;
                        btnSincRemota.Visible = true;
                        btnIniciarSesion.Visible = false;
                        btnCancelar.Visible = false;
                        txtUsuario.Visible = false;
                        txtContrasenia.Visible = false;
                    }
                    else
                    {
                        MessageBox.Show("Usuario o constraña incorrecta verifique sus datos");
                        txtUsuario.Focus();
                    }
                }
            }
            
        }

        private void labelSalir_Click(object sender, EventArgs e)
        {

            btnLogueo.Visible = false;
            btnReportes.Visible = false;
            btnEstereografia.Visible = false;
            btnEstadisticas.Visible = false;
            btnNosotros.Visible = false;
            btnSobreAplicacion.Visible = false;
            labelPerfil.Visible = false;
            labelSalir.Visible = false;
            btnSincLocal.Visible = false;
            btnSincRemota.Visible = false;
            btnIniciarSesion.Visible = true;
            btnCancelar.Visible = true;
            txtUsuario.Visible = true;
            txtContrasenia.Visible = true;
            txtUsuario.Text = "";
            txtContrasenia.Text = "";
            if (activeForm != null)
            {
                activeForm.Close();
            }
            
        }

        private Form activeForm = null;

        private void openChildForm(Form childForm)
        {
            if (activeForm != null)
                activeForm.Close();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.Dock = DockStyle.Fill;
            childForm.FormBorderStyle = FormBorderStyle.None;
            panelFormularios.Controls.Add(childForm);
            panelFormularios.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }

        private void labelPerfil_Click(object sender, EventArgs e)
        {
            openChildForm(new VistaPerfil());
        }

        private void btnLogueo_Click(object sender, EventArgs e)
        {
            openChildForm(new VistaLogueo());
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            openChildForm(new VistaReportes());
        }

        private void btnEstereografia_Click(object sender, EventArgs e)
        {
            openChildForm(new VistaEstereografia());
        }

        private void btnEstadisticas_Click(object sender, EventArgs e)
        {
            openChildForm(new VistaEstadisticas());
        }

        private void btnNosotros_Click(object sender, EventArgs e)
        {
            openChildForm(new VistaNosotros());
        }

        private void btnSobreAplicacion_Click(object sender, EventArgs e)
        {
            openChildForm(new VistaSobreAplicacion());
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                string dni = txtUsuario.Text.Trim();
                string contrasenia = txtContrasenia.Text.Trim();
                //btnLogueo.Visible = true;
                //btnReportes.Visible = true;
                //btnEstereografia.Visible = true;
                //btnEstadisticas.Visible = true;
                //btnNosotros.Visible = true;
                //btnSobreAplicacion.Visible = true;
                //labelPerfil.Visible = true;
                //labelSalir.Visible = true;
                //btnIniciarSesion.Visible = false;
                //btnCancelar.Visible = false;
                //txtUsuario.Visible = false;
                //txtContrasenia.Visible = false;
                if (txtUsuario.Text.Trim() == "")
                {
                    MessageBox.Show("Ingrese su usuario");
                    txtUsuario.Focus();
                }
                else
                {
                    if (txtContrasenia.Text.Trim() == "")
                    {
                        MessageBox.Show("Ingrese su contraseña");
                        txtContrasenia.Focus();
                    }
                    else
                    {
                        if (LogUsuario.Instancia.Verifica(dni, contrasenia) == true)
                        {
                            btnLogueo.Visible = true;
                            btnReportes.Visible = true;
                            btnEstereografia.Visible = true;
                            btnEstadisticas.Visible = true;
                            btnNosotros.Visible = true;
                            btnSobreAplicacion.Visible = true;
                            labelPerfil.Visible = true;
                            labelSalir.Visible = true;
                            btnSincLocal.Visible = true;
                            btnSincRemota.Visible = true;
                            btnIniciarSesion.Visible = false;
                            btnCancelar.Visible = false;
                            txtUsuario.Visible = false;
                            txtContrasenia.Visible = false;
                        }
                        else
                        {
                            MessageBox.Show("Usuario o constraña incorrecta verifique sus datos");
                            txtUsuario.Focus();
                        }
                    }
                }
            }
        }

        private void txtContrasenia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                string dni = txtUsuario.Text.Trim();
                string contrasenia = txtContrasenia.Text.Trim();
                //btnLogueo.Visible = true;
                //btnReportes.Visible = true;
                //btnEstereografia.Visible = true;
                //btnEstadisticas.Visible = true;
                //btnNosotros.Visible = true;
                //btnSobreAplicacion.Visible = true;
                //labelPerfil.Visible = true;
                //labelSalir.Visible = true;
                //btnIniciarSesion.Visible = false;
                //btnCancelar.Visible = false;
                //txtUsuario.Visible = false;
                //txtContrasenia.Visible = false;
                if (txtUsuario.Text.Trim() == "")
                {
                    MessageBox.Show("Ingrese su usuario");
                    txtUsuario.Focus();
                }
                else
                {
                    if (txtContrasenia.Text.Trim() == "")
                    {
                        MessageBox.Show("Ingrese su contraseña");
                        txtContrasenia.Focus();
                    }
                    else
                    {
                        if (LogUsuario.Instancia.Verifica(dni, contrasenia) == true)
                        {
                            btnLogueo.Visible = true;
                            btnReportes.Visible = true;
                            btnEstereografia.Visible = true;
                            btnEstadisticas.Visible = true;
                            btnNosotros.Visible = true;
                            btnSobreAplicacion.Visible = true;
                            labelPerfil.Visible = true;
                            labelSalir.Visible = true;
                            btnSincLocal.Visible = true;
                            btnSincRemota.Visible = true;
                            btnIniciarSesion.Visible = false;
                            btnCancelar.Visible = false;
                            txtUsuario.Visible = false;
                            txtContrasenia.Visible = false;
                        }
                        else
                        {
                            MessageBox.Show("Usuario o constraña incorrecta verifique sus datos");
                            txtUsuario.Focus();
                        }
                    }
                }
            }
        }

        private async void btnSincLocal_Click(object sender, EventArgs e)
        {
            Abrir();
            Task ltask = new Task(SincroLocal);
            ltask.Start();
            await ltask;
            Cerrar();
            openChildForm(new VistaLogueo());
        }

        private void SincroLocal()
        {
            LogSincronizacion.Instancia.Sinc_Local();
        }

        private async void btnSincRemota_Click(object sender, EventArgs e)
        {
            Abrir();
            Task rtask = new Task(SincroRemota);
            rtask.Start();
            await rtask;
            Cerrar();
            openChildForm(new VistaLogueo());
        }

        private void SincroRemota()
        {
            LogSincronizacion.Instancia.Sinc_Remota();
        }

        public void Abrir()
        {
            carga = new CargaSinc();
            carga.Show();
        }

        private void Cerrar()
        {
            if (carga != null)
                carga.Close();
        }
    }
}
