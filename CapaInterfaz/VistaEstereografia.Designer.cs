﻿
namespace CapaInterfaz
{
    partial class VistaEstereografia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.clbSondajes = new System.Windows.Forms.CheckedListBox();
            this.clbTipoEstrcuturas = new System.Windows.Forms.CheckedListBox();
            this.panelLimites = new System.Windows.Forms.Panel();
            this.dgvHasta = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idUserDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idSondajeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.desdeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hastaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perforacionDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recuperacionDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.entCorridaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dgvDesde = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idUserDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idSondajeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.desdeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hastaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perforacionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recuperacionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbTramo = new System.Windows.Forms.CheckBox();
            this.cbTipoEstructura = new System.Windows.Forms.CheckBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.entSondajeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panelData = new System.Windows.Forms.Panel();
            this.cbFiltros = new System.Windows.Forms.CheckBox();
            this.clbFiltros = new System.Windows.Forms.CheckedListBox();
            this.lbSondaje4 = new System.Windows.Forms.Label();
            this.lbSondaje3 = new System.Windows.Forms.Label();
            this.lbSondaje2 = new System.Windows.Forms.Label();
            this.dgvEstructura4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn65 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn66 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn67 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn68 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn69 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn70 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn71 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn72 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn73 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn74 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn75 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.entEstructuraBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dgvEstructura2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvEstructura3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbSondaje1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvEstructura1 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idSondajeDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idCorridaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idUsuarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numestructuraDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.profundidadeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tBDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alphaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.betaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reftotopDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.condfracturaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jrcDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.espesorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.relleno1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.relleno2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comentarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.geotecnicoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaEsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dipcalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dipdircalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dipsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.strikeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnExportar = new System.Windows.Forms.Button();
            this.cbTipoDiagrama = new System.Windows.Forms.ComboBox();
            this.btnGenerar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panelLimites.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entCorridaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entSondajeBindingSource)).BeginInit();
            this.panelData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstructura4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entEstructuraBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstructura2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstructura3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstructura1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(31)))), ((int)(((byte)(67)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1176, 33);
            this.panel1.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::CapaInterfaz.Properties.Resources.cerrar;
            this.pictureBox1.Location = new System.Drawing.Point(1148, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(536, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "ESTEREOGRAFÍA";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.clbSondajes);
            this.panel2.Controls.Add(this.clbTipoEstrcuturas);
            this.panel2.Controls.Add(this.panelLimites);
            this.panel2.Controls.Add(this.cbTramo);
            this.panel2.Controls.Add(this.cbTipoEstructura);
            this.panel2.Controls.Add(this.btnBuscar);
            this.panel2.Location = new System.Drawing.Point(9, 39);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1158, 208);
            this.panel2.TabIndex = 3;
            // 
            // clbSondajes
            // 
            this.clbSondajes.CheckOnClick = true;
            this.clbSondajes.FormattingEnabled = true;
            this.clbSondajes.Location = new System.Drawing.Point(76, 16);
            this.clbSondajes.Name = "clbSondajes";
            this.clbSondajes.Size = new System.Drawing.Size(155, 124);
            this.clbSondajes.TabIndex = 13;
            this.clbSondajes.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbSondajes_ItemCheck);
            this.clbSondajes.SelectedValueChanged += new System.EventHandler(this.clbSondajes_SelectedValueChanged);
            // 
            // clbTipoEstrcuturas
            // 
            this.clbTipoEstrcuturas.CheckOnClick = true;
            this.clbTipoEstrcuturas.Enabled = false;
            this.clbTipoEstrcuturas.FormattingEnabled = true;
            this.clbTipoEstrcuturas.Items.AddRange(new object[] {
            "Jn",
            "Vn",
            "Vnll",
            "F-10",
            "F+10",
            "Fl",
            "Sr",
            "Js",
            "Fo",
            "Bd",
            "Cn",
            "Dk",
            "Sc",
            "Uc",
            "Bh",
            "Se",
            "-1"});
            this.clbTipoEstrcuturas.Location = new System.Drawing.Point(851, 16);
            this.clbTipoEstrcuturas.Name = "clbTipoEstrcuturas";
            this.clbTipoEstrcuturas.Size = new System.Drawing.Size(155, 124);
            this.clbTipoEstrcuturas.TabIndex = 12;
            // 
            // panelLimites
            // 
            this.panelLimites.Controls.Add(this.dgvHasta);
            this.panelLimites.Controls.Add(this.dgvDesde);
            this.panelLimites.Enabled = false;
            this.panelLimites.Location = new System.Drawing.Point(420, 16);
            this.panelLimites.Name = "panelLimites";
            this.panelLimites.Size = new System.Drawing.Size(315, 131);
            this.panelLimites.TabIndex = 4;
            // 
            // dgvHasta
            // 
            this.dgvHasta.AllowUserToAddRows = false;
            this.dgvHasta.AllowUserToDeleteRows = false;
            this.dgvHasta.AllowUserToResizeColumns = false;
            this.dgvHasta.AllowUserToResizeRows = false;
            this.dgvHasta.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvHasta.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvHasta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvHasta.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn2,
            this.idUserDataGridViewTextBoxColumn2,
            this.idSondajeDataGridViewTextBoxColumn1,
            this.desdeDataGridViewTextBoxColumn1,
            this.hastaDataGridViewTextBoxColumn1,
            this.perforacionDataGridViewTextBoxColumn1,
            this.recuperacionDataGridViewTextBoxColumn1});
            this.dgvHasta.DataSource = this.entCorridaBindingSource;
            this.dgvHasta.Location = new System.Drawing.Point(173, 6);
            this.dgvHasta.Name = "dgvHasta";
            this.dgvHasta.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvHasta.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvHasta.RowHeadersVisible = false;
            this.dgvHasta.Size = new System.Drawing.Size(138, 119);
            this.dgvHasta.TabIndex = 15;
            // 
            // idDataGridViewTextBoxColumn2
            // 
            this.idDataGridViewTextBoxColumn2.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn2.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn2.Name = "idDataGridViewTextBoxColumn2";
            this.idDataGridViewTextBoxColumn2.ReadOnly = true;
            this.idDataGridViewTextBoxColumn2.Visible = false;
            // 
            // idUserDataGridViewTextBoxColumn2
            // 
            this.idUserDataGridViewTextBoxColumn2.DataPropertyName = "IdUser";
            this.idUserDataGridViewTextBoxColumn2.HeaderText = "IdUser";
            this.idUserDataGridViewTextBoxColumn2.Name = "idUserDataGridViewTextBoxColumn2";
            this.idUserDataGridViewTextBoxColumn2.ReadOnly = true;
            this.idUserDataGridViewTextBoxColumn2.Visible = false;
            // 
            // idSondajeDataGridViewTextBoxColumn1
            // 
            this.idSondajeDataGridViewTextBoxColumn1.DataPropertyName = "Id_Sondaje";
            this.idSondajeDataGridViewTextBoxColumn1.HeaderText = "Id_Sondaje";
            this.idSondajeDataGridViewTextBoxColumn1.Name = "idSondajeDataGridViewTextBoxColumn1";
            this.idSondajeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.idSondajeDataGridViewTextBoxColumn1.Visible = false;
            // 
            // desdeDataGridViewTextBoxColumn1
            // 
            this.desdeDataGridViewTextBoxColumn1.DataPropertyName = "Desde";
            this.desdeDataGridViewTextBoxColumn1.HeaderText = "Desde";
            this.desdeDataGridViewTextBoxColumn1.Name = "desdeDataGridViewTextBoxColumn1";
            this.desdeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.desdeDataGridViewTextBoxColumn1.Visible = false;
            // 
            // hastaDataGridViewTextBoxColumn1
            // 
            this.hastaDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.hastaDataGridViewTextBoxColumn1.DataPropertyName = "Hasta";
            this.hastaDataGridViewTextBoxColumn1.HeaderText = "Hasta";
            this.hastaDataGridViewTextBoxColumn1.Name = "hastaDataGridViewTextBoxColumn1";
            this.hastaDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // perforacionDataGridViewTextBoxColumn1
            // 
            this.perforacionDataGridViewTextBoxColumn1.DataPropertyName = "Perforacion";
            this.perforacionDataGridViewTextBoxColumn1.HeaderText = "Perforacion";
            this.perforacionDataGridViewTextBoxColumn1.Name = "perforacionDataGridViewTextBoxColumn1";
            this.perforacionDataGridViewTextBoxColumn1.ReadOnly = true;
            this.perforacionDataGridViewTextBoxColumn1.Visible = false;
            // 
            // recuperacionDataGridViewTextBoxColumn1
            // 
            this.recuperacionDataGridViewTextBoxColumn1.DataPropertyName = "Recuperacion";
            this.recuperacionDataGridViewTextBoxColumn1.HeaderText = "Recuperacion";
            this.recuperacionDataGridViewTextBoxColumn1.Name = "recuperacionDataGridViewTextBoxColumn1";
            this.recuperacionDataGridViewTextBoxColumn1.ReadOnly = true;
            this.recuperacionDataGridViewTextBoxColumn1.Visible = false;
            // 
            // entCorridaBindingSource
            // 
            this.entCorridaBindingSource.DataSource = typeof(CapaEntidad.EntCorrida);
            // 
            // dgvDesde
            // 
            this.dgvDesde.AllowUserToAddRows = false;
            this.dgvDesde.AllowUserToDeleteRows = false;
            this.dgvDesde.AllowUserToResizeColumns = false;
            this.dgvDesde.AllowUserToResizeRows = false;
            this.dgvDesde.AutoGenerateColumns = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDesde.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvDesde.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvDesde.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.idUserDataGridViewTextBoxColumn1,
            this.idSondajeDataGridViewTextBoxColumn,
            this.desdeDataGridViewTextBoxColumn,
            this.hastaDataGridViewTextBoxColumn,
            this.perforacionDataGridViewTextBoxColumn,
            this.recuperacionDataGridViewTextBoxColumn});
            this.dgvDesde.DataSource = this.entCorridaBindingSource;
            this.dgvDesde.Location = new System.Drawing.Point(3, 8);
            this.dgvDesde.Name = "dgvDesde";
            this.dgvDesde.ReadOnly = true;
            this.dgvDesde.RowHeadersVisible = false;
            this.dgvDesde.Size = new System.Drawing.Size(138, 117);
            this.dgvDesde.TabIndex = 14;
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            this.idDataGridViewTextBoxColumn1.Visible = false;
            // 
            // idUserDataGridViewTextBoxColumn1
            // 
            this.idUserDataGridViewTextBoxColumn1.DataPropertyName = "IdUser";
            this.idUserDataGridViewTextBoxColumn1.HeaderText = "IdUser";
            this.idUserDataGridViewTextBoxColumn1.Name = "idUserDataGridViewTextBoxColumn1";
            this.idUserDataGridViewTextBoxColumn1.ReadOnly = true;
            this.idUserDataGridViewTextBoxColumn1.Visible = false;
            // 
            // idSondajeDataGridViewTextBoxColumn
            // 
            this.idSondajeDataGridViewTextBoxColumn.DataPropertyName = "Id_Sondaje";
            this.idSondajeDataGridViewTextBoxColumn.HeaderText = "Id_Sondaje";
            this.idSondajeDataGridViewTextBoxColumn.Name = "idSondajeDataGridViewTextBoxColumn";
            this.idSondajeDataGridViewTextBoxColumn.ReadOnly = true;
            this.idSondajeDataGridViewTextBoxColumn.Visible = false;
            // 
            // desdeDataGridViewTextBoxColumn
            // 
            this.desdeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.desdeDataGridViewTextBoxColumn.DataPropertyName = "Desde";
            this.desdeDataGridViewTextBoxColumn.HeaderText = "Desde";
            this.desdeDataGridViewTextBoxColumn.Name = "desdeDataGridViewTextBoxColumn";
            this.desdeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // hastaDataGridViewTextBoxColumn
            // 
            this.hastaDataGridViewTextBoxColumn.DataPropertyName = "Hasta";
            this.hastaDataGridViewTextBoxColumn.HeaderText = "Hasta";
            this.hastaDataGridViewTextBoxColumn.Name = "hastaDataGridViewTextBoxColumn";
            this.hastaDataGridViewTextBoxColumn.ReadOnly = true;
            this.hastaDataGridViewTextBoxColumn.Visible = false;
            // 
            // perforacionDataGridViewTextBoxColumn
            // 
            this.perforacionDataGridViewTextBoxColumn.DataPropertyName = "Perforacion";
            this.perforacionDataGridViewTextBoxColumn.HeaderText = "Perforacion";
            this.perforacionDataGridViewTextBoxColumn.Name = "perforacionDataGridViewTextBoxColumn";
            this.perforacionDataGridViewTextBoxColumn.ReadOnly = true;
            this.perforacionDataGridViewTextBoxColumn.Visible = false;
            // 
            // recuperacionDataGridViewTextBoxColumn
            // 
            this.recuperacionDataGridViewTextBoxColumn.DataPropertyName = "Recuperacion";
            this.recuperacionDataGridViewTextBoxColumn.HeaderText = "Recuperacion";
            this.recuperacionDataGridViewTextBoxColumn.Name = "recuperacionDataGridViewTextBoxColumn";
            this.recuperacionDataGridViewTextBoxColumn.ReadOnly = true;
            this.recuperacionDataGridViewTextBoxColumn.Visible = false;
            // 
            // cbTramo
            // 
            this.cbTramo.AutoSize = true;
            this.cbTramo.Location = new System.Drawing.Point(741, 16);
            this.cbTramo.Name = "cbTramo";
            this.cbTramo.Size = new System.Drawing.Size(56, 17);
            this.cbTramo.TabIndex = 10;
            this.cbTramo.Text = "Tramo";
            this.cbTramo.UseVisualStyleBackColor = true;
            this.cbTramo.CheckedChanged += new System.EventHandler(this.cbTramo_CheckedChanged);
            // 
            // cbTipoEstructura
            // 
            this.cbTipoEstructura.AutoSize = true;
            this.cbTipoEstructura.Location = new System.Drawing.Point(1026, 16);
            this.cbTipoEstructura.Name = "cbTipoEstructura";
            this.cbTipoEstructura.Size = new System.Drawing.Size(112, 17);
            this.cbTipoEstructura.TabIndex = 9;
            this.cbTipoEstructura.Text = "Tipo de estructura";
            this.cbTipoEstructura.UseVisualStyleBackColor = true;
            this.cbTipoEstructura.CheckedChanged += new System.EventHandler(this.cbTipoEstructura_CheckedChanged);
            // 
            // btnBuscar
            // 
            this.btnBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Location = new System.Drawing.Point(536, 160);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(85, 33);
            this.btnBuscar.TabIndex = 3;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = false;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // entSondajeBindingSource
            // 
            this.entSondajeBindingSource.DataSource = typeof(CapaEntidad.EntSondaje);
            // 
            // panelData
            // 
            this.panelData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelData.Controls.Add(this.cbFiltros);
            this.panelData.Controls.Add(this.clbFiltros);
            this.panelData.Controls.Add(this.lbSondaje4);
            this.panelData.Controls.Add(this.lbSondaje3);
            this.panelData.Controls.Add(this.lbSondaje2);
            this.panelData.Controls.Add(this.dgvEstructura4);
            this.panelData.Controls.Add(this.dgvEstructura2);
            this.panelData.Controls.Add(this.dgvEstructura3);
            this.panelData.Controls.Add(this.lbSondaje1);
            this.panelData.Controls.Add(this.label1);
            this.panelData.Controls.Add(this.dgvEstructura1);
            this.panelData.Location = new System.Drawing.Point(9, 253);
            this.panelData.Name = "panelData";
            this.panelData.Size = new System.Drawing.Size(1158, 390);
            this.panelData.TabIndex = 4;
            this.panelData.Visible = false;
            // 
            // cbFiltros
            // 
            this.cbFiltros.AutoSize = true;
            this.cbFiltros.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFiltros.Location = new System.Drawing.Point(1070, 11);
            this.cbFiltros.Name = "cbFiltros";
            this.cbFiltros.Size = new System.Drawing.Size(68, 22);
            this.cbFiltros.TabIndex = 10;
            this.cbFiltros.Text = "Filtros";
            this.cbFiltros.UseVisualStyleBackColor = true;
            this.cbFiltros.CheckedChanged += new System.EventHandler(this.cbFiltros_CheckedChanged);
            // 
            // clbFiltros
            // 
            this.clbFiltros.CheckOnClick = true;
            this.clbFiltros.FormattingEnabled = true;
            this.clbFiltros.Location = new System.Drawing.Point(1014, 66);
            this.clbFiltros.Name = "clbFiltros";
            this.clbFiltros.Size = new System.Drawing.Size(135, 304);
            this.clbFiltros.TabIndex = 9;
            this.clbFiltros.Visible = false;
            this.clbFiltros.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbFiltros_ItemCheck);
            // 
            // lbSondaje4
            // 
            this.lbSondaje4.AutoSize = true;
            this.lbSondaje4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSondaje4.Location = new System.Drawing.Point(521, 218);
            this.lbSondaje4.Name = "lbSondaje4";
            this.lbSondaje4.Size = new System.Drawing.Size(68, 20);
            this.lbSondaje4.TabIndex = 8;
            this.lbSondaje4.Text = "Sondaje";
            this.lbSondaje4.Visible = false;
            // 
            // lbSondaje3
            // 
            this.lbSondaje3.AutoSize = true;
            this.lbSondaje3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSondaje3.Location = new System.Drawing.Point(15, 218);
            this.lbSondaje3.Name = "lbSondaje3";
            this.lbSondaje3.Size = new System.Drawing.Size(68, 20);
            this.lbSondaje3.TabIndex = 7;
            this.lbSondaje3.Text = "Sondaje";
            this.lbSondaje3.Visible = false;
            // 
            // lbSondaje2
            // 
            this.lbSondaje2.AutoSize = true;
            this.lbSondaje2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSondaje2.Location = new System.Drawing.Point(521, 40);
            this.lbSondaje2.Name = "lbSondaje2";
            this.lbSondaje2.Size = new System.Drawing.Size(68, 20);
            this.lbSondaje2.TabIndex = 6;
            this.lbSondaje2.Text = "Sondaje";
            this.lbSondaje2.Visible = false;
            // 
            // dgvEstructura4
            // 
            this.dgvEstructura4.AllowUserToAddRows = false;
            this.dgvEstructura4.AllowUserToDeleteRows = false;
            this.dgvEstructura4.AllowUserToResizeColumns = false;
            this.dgvEstructura4.AllowUserToResizeRows = false;
            this.dgvEstructura4.AutoGenerateColumns = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEstructura4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvEstructura4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvEstructura4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn53,
            this.dataGridViewTextBoxColumn54,
            this.dataGridViewTextBoxColumn55,
            this.dataGridViewTextBoxColumn56,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewTextBoxColumn59,
            this.dataGridViewTextBoxColumn60,
            this.dataGridViewTextBoxColumn61,
            this.dataGridViewTextBoxColumn62,
            this.dataGridViewTextBoxColumn63,
            this.dataGridViewTextBoxColumn64,
            this.dataGridViewTextBoxColumn65,
            this.dataGridViewTextBoxColumn66,
            this.dataGridViewTextBoxColumn67,
            this.dataGridViewTextBoxColumn68,
            this.dataGridViewTextBoxColumn69,
            this.dataGridViewTextBoxColumn70,
            this.dataGridViewTextBoxColumn71,
            this.dataGridViewTextBoxColumn72,
            this.dataGridViewTextBoxColumn73,
            this.dataGridViewTextBoxColumn74,
            this.dataGridViewTextBoxColumn75});
            this.dgvEstructura4.DataSource = this.entEstructuraBindingSource;
            this.dgvEstructura4.Location = new System.Drawing.Point(525, 241);
            this.dgvEstructura4.Name = "dgvEstructura4";
            this.dgvEstructura4.ReadOnly = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEstructura4.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvEstructura4.RowHeadersVisible = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgvEstructura4.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvEstructura4.Size = new System.Drawing.Size(477, 136);
            this.dgvEstructura4.TabIndex = 5;
            this.dgvEstructura4.Visible = false;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn51.HeaderText = "Id";
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.ReadOnly = true;
            this.dataGridViewTextBoxColumn51.Visible = false;
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.DataPropertyName = "Id_Sondaje";
            this.dataGridViewTextBoxColumn52.HeaderText = "Id_Sondaje";
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.ReadOnly = true;
            this.dataGridViewTextBoxColumn52.Visible = false;
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.DataPropertyName = "Id_Corrida";
            this.dataGridViewTextBoxColumn53.HeaderText = "Id_Corrida";
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.ReadOnly = true;
            this.dataGridViewTextBoxColumn53.Visible = false;
            // 
            // dataGridViewTextBoxColumn54
            // 
            this.dataGridViewTextBoxColumn54.DataPropertyName = "Id_Usuario";
            this.dataGridViewTextBoxColumn54.HeaderText = "Id_Usuario";
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            this.dataGridViewTextBoxColumn54.ReadOnly = true;
            this.dataGridViewTextBoxColumn54.Visible = false;
            // 
            // dataGridViewTextBoxColumn55
            // 
            this.dataGridViewTextBoxColumn55.DataPropertyName = "Num_estructura";
            this.dataGridViewTextBoxColumn55.HeaderText = "Num_estructura";
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            this.dataGridViewTextBoxColumn55.ReadOnly = true;
            this.dataGridViewTextBoxColumn55.Visible = false;
            // 
            // dataGridViewTextBoxColumn56
            // 
            this.dataGridViewTextBoxColumn56.DataPropertyName = "Profundidad_e";
            this.dataGridViewTextBoxColumn56.HeaderText = "Profundidad de estructura";
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            this.dataGridViewTextBoxColumn56.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn57
            // 
            this.dataGridViewTextBoxColumn57.DataPropertyName = "Tipo_e";
            this.dataGridViewTextBoxColumn57.HeaderText = "Tipo de estrcutura";
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            this.dataGridViewTextBoxColumn57.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn58
            // 
            this.dataGridViewTextBoxColumn58.DataPropertyName = "T_B";
            this.dataGridViewTextBoxColumn58.HeaderText = "T_B";
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            this.dataGridViewTextBoxColumn58.ReadOnly = true;
            this.dataGridViewTextBoxColumn58.Visible = false;
            // 
            // dataGridViewTextBoxColumn59
            // 
            this.dataGridViewTextBoxColumn59.DataPropertyName = "Alpha";
            this.dataGridViewTextBoxColumn59.HeaderText = "Alpha";
            this.dataGridViewTextBoxColumn59.Name = "dataGridViewTextBoxColumn59";
            this.dataGridViewTextBoxColumn59.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn60
            // 
            this.dataGridViewTextBoxColumn60.DataPropertyName = "Beta";
            this.dataGridViewTextBoxColumn60.HeaderText = "Beta";
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            this.dataGridViewTextBoxColumn60.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn61
            // 
            this.dataGridViewTextBoxColumn61.DataPropertyName = "Ref_to_top";
            this.dataGridViewTextBoxColumn61.HeaderText = "Ref_to_top";
            this.dataGridViewTextBoxColumn61.Name = "dataGridViewTextBoxColumn61";
            this.dataGridViewTextBoxColumn61.ReadOnly = true;
            this.dataGridViewTextBoxColumn61.Visible = false;
            // 
            // dataGridViewTextBoxColumn62
            // 
            this.dataGridViewTextBoxColumn62.DataPropertyName = "Cond_fractura";
            this.dataGridViewTextBoxColumn62.HeaderText = "Condición de fractura";
            this.dataGridViewTextBoxColumn62.Name = "dataGridViewTextBoxColumn62";
            this.dataGridViewTextBoxColumn62.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn63
            // 
            this.dataGridViewTextBoxColumn63.DataPropertyName = "Jrc";
            this.dataGridViewTextBoxColumn63.HeaderText = "Jrc";
            this.dataGridViewTextBoxColumn63.Name = "dataGridViewTextBoxColumn63";
            this.dataGridViewTextBoxColumn63.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn64
            // 
            this.dataGridViewTextBoxColumn64.DataPropertyName = "Jr";
            this.dataGridViewTextBoxColumn64.HeaderText = "Jr";
            this.dataGridViewTextBoxColumn64.Name = "dataGridViewTextBoxColumn64";
            this.dataGridViewTextBoxColumn64.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn65
            // 
            this.dataGridViewTextBoxColumn65.DataPropertyName = "Ja";
            this.dataGridViewTextBoxColumn65.HeaderText = "Ja";
            this.dataGridViewTextBoxColumn65.Name = "dataGridViewTextBoxColumn65";
            this.dataGridViewTextBoxColumn65.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn66
            // 
            this.dataGridViewTextBoxColumn66.DataPropertyName = "Espesor";
            this.dataGridViewTextBoxColumn66.HeaderText = "Espesor";
            this.dataGridViewTextBoxColumn66.Name = "dataGridViewTextBoxColumn66";
            this.dataGridViewTextBoxColumn66.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn67
            // 
            this.dataGridViewTextBoxColumn67.DataPropertyName = "Relleno_1";
            this.dataGridViewTextBoxColumn67.HeaderText = "Relleno";
            this.dataGridViewTextBoxColumn67.Name = "dataGridViewTextBoxColumn67";
            this.dataGridViewTextBoxColumn67.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn68
            // 
            this.dataGridViewTextBoxColumn68.DataPropertyName = "Relleno_2";
            this.dataGridViewTextBoxColumn68.HeaderText = "Relleno_2";
            this.dataGridViewTextBoxColumn68.Name = "dataGridViewTextBoxColumn68";
            this.dataGridViewTextBoxColumn68.ReadOnly = true;
            this.dataGridViewTextBoxColumn68.Visible = false;
            // 
            // dataGridViewTextBoxColumn69
            // 
            this.dataGridViewTextBoxColumn69.DataPropertyName = "Comentario";
            this.dataGridViewTextBoxColumn69.HeaderText = "Comentario";
            this.dataGridViewTextBoxColumn69.Name = "dataGridViewTextBoxColumn69";
            this.dataGridViewTextBoxColumn69.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn70
            // 
            this.dataGridViewTextBoxColumn70.DataPropertyName = "Geotecnico";
            this.dataGridViewTextBoxColumn70.HeaderText = "Geotécnico";
            this.dataGridViewTextBoxColumn70.Name = "dataGridViewTextBoxColumn70";
            this.dataGridViewTextBoxColumn70.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn71
            // 
            this.dataGridViewTextBoxColumn71.DataPropertyName = "FechaEs";
            this.dataGridViewTextBoxColumn71.HeaderText = "Fecha";
            this.dataGridViewTextBoxColumn71.Name = "dataGridViewTextBoxColumn71";
            this.dataGridViewTextBoxColumn71.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn72
            // 
            this.dataGridViewTextBoxColumn72.DataPropertyName = "Dip_cal";
            this.dataGridViewTextBoxColumn72.HeaderText = "Dip calculado";
            this.dataGridViewTextBoxColumn72.Name = "dataGridViewTextBoxColumn72";
            this.dataGridViewTextBoxColumn72.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn73
            // 
            this.dataGridViewTextBoxColumn73.DataPropertyName = "Dip_dir_cal";
            this.dataGridViewTextBoxColumn73.HeaderText = "Dip direction calculado";
            this.dataGridViewTextBoxColumn73.Name = "dataGridViewTextBoxColumn73";
            this.dataGridViewTextBoxColumn73.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn74
            // 
            this.dataGridViewTextBoxColumn74.DataPropertyName = "Dips";
            this.dataGridViewTextBoxColumn74.HeaderText = "Dip";
            this.dataGridViewTextBoxColumn74.Name = "dataGridViewTextBoxColumn74";
            this.dataGridViewTextBoxColumn74.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn75
            // 
            this.dataGridViewTextBoxColumn75.DataPropertyName = "Strike";
            this.dataGridViewTextBoxColumn75.HeaderText = "Strike";
            this.dataGridViewTextBoxColumn75.Name = "dataGridViewTextBoxColumn75";
            this.dataGridViewTextBoxColumn75.ReadOnly = true;
            // 
            // entEstructuraBindingSource
            // 
            this.entEstructuraBindingSource.DataSource = typeof(CapaEntidad.EntEstructura);
            // 
            // dgvEstructura2
            // 
            this.dgvEstructura2.AllowUserToAddRows = false;
            this.dgvEstructura2.AllowUserToDeleteRows = false;
            this.dgvEstructura2.AllowUserToResizeColumns = false;
            this.dgvEstructura2.AllowUserToResizeRows = false;
            this.dgvEstructura2.AutoGenerateColumns = false;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEstructura2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvEstructura2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvEstructura2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40,
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn45,
            this.dataGridViewTextBoxColumn46,
            this.dataGridViewTextBoxColumn47,
            this.dataGridViewTextBoxColumn48,
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewTextBoxColumn50});
            this.dgvEstructura2.DataSource = this.entEstructuraBindingSource;
            this.dgvEstructura2.Location = new System.Drawing.Point(525, 66);
            this.dgvEstructura2.Name = "dgvEstructura2";
            this.dgvEstructura2.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEstructura2.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvEstructura2.RowHeadersVisible = false;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgvEstructura2.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvEstructura2.Size = new System.Drawing.Size(477, 136);
            this.dgvEstructura2.TabIndex = 4;
            this.dgvEstructura2.Visible = false;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn26.HeaderText = "Id";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Visible = false;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "Id_Sondaje";
            this.dataGridViewTextBoxColumn27.HeaderText = "Id_Sondaje";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.Visible = false;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "Id_Corrida";
            this.dataGridViewTextBoxColumn28.HeaderText = "Id_Corrida";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.Visible = false;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "Id_Usuario";
            this.dataGridViewTextBoxColumn29.HeaderText = "Id_Usuario";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.Visible = false;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "Num_estructura";
            this.dataGridViewTextBoxColumn30.HeaderText = "Num_estructura";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.Visible = false;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "Profundidad_e";
            this.dataGridViewTextBoxColumn31.HeaderText = "Profundidad de estructura";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "Tipo_e";
            this.dataGridViewTextBoxColumn32.HeaderText = "Tipo de estrcutura";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "T_B";
            this.dataGridViewTextBoxColumn33.HeaderText = "T_B";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            this.dataGridViewTextBoxColumn33.Visible = false;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "Alpha";
            this.dataGridViewTextBoxColumn34.HeaderText = "Alpha";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.DataPropertyName = "Beta";
            this.dataGridViewTextBoxColumn35.HeaderText = "Beta";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.DataPropertyName = "Ref_to_top";
            this.dataGridViewTextBoxColumn36.HeaderText = "Ref_to_top";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.ReadOnly = true;
            this.dataGridViewTextBoxColumn36.Visible = false;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.DataPropertyName = "Cond_fractura";
            this.dataGridViewTextBoxColumn37.HeaderText = "Condición de fractura";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.DataPropertyName = "Jrc";
            this.dataGridViewTextBoxColumn38.HeaderText = "Jrc";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.DataPropertyName = "Jr";
            this.dataGridViewTextBoxColumn39.HeaderText = "Jr";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.DataPropertyName = "Ja";
            this.dataGridViewTextBoxColumn40.HeaderText = "Ja";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.DataPropertyName = "Espesor";
            this.dataGridViewTextBoxColumn41.HeaderText = "Espesor";
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.DataPropertyName = "Relleno_1";
            this.dataGridViewTextBoxColumn42.HeaderText = "Relleno";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.DataPropertyName = "Relleno_2";
            this.dataGridViewTextBoxColumn43.HeaderText = "Relleno_2";
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.ReadOnly = true;
            this.dataGridViewTextBoxColumn43.Visible = false;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.DataPropertyName = "Comentario";
            this.dataGridViewTextBoxColumn44.HeaderText = "Comentario";
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.DataPropertyName = "Geotecnico";
            this.dataGridViewTextBoxColumn45.HeaderText = "Geotécnico";
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.DataPropertyName = "FechaEs";
            this.dataGridViewTextBoxColumn46.HeaderText = "Fecha";
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.DataPropertyName = "Dip_cal";
            this.dataGridViewTextBoxColumn47.HeaderText = "Dip calculado";
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.DataPropertyName = "Dip_dir_cal";
            this.dataGridViewTextBoxColumn48.HeaderText = "Dip direction calculado";
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.DataPropertyName = "Dips";
            this.dataGridViewTextBoxColumn49.HeaderText = "Dip";
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.DataPropertyName = "Strike";
            this.dataGridViewTextBoxColumn50.HeaderText = "Strike";
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.ReadOnly = true;
            // 
            // dgvEstructura3
            // 
            this.dgvEstructura3.AllowUserToAddRows = false;
            this.dgvEstructura3.AllowUserToDeleteRows = false;
            this.dgvEstructura3.AllowUserToResizeColumns = false;
            this.dgvEstructura3.AllowUserToResizeRows = false;
            this.dgvEstructura3.AutoGenerateColumns = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEstructura3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvEstructura3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvEstructura3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25});
            this.dgvEstructura3.DataSource = this.entEstructuraBindingSource;
            this.dgvEstructura3.Location = new System.Drawing.Point(19, 241);
            this.dgvEstructura3.Name = "dgvEstructura3";
            this.dgvEstructura3.ReadOnly = true;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEstructura3.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvEstructura3.RowHeadersVisible = false;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgvEstructura3.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvEstructura3.Size = new System.Drawing.Size(477, 136);
            this.dgvEstructura3.TabIndex = 3;
            this.dgvEstructura3.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Id_Sondaje";
            this.dataGridViewTextBoxColumn2.HeaderText = "Id_Sondaje";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Id_Corrida";
            this.dataGridViewTextBoxColumn3.HeaderText = "Id_Corrida";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Id_Usuario";
            this.dataGridViewTextBoxColumn4.HeaderText = "Id_Usuario";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Num_estructura";
            this.dataGridViewTextBoxColumn5.HeaderText = "Num_estructura";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Profundidad_e";
            this.dataGridViewTextBoxColumn6.HeaderText = "Profundidad de estructura";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Tipo_e";
            this.dataGridViewTextBoxColumn7.HeaderText = "Tipo de estrcutura";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "T_B";
            this.dataGridViewTextBoxColumn8.HeaderText = "T_B";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Alpha";
            this.dataGridViewTextBoxColumn9.HeaderText = "Alpha";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Beta";
            this.dataGridViewTextBoxColumn10.HeaderText = "Beta";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Ref_to_top";
            this.dataGridViewTextBoxColumn11.HeaderText = "Ref_to_top";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Cond_fractura";
            this.dataGridViewTextBoxColumn12.HeaderText = "Condición de fractura";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "Jrc";
            this.dataGridViewTextBoxColumn13.HeaderText = "Jrc";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "Jr";
            this.dataGridViewTextBoxColumn14.HeaderText = "Jr";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Ja";
            this.dataGridViewTextBoxColumn15.HeaderText = "Ja";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "Espesor";
            this.dataGridViewTextBoxColumn16.HeaderText = "Espesor";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "Relleno_1";
            this.dataGridViewTextBoxColumn17.HeaderText = "Relleno";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "Relleno_2";
            this.dataGridViewTextBoxColumn18.HeaderText = "Relleno_2";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Visible = false;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "Comentario";
            this.dataGridViewTextBoxColumn19.HeaderText = "Comentario";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "Geotecnico";
            this.dataGridViewTextBoxColumn20.HeaderText = "Geotécnico";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "FechaEs";
            this.dataGridViewTextBoxColumn21.HeaderText = "Fecha";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "Dip_cal";
            this.dataGridViewTextBoxColumn22.HeaderText = "Dip calculado";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "Dip_dir_cal";
            this.dataGridViewTextBoxColumn23.HeaderText = "Dip direction calculado";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "Dips";
            this.dataGridViewTextBoxColumn24.HeaderText = "Dip";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "Strike";
            this.dataGridViewTextBoxColumn25.HeaderText = "Strike";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            // 
            // lbSondaje1
            // 
            this.lbSondaje1.AutoSize = true;
            this.lbSondaje1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSondaje1.Location = new System.Drawing.Point(15, 40);
            this.lbSondaje1.Name = "lbSondaje1";
            this.lbSondaje1.Size = new System.Drawing.Size(68, 20);
            this.lbSondaje1.TabIndex = 2;
            this.lbSondaje1.Text = "Sondaje";
            this.lbSondaje1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(476, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "DATA DE ESTRUCTURAS";
            // 
            // dgvEstructura1
            // 
            this.dgvEstructura1.AllowUserToAddRows = false;
            this.dgvEstructura1.AllowUserToDeleteRows = false;
            this.dgvEstructura1.AllowUserToResizeColumns = false;
            this.dgvEstructura1.AllowUserToResizeRows = false;
            this.dgvEstructura1.AutoGenerateColumns = false;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEstructura1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvEstructura1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvEstructura1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn3,
            this.idSondajeDataGridViewTextBoxColumn2,
            this.idCorridaDataGridViewTextBoxColumn,
            this.idUsuarioDataGridViewTextBoxColumn,
            this.numestructuraDataGridViewTextBoxColumn,
            this.profundidadeDataGridViewTextBoxColumn,
            this.tipoeDataGridViewTextBoxColumn,
            this.tBDataGridViewTextBoxColumn,
            this.alphaDataGridViewTextBoxColumn,
            this.betaDataGridViewTextBoxColumn,
            this.reftotopDataGridViewTextBoxColumn,
            this.condfracturaDataGridViewTextBoxColumn,
            this.jrcDataGridViewTextBoxColumn,
            this.jrDataGridViewTextBoxColumn,
            this.jaDataGridViewTextBoxColumn,
            this.espesorDataGridViewTextBoxColumn,
            this.relleno1DataGridViewTextBoxColumn,
            this.relleno2DataGridViewTextBoxColumn,
            this.comentarioDataGridViewTextBoxColumn,
            this.geotecnicoDataGridViewTextBoxColumn1,
            this.fechaEsDataGridViewTextBoxColumn,
            this.dipcalDataGridViewTextBoxColumn,
            this.dipdircalDataGridViewTextBoxColumn,
            this.dipsDataGridViewTextBoxColumn,
            this.strikeDataGridViewTextBoxColumn});
            this.dgvEstructura1.DataSource = this.entEstructuraBindingSource;
            this.dgvEstructura1.Location = new System.Drawing.Point(18, 66);
            this.dgvEstructura1.Name = "dgvEstructura1";
            this.dgvEstructura1.ReadOnly = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEstructura1.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgvEstructura1.RowHeadersVisible = false;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgvEstructura1.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvEstructura1.Size = new System.Drawing.Size(477, 136);
            this.dgvEstructura1.TabIndex = 0;
            this.dgvEstructura1.Visible = false;
            // 
            // idDataGridViewTextBoxColumn3
            // 
            this.idDataGridViewTextBoxColumn3.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn3.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn3.Name = "idDataGridViewTextBoxColumn3";
            this.idDataGridViewTextBoxColumn3.ReadOnly = true;
            this.idDataGridViewTextBoxColumn3.Visible = false;
            // 
            // idSondajeDataGridViewTextBoxColumn2
            // 
            this.idSondajeDataGridViewTextBoxColumn2.DataPropertyName = "Id_Sondaje";
            this.idSondajeDataGridViewTextBoxColumn2.HeaderText = "Id_Sondaje";
            this.idSondajeDataGridViewTextBoxColumn2.Name = "idSondajeDataGridViewTextBoxColumn2";
            this.idSondajeDataGridViewTextBoxColumn2.ReadOnly = true;
            this.idSondajeDataGridViewTextBoxColumn2.Visible = false;
            // 
            // idCorridaDataGridViewTextBoxColumn
            // 
            this.idCorridaDataGridViewTextBoxColumn.DataPropertyName = "Id_Corrida";
            this.idCorridaDataGridViewTextBoxColumn.HeaderText = "Id_Corrida";
            this.idCorridaDataGridViewTextBoxColumn.Name = "idCorridaDataGridViewTextBoxColumn";
            this.idCorridaDataGridViewTextBoxColumn.ReadOnly = true;
            this.idCorridaDataGridViewTextBoxColumn.Visible = false;
            // 
            // idUsuarioDataGridViewTextBoxColumn
            // 
            this.idUsuarioDataGridViewTextBoxColumn.DataPropertyName = "Id_Usuario";
            this.idUsuarioDataGridViewTextBoxColumn.HeaderText = "Id_Usuario";
            this.idUsuarioDataGridViewTextBoxColumn.Name = "idUsuarioDataGridViewTextBoxColumn";
            this.idUsuarioDataGridViewTextBoxColumn.ReadOnly = true;
            this.idUsuarioDataGridViewTextBoxColumn.Visible = false;
            // 
            // numestructuraDataGridViewTextBoxColumn
            // 
            this.numestructuraDataGridViewTextBoxColumn.DataPropertyName = "Num_estructura";
            this.numestructuraDataGridViewTextBoxColumn.HeaderText = "Num_estructura";
            this.numestructuraDataGridViewTextBoxColumn.Name = "numestructuraDataGridViewTextBoxColumn";
            this.numestructuraDataGridViewTextBoxColumn.ReadOnly = true;
            this.numestructuraDataGridViewTextBoxColumn.Visible = false;
            // 
            // profundidadeDataGridViewTextBoxColumn
            // 
            this.profundidadeDataGridViewTextBoxColumn.DataPropertyName = "Profundidad_e";
            this.profundidadeDataGridViewTextBoxColumn.HeaderText = "Profundidad de estructura";
            this.profundidadeDataGridViewTextBoxColumn.Name = "profundidadeDataGridViewTextBoxColumn";
            this.profundidadeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tipoeDataGridViewTextBoxColumn
            // 
            this.tipoeDataGridViewTextBoxColumn.DataPropertyName = "Tipo_e";
            this.tipoeDataGridViewTextBoxColumn.HeaderText = "Tipo de estrcutura";
            this.tipoeDataGridViewTextBoxColumn.Name = "tipoeDataGridViewTextBoxColumn";
            this.tipoeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tBDataGridViewTextBoxColumn
            // 
            this.tBDataGridViewTextBoxColumn.DataPropertyName = "T_B";
            this.tBDataGridViewTextBoxColumn.HeaderText = "T_B";
            this.tBDataGridViewTextBoxColumn.Name = "tBDataGridViewTextBoxColumn";
            this.tBDataGridViewTextBoxColumn.ReadOnly = true;
            this.tBDataGridViewTextBoxColumn.Visible = false;
            // 
            // alphaDataGridViewTextBoxColumn
            // 
            this.alphaDataGridViewTextBoxColumn.DataPropertyName = "Alpha";
            this.alphaDataGridViewTextBoxColumn.HeaderText = "Alpha";
            this.alphaDataGridViewTextBoxColumn.Name = "alphaDataGridViewTextBoxColumn";
            this.alphaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // betaDataGridViewTextBoxColumn
            // 
            this.betaDataGridViewTextBoxColumn.DataPropertyName = "Beta";
            this.betaDataGridViewTextBoxColumn.HeaderText = "Beta";
            this.betaDataGridViewTextBoxColumn.Name = "betaDataGridViewTextBoxColumn";
            this.betaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reftotopDataGridViewTextBoxColumn
            // 
            this.reftotopDataGridViewTextBoxColumn.DataPropertyName = "Ref_to_top";
            this.reftotopDataGridViewTextBoxColumn.HeaderText = "Ref_to_top";
            this.reftotopDataGridViewTextBoxColumn.Name = "reftotopDataGridViewTextBoxColumn";
            this.reftotopDataGridViewTextBoxColumn.ReadOnly = true;
            this.reftotopDataGridViewTextBoxColumn.Visible = false;
            // 
            // condfracturaDataGridViewTextBoxColumn
            // 
            this.condfracturaDataGridViewTextBoxColumn.DataPropertyName = "Cond_fractura";
            this.condfracturaDataGridViewTextBoxColumn.HeaderText = "Condición de fractura";
            this.condfracturaDataGridViewTextBoxColumn.Name = "condfracturaDataGridViewTextBoxColumn";
            this.condfracturaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // jrcDataGridViewTextBoxColumn
            // 
            this.jrcDataGridViewTextBoxColumn.DataPropertyName = "Jrc";
            this.jrcDataGridViewTextBoxColumn.HeaderText = "Jrc";
            this.jrcDataGridViewTextBoxColumn.Name = "jrcDataGridViewTextBoxColumn";
            this.jrcDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // jrDataGridViewTextBoxColumn
            // 
            this.jrDataGridViewTextBoxColumn.DataPropertyName = "Jr";
            this.jrDataGridViewTextBoxColumn.HeaderText = "Jr";
            this.jrDataGridViewTextBoxColumn.Name = "jrDataGridViewTextBoxColumn";
            this.jrDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // jaDataGridViewTextBoxColumn
            // 
            this.jaDataGridViewTextBoxColumn.DataPropertyName = "Ja";
            this.jaDataGridViewTextBoxColumn.HeaderText = "Ja";
            this.jaDataGridViewTextBoxColumn.Name = "jaDataGridViewTextBoxColumn";
            this.jaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // espesorDataGridViewTextBoxColumn
            // 
            this.espesorDataGridViewTextBoxColumn.DataPropertyName = "Espesor";
            this.espesorDataGridViewTextBoxColumn.HeaderText = "Espesor";
            this.espesorDataGridViewTextBoxColumn.Name = "espesorDataGridViewTextBoxColumn";
            this.espesorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // relleno1DataGridViewTextBoxColumn
            // 
            this.relleno1DataGridViewTextBoxColumn.DataPropertyName = "Relleno_1";
            this.relleno1DataGridViewTextBoxColumn.HeaderText = "Relleno";
            this.relleno1DataGridViewTextBoxColumn.Name = "relleno1DataGridViewTextBoxColumn";
            this.relleno1DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // relleno2DataGridViewTextBoxColumn
            // 
            this.relleno2DataGridViewTextBoxColumn.DataPropertyName = "Relleno_2";
            this.relleno2DataGridViewTextBoxColumn.HeaderText = "Relleno_2";
            this.relleno2DataGridViewTextBoxColumn.Name = "relleno2DataGridViewTextBoxColumn";
            this.relleno2DataGridViewTextBoxColumn.ReadOnly = true;
            this.relleno2DataGridViewTextBoxColumn.Visible = false;
            // 
            // comentarioDataGridViewTextBoxColumn
            // 
            this.comentarioDataGridViewTextBoxColumn.DataPropertyName = "Comentario";
            this.comentarioDataGridViewTextBoxColumn.HeaderText = "Comentario";
            this.comentarioDataGridViewTextBoxColumn.Name = "comentarioDataGridViewTextBoxColumn";
            this.comentarioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // geotecnicoDataGridViewTextBoxColumn1
            // 
            this.geotecnicoDataGridViewTextBoxColumn1.DataPropertyName = "Geotecnico";
            this.geotecnicoDataGridViewTextBoxColumn1.HeaderText = "Geotécnico";
            this.geotecnicoDataGridViewTextBoxColumn1.Name = "geotecnicoDataGridViewTextBoxColumn1";
            this.geotecnicoDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // fechaEsDataGridViewTextBoxColumn
            // 
            this.fechaEsDataGridViewTextBoxColumn.DataPropertyName = "FechaEs";
            this.fechaEsDataGridViewTextBoxColumn.HeaderText = "Fecha";
            this.fechaEsDataGridViewTextBoxColumn.Name = "fechaEsDataGridViewTextBoxColumn";
            this.fechaEsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dipcalDataGridViewTextBoxColumn
            // 
            this.dipcalDataGridViewTextBoxColumn.DataPropertyName = "Dip_cal";
            this.dipcalDataGridViewTextBoxColumn.HeaderText = "Dip calculado";
            this.dipcalDataGridViewTextBoxColumn.Name = "dipcalDataGridViewTextBoxColumn";
            this.dipcalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dipdircalDataGridViewTextBoxColumn
            // 
            this.dipdircalDataGridViewTextBoxColumn.DataPropertyName = "Dip_dir_cal";
            this.dipdircalDataGridViewTextBoxColumn.HeaderText = "Dip direction calculado";
            this.dipdircalDataGridViewTextBoxColumn.Name = "dipdircalDataGridViewTextBoxColumn";
            this.dipdircalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dipsDataGridViewTextBoxColumn
            // 
            this.dipsDataGridViewTextBoxColumn.DataPropertyName = "Dips";
            this.dipsDataGridViewTextBoxColumn.HeaderText = "Dip";
            this.dipsDataGridViewTextBoxColumn.Name = "dipsDataGridViewTextBoxColumn";
            this.dipsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // strikeDataGridViewTextBoxColumn
            // 
            this.strikeDataGridViewTextBoxColumn.DataPropertyName = "Strike";
            this.strikeDataGridViewTextBoxColumn.HeaderText = "Strike";
            this.strikeDataGridViewTextBoxColumn.Name = "strikeDataGridViewTextBoxColumn";
            this.strikeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // btnExportar
            // 
            this.btnExportar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnExportar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportar.Location = new System.Drawing.Point(29, 656);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(102, 33);
            this.btnExportar.TabIndex = 11;
            this.btnExportar.Text = "Exportar";
            this.btnExportar.UseVisualStyleBackColor = false;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // cbTipoDiagrama
            // 
            this.cbTipoDiagrama.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTipoDiagrama.FormattingEnabled = true;
            this.cbTipoDiagrama.Items.AddRange(new object[] {
            "Diagrama de Planos",
            "Concentracion de polos",
            "Diagrama de Rosette"});
            this.cbTipoDiagrama.Location = new System.Drawing.Point(907, 662);
            this.cbTipoDiagrama.Name = "cbTipoDiagrama";
            this.cbTipoDiagrama.Size = new System.Drawing.Size(121, 21);
            this.cbTipoDiagrama.TabIndex = 10;
            // 
            // btnGenerar
            // 
            this.btnGenerar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnGenerar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerar.Location = new System.Drawing.Point(1038, 659);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(89, 26);
            this.btnGenerar.TabIndex = 9;
            this.btnGenerar.Text = "Generar";
            this.btnGenerar.UseVisualStyleBackColor = false;
            this.btnGenerar.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(758, 662);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Tipo de diagrama: ";
            // 
            // VistaEstereografia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1176, 701);
            this.Controls.Add(this.btnExportar);
            this.Controls.Add(this.cbTipoDiagrama);
            this.Controls.Add(this.btnGenerar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panelData);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "VistaEstereografia";
            this.Text = "VistaEstereografia";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelLimites.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entCorridaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entSondajeBindingSource)).EndInit();
            this.panelData.ResumeLayout(false);
            this.panelData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstructura4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entEstructuraBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstructura2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstructura3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstructura1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox cbTramo;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.CheckBox cbTipoEstructura;
        private System.Windows.Forms.Panel panelLimites;
        private System.Windows.Forms.DataGridView dgvHasta;
        private System.Windows.Forms.DataGridView dgvDesde;
        private System.Windows.Forms.BindingSource entSondajeBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idUserDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSondajeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn desdeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn hastaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn perforacionDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn recuperacionDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource entCorridaBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idUserDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSondajeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn desdeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hastaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn perforacionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn recuperacionDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel panelData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dipDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource entEstructuraBindingSource;
        private System.Windows.Forms.CheckedListBox clbTipoEstrcuturas;
        private System.Windows.Forms.CheckedListBox clbSondajes;
        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.ComboBox cbTipoDiagrama;
        private System.Windows.Forms.Button btnGenerar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckedListBox clbFiltros;
        private System.Windows.Forms.Label lbSondaje4;
        private System.Windows.Forms.Label lbSondaje3;
        private System.Windows.Forms.Label lbSondaje2;
        private System.Windows.Forms.DataGridView dgvEstructura4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn59;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn61;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn62;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn63;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn64;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn65;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn66;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn67;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn68;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn69;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn70;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn71;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn72;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn73;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn74;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn75;
        private System.Windows.Forms.DataGridView dgvEstructura2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridView dgvEstructura3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.Label lbSondaje1;
        private System.Windows.Forms.DataGridView dgvEstructura1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSondajeDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idCorridaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idUsuarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numestructuraDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn profundidadeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tBDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn alphaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn betaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reftotopDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn condfracturaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jrcDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jrDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn espesorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn relleno1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn relleno2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn comentarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn geotecnicoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaEsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dipcalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dipdircalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dipsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn strikeDataGridViewTextBoxColumn;
        private System.Windows.Forms.CheckBox cbFiltros;
    }
}