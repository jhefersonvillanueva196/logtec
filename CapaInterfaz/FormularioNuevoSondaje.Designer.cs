﻿
namespace CapaInterfaz
{
    partial class FormularioNuevoSondaje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbValidar = new System.Windows.Forms.CheckBox();
            this.cboLineaCore = new System.Windows.Forms.ComboBox();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.btnCrear = new System.Windows.Forms.Button();
            this.txtAzimut = new System.Windows.Forms.TextBox();
            this.txtDip = new System.Windows.Forms.TextBox();
            this.txtMaquina = new System.Windows.Forms.TextBox();
            this.txtCota = new System.Windows.Forms.TextBox();
            this.txtEste = new System.Windows.Forms.TextBox();
            this.txtNorte = new System.Windows.Forms.TextBox();
            this.txtProfundidad = new System.Windows.Forms.TextBox();
            this.txtNombreSondaje = new System.Windows.Forms.TextBox();
            this.txtGeotecnico = new System.Windows.Forms.TextBox();
            this.txtProyecto = new System.Windows.Forms.TextBox();
            this.txtCliente = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tips = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel1.Controls.Add(this.cbValidar);
            this.panel1.Controls.Add(this.cboLineaCore);
            this.panel1.Controls.Add(this.dtpFecha);
            this.panel1.Controls.Add(this.btnCerrar);
            this.panel1.Controls.Add(this.btnCrear);
            this.panel1.Controls.Add(this.txtAzimut);
            this.panel1.Controls.Add(this.txtDip);
            this.panel1.Controls.Add(this.txtMaquina);
            this.panel1.Controls.Add(this.txtCota);
            this.panel1.Controls.Add(this.txtEste);
            this.panel1.Controls.Add(this.txtNorte);
            this.panel1.Controls.Add(this.txtProfundidad);
            this.panel1.Controls.Add(this.txtNombreSondaje);
            this.panel1.Controls.Add(this.txtGeotecnico);
            this.panel1.Controls.Add(this.txtProyecto);
            this.panel1.Controls.Add(this.txtCliente);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(860, 501);
            this.panel1.TabIndex = 0;
            // 
            // cbValidar
            // 
            this.cbValidar.AutoSize = true;
            this.cbValidar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbValidar.Location = new System.Drawing.Point(691, 18);
            this.cbValidar.Name = "cbValidar";
            this.cbValidar.Size = new System.Drawing.Size(77, 24);
            this.cbValidar.TabIndex = 32;
            this.cbValidar.Text = "Validar";
            this.cbValidar.UseVisualStyleBackColor = true;
            // 
            // cboLineaCore
            // 
            this.cboLineaCore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLineaCore.FormattingEnabled = true;
            this.cboLineaCore.Items.AddRange(new object[] {
            "PQ",
            "HQ",
            "HQ3",
            "NQ",
            "NQ3",
            "BQ"});
            this.cboLineaCore.Location = new System.Drawing.Point(642, 213);
            this.cboLineaCore.Name = "cboLineaCore";
            this.cboLineaCore.Size = new System.Drawing.Size(126, 21);
            this.cboLineaCore.TabIndex = 31;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Location = new System.Drawing.Point(267, 260);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(126, 20);
            this.dtpFecha.TabIndex = 30;
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnCerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.ForeColor = System.Drawing.Color.White;
            this.btnCerrar.Location = new System.Drawing.Point(562, 437);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(102, 29);
            this.btnCerrar.TabIndex = 28;
            this.btnCerrar.Text = "Cancelar";
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // btnCrear
            // 
            this.btnCrear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnCrear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrear.ForeColor = System.Drawing.Color.White;
            this.btnCrear.Location = new System.Drawing.Point(196, 437);
            this.btnCrear.Name = "btnCrear";
            this.btnCrear.Size = new System.Drawing.Size(102, 29);
            this.btnCrear.TabIndex = 27;
            this.btnCrear.Text = "Crear";
            this.btnCrear.UseVisualStyleBackColor = false;
            this.btnCrear.Click += new System.EventHandler(this.btnCrear_Click);
            // 
            // txtAzimut
            // 
            this.txtAzimut.Location = new System.Drawing.Point(642, 309);
            this.txtAzimut.Name = "txtAzimut";
            this.txtAzimut.Size = new System.Drawing.Size(126, 20);
            this.txtAzimut.TabIndex = 26;
            this.txtAzimut.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAzimut_KeyPress);
            this.txtAzimut.Leave += new System.EventHandler(this.txtAzimut_Leave);
            // 
            // txtDip
            // 
            this.txtDip.Location = new System.Drawing.Point(642, 263);
            this.txtDip.Name = "txtDip";
            this.txtDip.Size = new System.Drawing.Size(126, 20);
            this.txtDip.TabIndex = 25;
            this.txtDip.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDip_KeyPress);
            this.txtDip.Leave += new System.EventHandler(this.txtDip_Leave);
            // 
            // txtMaquina
            // 
            this.txtMaquina.Location = new System.Drawing.Point(642, 167);
            this.txtMaquina.Name = "txtMaquina";
            this.txtMaquina.Size = new System.Drawing.Size(126, 20);
            this.txtMaquina.TabIndex = 23;
            // 
            // txtCota
            // 
            this.txtCota.Location = new System.Drawing.Point(642, 119);
            this.txtCota.Name = "txtCota";
            this.txtCota.Size = new System.Drawing.Size(126, 20);
            this.txtCota.TabIndex = 22;
            this.txtCota.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCota_KeyPress);
            this.txtCota.Leave += new System.EventHandler(this.txtCota_Leave);
            // 
            // txtEste
            // 
            this.txtEste.Location = new System.Drawing.Point(642, 71);
            this.txtEste.Name = "txtEste";
            this.txtEste.Size = new System.Drawing.Size(126, 20);
            this.txtEste.TabIndex = 21;
            this.txtEste.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEste_KeyPress);
            this.txtEste.Leave += new System.EventHandler(this.txtEste_Leave);
            // 
            // txtNorte
            // 
            this.txtNorte.Location = new System.Drawing.Point(267, 357);
            this.txtNorte.Name = "txtNorte";
            this.txtNorte.Size = new System.Drawing.Size(126, 20);
            this.txtNorte.TabIndex = 20;
            this.txtNorte.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNorte_KeyPress);
            this.txtNorte.Leave += new System.EventHandler(this.txtNorte_Leave);
            // 
            // txtProfundidad
            // 
            this.txtProfundidad.Location = new System.Drawing.Point(267, 309);
            this.txtProfundidad.Name = "txtProfundidad";
            this.txtProfundidad.Size = new System.Drawing.Size(126, 20);
            this.txtProfundidad.TabIndex = 19;
            this.txtProfundidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtProfundidad_KeyPress);
            this.txtProfundidad.Leave += new System.EventHandler(this.txtProfundidad_Leave);
            // 
            // txtNombreSondaje
            // 
            this.txtNombreSondaje.Location = new System.Drawing.Point(267, 213);
            this.txtNombreSondaje.Name = "txtNombreSondaje";
            this.txtNombreSondaje.Size = new System.Drawing.Size(126, 20);
            this.txtNombreSondaje.TabIndex = 17;
            // 
            // txtGeotecnico
            // 
            this.txtGeotecnico.Enabled = false;
            this.txtGeotecnico.Location = new System.Drawing.Point(267, 165);
            this.txtGeotecnico.Name = "txtGeotecnico";
            this.txtGeotecnico.Size = new System.Drawing.Size(126, 20);
            this.txtGeotecnico.TabIndex = 16;
            // 
            // txtProyecto
            // 
            this.txtProyecto.Location = new System.Drawing.Point(267, 117);
            this.txtProyecto.Name = "txtProyecto";
            this.txtProyecto.Size = new System.Drawing.Size(126, 20);
            this.txtProyecto.TabIndex = 15;
            // 
            // txtCliente
            // 
            this.txtCliente.Location = new System.Drawing.Point(267, 69);
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.Size = new System.Drawing.Size(126, 20);
            this.txtCliente.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(361, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(138, 24);
            this.label14.TabIndex = 13;
            this.label14.Text = "Nuevo sondaje";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(500, 309);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 20);
            this.label13.TabIndex = 12;
            this.label13.Text = "Azimut";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(500, 261);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 20);
            this.label12.TabIndex = 11;
            this.label12.Text = "Dip";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(500, 165);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 20);
            this.label11.TabIndex = 10;
            this.label11.Text = "Máquina";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(500, 213);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 20);
            this.label10.TabIndex = 9;
            this.label10.Text = "Línea de Core";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(500, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "Cota";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(500, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "Este";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(93, 357);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Norte";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(93, 309);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Profundidad";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(93, 261);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Fecha";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(93, 213);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nombre de sondaje";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(93, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Geotécnico";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(93, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Proyecto";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(93, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cliente";
            // 
            // FormularioNuevoSondaje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 501);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormularioNuevoSondaje";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Formulario Nuevo Sondaje";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Button btnCrear;
        private System.Windows.Forms.TextBox txtAzimut;
        private System.Windows.Forms.TextBox txtDip;
        private System.Windows.Forms.TextBox txtMaquina;
        private System.Windows.Forms.TextBox txtCota;
        private System.Windows.Forms.TextBox txtEste;
        private System.Windows.Forms.TextBox txtNorte;
        private System.Windows.Forms.TextBox txtProfundidad;
        private System.Windows.Forms.TextBox txtNombreSondaje;
        private System.Windows.Forms.TextBox txtGeotecnico;
        private System.Windows.Forms.TextBox txtProyecto;
        private System.Windows.Forms.TextBox txtCliente;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.ComboBox cboLineaCore;
        private System.Windows.Forms.ToolTip tips;
        private System.Windows.Forms.CheckBox cbValidar;
    }
}