﻿
namespace CapaInterfaz
{
    partial class VistaLogueo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbCerrar = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnEliminarCorrida = new System.Windows.Forms.Button();
            this.btnEditarCorrida = new System.Windows.Forms.Button();
            this.btnNuevaCorrida = new System.Windows.Forms.Button();
            this.dgvCorrida = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idUserDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idSondajeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.desdeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hastaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perforacionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recuperacionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.validarDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.entCorridaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnEliminarEstructura = new System.Windows.Forms.Button();
            this.btnEditarEstructura = new System.Windows.Forms.Button();
            this.btnNuevaEstructura = new System.Windows.Forms.Button();
            this.dgvEstructura = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idSondajeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idCorridaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idUsuarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numestructuraDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.profundidadeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tBDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alphaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.betaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reftotopDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.condfracturaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jrcDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.espesorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.relleno1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.relleno2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comentarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.geotecnicoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaEsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dipcalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dipdircalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dipsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.strikeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stateDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.validarDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.entEstructuraBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnEliminarSondaje = new System.Windows.Forms.Button();
            this.btnEditarSondaje = new System.Windows.Forms.Button();
            this.btnNuevoSondaje = new System.Windows.Forms.Button();
            this.dgvSondaje = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idUserDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clienteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.proyectoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.geotecnicoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomsondajeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.profundidadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.norteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.esteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cotaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maquinaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lineaCoreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dipDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.azimutDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.validarDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.entSondajeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCerrar)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCorrida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entCorridaBindingSource)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstructura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entEstructuraBindingSource)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSondaje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entSondajeBindingSource)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(558, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "LOGUEO";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(31)))), ((int)(((byte)(67)))));
            this.panel1.Controls.Add(this.pbCerrar);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(695, 33);
            this.panel1.TabIndex = 1;
            // 
            // pbCerrar
            // 
            this.pbCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbCerrar.Image = global::CapaInterfaz.Properties.Resources.cerrar;
            this.pbCerrar.Location = new System.Drawing.Point(667, 8);
            this.pbCerrar.Name = "pbCerrar";
            this.pbCerrar.Size = new System.Drawing.Size(16, 16);
            this.pbCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbCerrar.TabIndex = 1;
            this.pbCerrar.TabStop = false;
            this.pbCerrar.Click += new System.EventHandler(this.pbCerrar_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Location = new System.Drawing.Point(0, 260);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(695, 215);
            this.panel3.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btnEliminarCorrida);
            this.panel6.Controls.Add(this.btnEditarCorrida);
            this.panel6.Controls.Add(this.btnNuevaCorrida);
            this.panel6.Controls.Add(this.dgvCorrida);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Location = new System.Drawing.Point(14, 13);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(666, 193);
            this.panel6.TabIndex = 8;
            // 
            // btnEliminarCorrida
            // 
            this.btnEliminarCorrida.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEliminarCorrida.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnEliminarCorrida.Enabled = false;
            this.btnEliminarCorrida.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnEliminarCorrida.ForeColor = System.Drawing.Color.White;
            this.btnEliminarCorrida.Location = new System.Drawing.Point(544, 3);
            this.btnEliminarCorrida.Name = "btnEliminarCorrida";
            this.btnEliminarCorrida.Size = new System.Drawing.Size(94, 23);
            this.btnEliminarCorrida.TabIndex = 4;
            this.btnEliminarCorrida.Text = "Eliminar corrida";
            this.btnEliminarCorrida.UseVisualStyleBackColor = false;
            this.btnEliminarCorrida.Click += new System.EventHandler(this.btnEliminarCorrida_Click);
            // 
            // btnEditarCorrida
            // 
            this.btnEditarCorrida.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditarCorrida.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnEditarCorrida.Enabled = false;
            this.btnEditarCorrida.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnEditarCorrida.ForeColor = System.Drawing.Color.White;
            this.btnEditarCorrida.Location = new System.Drawing.Point(454, 3);
            this.btnEditarCorrida.Name = "btnEditarCorrida";
            this.btnEditarCorrida.Size = new System.Drawing.Size(84, 23);
            this.btnEditarCorrida.TabIndex = 3;
            this.btnEditarCorrida.Text = "Editar corrida";
            this.btnEditarCorrida.UseVisualStyleBackColor = false;
            this.btnEditarCorrida.Click += new System.EventHandler(this.btnEditarCorrida_Click);
            // 
            // btnNuevaCorrida
            // 
            this.btnNuevaCorrida.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNuevaCorrida.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnNuevaCorrida.Enabled = false;
            this.btnNuevaCorrida.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnNuevaCorrida.ForeColor = System.Drawing.Color.White;
            this.btnNuevaCorrida.Location = new System.Drawing.Point(357, 4);
            this.btnNuevaCorrida.Name = "btnNuevaCorrida";
            this.btnNuevaCorrida.Size = new System.Drawing.Size(91, 23);
            this.btnNuevaCorrida.TabIndex = 2;
            this.btnNuevaCorrida.Text = "Nueva corrida";
            this.btnNuevaCorrida.UseVisualStyleBackColor = false;
            this.btnNuevaCorrida.Click += new System.EventHandler(this.btnNuevaCorrida_Click);
            // 
            // dgvCorrida
            // 
            this.dgvCorrida.AllowUserToAddRows = false;
            this.dgvCorrida.AllowUserToDeleteRows = false;
            this.dgvCorrida.AllowUserToResizeColumns = false;
            this.dgvCorrida.AllowUserToResizeRows = false;
            this.dgvCorrida.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCorrida.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCorrida.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCorrida.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvCorrida.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.idUserDataGridViewTextBoxColumn1,
            this.idSondajeDataGridViewTextBoxColumn,
            this.desdeDataGridViewTextBoxColumn,
            this.hastaDataGridViewTextBoxColumn,
            this.perforacionDataGridViewTextBoxColumn,
            this.recuperacionDataGridViewTextBoxColumn,
            this.stateDataGridViewTextBoxColumn1,
            this.validarDataGridViewTextBoxColumn1});
            this.dgvCorrida.DataSource = this.entCorridaBindingSource;
            this.dgvCorrida.Location = new System.Drawing.Point(11, 30);
            this.dgvCorrida.Name = "dgvCorrida";
            this.dgvCorrida.ReadOnly = true;
            this.dgvCorrida.RowHeadersVisible = false;
            this.dgvCorrida.Size = new System.Drawing.Size(642, 153);
            this.dgvCorrida.TabIndex = 1;
            this.dgvCorrida.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvCorrida_CellMouseClick);
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // idUserDataGridViewTextBoxColumn1
            // 
            this.idUserDataGridViewTextBoxColumn1.DataPropertyName = "IdUser";
            this.idUserDataGridViewTextBoxColumn1.HeaderText = "IdUser";
            this.idUserDataGridViewTextBoxColumn1.Name = "idUserDataGridViewTextBoxColumn1";
            this.idUserDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // idSondajeDataGridViewTextBoxColumn
            // 
            this.idSondajeDataGridViewTextBoxColumn.DataPropertyName = "Id_Sondaje";
            this.idSondajeDataGridViewTextBoxColumn.HeaderText = "Id_Sondaje";
            this.idSondajeDataGridViewTextBoxColumn.Name = "idSondajeDataGridViewTextBoxColumn";
            this.idSondajeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // desdeDataGridViewTextBoxColumn
            // 
            this.desdeDataGridViewTextBoxColumn.DataPropertyName = "Desde";
            this.desdeDataGridViewTextBoxColumn.HeaderText = "Desde";
            this.desdeDataGridViewTextBoxColumn.Name = "desdeDataGridViewTextBoxColumn";
            this.desdeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // hastaDataGridViewTextBoxColumn
            // 
            this.hastaDataGridViewTextBoxColumn.DataPropertyName = "Hasta";
            this.hastaDataGridViewTextBoxColumn.HeaderText = "Hasta";
            this.hastaDataGridViewTextBoxColumn.Name = "hastaDataGridViewTextBoxColumn";
            this.hastaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // perforacionDataGridViewTextBoxColumn
            // 
            this.perforacionDataGridViewTextBoxColumn.DataPropertyName = "Perforacion";
            this.perforacionDataGridViewTextBoxColumn.HeaderText = "Perforacion";
            this.perforacionDataGridViewTextBoxColumn.Name = "perforacionDataGridViewTextBoxColumn";
            this.perforacionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // recuperacionDataGridViewTextBoxColumn
            // 
            this.recuperacionDataGridViewTextBoxColumn.DataPropertyName = "Recuperacion";
            this.recuperacionDataGridViewTextBoxColumn.HeaderText = "Recuperacion";
            this.recuperacionDataGridViewTextBoxColumn.Name = "recuperacionDataGridViewTextBoxColumn";
            this.recuperacionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // stateDataGridViewTextBoxColumn1
            // 
            this.stateDataGridViewTextBoxColumn1.DataPropertyName = "State";
            this.stateDataGridViewTextBoxColumn1.HeaderText = "State";
            this.stateDataGridViewTextBoxColumn1.Name = "stateDataGridViewTextBoxColumn1";
            this.stateDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // validarDataGridViewTextBoxColumn1
            // 
            this.validarDataGridViewTextBoxColumn1.DataPropertyName = "Validar";
            this.validarDataGridViewTextBoxColumn1.HeaderText = "Validar";
            this.validarDataGridViewTextBoxColumn1.Name = "validarDataGridViewTextBoxColumn1";
            this.validarDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // entCorridaBindingSource
            // 
            this.entCorridaBindingSource.DataSource = typeof(CapaEntidad.EntCorrida);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "Corrida";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Location = new System.Drawing.Point(0, 481);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(695, 215);
            this.panel4.TabIndex = 4;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btnEliminarEstructura);
            this.panel5.Controls.Add(this.btnEditarEstructura);
            this.panel5.Controls.Add(this.btnNuevaEstructura);
            this.panel5.Controls.Add(this.dgvEstructura);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Location = new System.Drawing.Point(14, 13);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(666, 193);
            this.panel5.TabIndex = 8;
            // 
            // btnEliminarEstructura
            // 
            this.btnEliminarEstructura.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEliminarEstructura.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnEliminarEstructura.Enabled = false;
            this.btnEliminarEstructura.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnEliminarEstructura.ForeColor = System.Drawing.Color.White;
            this.btnEliminarEstructura.Location = new System.Drawing.Point(532, 4);
            this.btnEliminarEstructura.Name = "btnEliminarEstructura";
            this.btnEliminarEstructura.Size = new System.Drawing.Size(107, 23);
            this.btnEliminarEstructura.TabIndex = 4;
            this.btnEliminarEstructura.Text = "Eliminar estructura";
            this.btnEliminarEstructura.UseVisualStyleBackColor = false;
            this.btnEliminarEstructura.Click += new System.EventHandler(this.btnEliminarEstructura_Click);
            // 
            // btnEditarEstructura
            // 
            this.btnEditarEstructura.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditarEstructura.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnEditarEstructura.Enabled = false;
            this.btnEditarEstructura.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnEditarEstructura.ForeColor = System.Drawing.Color.White;
            this.btnEditarEstructura.Location = new System.Drawing.Point(433, 4);
            this.btnEditarEstructura.Name = "btnEditarEstructura";
            this.btnEditarEstructura.Size = new System.Drawing.Size(93, 23);
            this.btnEditarEstructura.TabIndex = 3;
            this.btnEditarEstructura.Text = "Editar estructura";
            this.btnEditarEstructura.UseVisualStyleBackColor = false;
            this.btnEditarEstructura.Click += new System.EventHandler(this.btnEditarEstructura_Click);
            // 
            // btnNuevaEstructura
            // 
            this.btnNuevaEstructura.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNuevaEstructura.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnNuevaEstructura.Enabled = false;
            this.btnNuevaEstructura.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnNuevaEstructura.ForeColor = System.Drawing.Color.White;
            this.btnNuevaEstructura.Location = new System.Drawing.Point(328, 4);
            this.btnNuevaEstructura.Name = "btnNuevaEstructura";
            this.btnNuevaEstructura.Size = new System.Drawing.Size(99, 23);
            this.btnNuevaEstructura.TabIndex = 2;
            this.btnNuevaEstructura.Text = "Nueva estructura";
            this.btnNuevaEstructura.UseVisualStyleBackColor = false;
            this.btnNuevaEstructura.Click += new System.EventHandler(this.btnNuevaEstructura_Click);
            // 
            // dgvEstructura
            // 
            this.dgvEstructura.AllowUserToAddRows = false;
            this.dgvEstructura.AllowUserToDeleteRows = false;
            this.dgvEstructura.AllowUserToResizeColumns = false;
            this.dgvEstructura.AllowUserToResizeRows = false;
            this.dgvEstructura.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvEstructura.AutoGenerateColumns = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEstructura.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvEstructura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvEstructura.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn2,
            this.idSondajeDataGridViewTextBoxColumn1,
            this.idCorridaDataGridViewTextBoxColumn,
            this.idUsuarioDataGridViewTextBoxColumn,
            this.numestructuraDataGridViewTextBoxColumn,
            this.profundidadeDataGridViewTextBoxColumn,
            this.tipoeDataGridViewTextBoxColumn,
            this.tBDataGridViewTextBoxColumn,
            this.alphaDataGridViewTextBoxColumn,
            this.betaDataGridViewTextBoxColumn,
            this.reftotopDataGridViewTextBoxColumn,
            this.condfracturaDataGridViewTextBoxColumn,
            this.jrcDataGridViewTextBoxColumn,
            this.jrDataGridViewTextBoxColumn,
            this.jaDataGridViewTextBoxColumn,
            this.espesorDataGridViewTextBoxColumn,
            this.relleno1DataGridViewTextBoxColumn,
            this.relleno2DataGridViewTextBoxColumn,
            this.comentarioDataGridViewTextBoxColumn,
            this.geotecnicoDataGridViewTextBoxColumn1,
            this.fechaEsDataGridViewTextBoxColumn,
            this.dipcalDataGridViewTextBoxColumn,
            this.dipdircalDataGridViewTextBoxColumn,
            this.dipsDataGridViewTextBoxColumn,
            this.strikeDataGridViewTextBoxColumn,
            this.stateDataGridViewTextBoxColumn2,
            this.validarDataGridViewTextBoxColumn2});
            this.dgvEstructura.DataSource = this.entEstructuraBindingSource;
            this.dgvEstructura.Location = new System.Drawing.Point(11, 30);
            this.dgvEstructura.Name = "dgvEstructura";
            this.dgvEstructura.RowHeadersVisible = false;
            this.dgvEstructura.Size = new System.Drawing.Size(642, 153);
            this.dgvEstructura.TabIndex = 1;
            this.dgvEstructura.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvEstructura_CellMouseClick);
            // 
            // idDataGridViewTextBoxColumn2
            // 
            this.idDataGridViewTextBoxColumn2.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn2.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn2.Name = "idDataGridViewTextBoxColumn2";
            // 
            // idSondajeDataGridViewTextBoxColumn1
            // 
            this.idSondajeDataGridViewTextBoxColumn1.DataPropertyName = "Id_Sondaje";
            this.idSondajeDataGridViewTextBoxColumn1.HeaderText = "Id_Sondaje";
            this.idSondajeDataGridViewTextBoxColumn1.Name = "idSondajeDataGridViewTextBoxColumn1";
            // 
            // idCorridaDataGridViewTextBoxColumn
            // 
            this.idCorridaDataGridViewTextBoxColumn.DataPropertyName = "Id_Corrida";
            this.idCorridaDataGridViewTextBoxColumn.HeaderText = "Id_Corrida";
            this.idCorridaDataGridViewTextBoxColumn.Name = "idCorridaDataGridViewTextBoxColumn";
            // 
            // idUsuarioDataGridViewTextBoxColumn
            // 
            this.idUsuarioDataGridViewTextBoxColumn.DataPropertyName = "Id_Usuario";
            this.idUsuarioDataGridViewTextBoxColumn.HeaderText = "Id_Usuario";
            this.idUsuarioDataGridViewTextBoxColumn.Name = "idUsuarioDataGridViewTextBoxColumn";
            // 
            // numestructuraDataGridViewTextBoxColumn
            // 
            this.numestructuraDataGridViewTextBoxColumn.DataPropertyName = "Num_estructura";
            this.numestructuraDataGridViewTextBoxColumn.HeaderText = "Num_estructura";
            this.numestructuraDataGridViewTextBoxColumn.Name = "numestructuraDataGridViewTextBoxColumn";
            // 
            // profundidadeDataGridViewTextBoxColumn
            // 
            this.profundidadeDataGridViewTextBoxColumn.DataPropertyName = "Profundidad_e";
            this.profundidadeDataGridViewTextBoxColumn.HeaderText = "Profundidad_e";
            this.profundidadeDataGridViewTextBoxColumn.Name = "profundidadeDataGridViewTextBoxColumn";
            // 
            // tipoeDataGridViewTextBoxColumn
            // 
            this.tipoeDataGridViewTextBoxColumn.DataPropertyName = "Tipo_e";
            this.tipoeDataGridViewTextBoxColumn.HeaderText = "Tipo_e";
            this.tipoeDataGridViewTextBoxColumn.Name = "tipoeDataGridViewTextBoxColumn";
            // 
            // tBDataGridViewTextBoxColumn
            // 
            this.tBDataGridViewTextBoxColumn.DataPropertyName = "T_B";
            this.tBDataGridViewTextBoxColumn.HeaderText = "T_B";
            this.tBDataGridViewTextBoxColumn.Name = "tBDataGridViewTextBoxColumn";
            // 
            // alphaDataGridViewTextBoxColumn
            // 
            this.alphaDataGridViewTextBoxColumn.DataPropertyName = "Alpha";
            this.alphaDataGridViewTextBoxColumn.HeaderText = "Alpha";
            this.alphaDataGridViewTextBoxColumn.Name = "alphaDataGridViewTextBoxColumn";
            // 
            // betaDataGridViewTextBoxColumn
            // 
            this.betaDataGridViewTextBoxColumn.DataPropertyName = "Beta";
            this.betaDataGridViewTextBoxColumn.HeaderText = "Beta";
            this.betaDataGridViewTextBoxColumn.Name = "betaDataGridViewTextBoxColumn";
            // 
            // reftotopDataGridViewTextBoxColumn
            // 
            this.reftotopDataGridViewTextBoxColumn.DataPropertyName = "Ref_to_top";
            this.reftotopDataGridViewTextBoxColumn.HeaderText = "Ref_to_top";
            this.reftotopDataGridViewTextBoxColumn.Name = "reftotopDataGridViewTextBoxColumn";
            // 
            // condfracturaDataGridViewTextBoxColumn
            // 
            this.condfracturaDataGridViewTextBoxColumn.DataPropertyName = "Cond_fractura";
            this.condfracturaDataGridViewTextBoxColumn.HeaderText = "Cond_fractura";
            this.condfracturaDataGridViewTextBoxColumn.Name = "condfracturaDataGridViewTextBoxColumn";
            // 
            // jrcDataGridViewTextBoxColumn
            // 
            this.jrcDataGridViewTextBoxColumn.DataPropertyName = "Jrc";
            this.jrcDataGridViewTextBoxColumn.HeaderText = "Jrc";
            this.jrcDataGridViewTextBoxColumn.Name = "jrcDataGridViewTextBoxColumn";
            // 
            // jrDataGridViewTextBoxColumn
            // 
            this.jrDataGridViewTextBoxColumn.DataPropertyName = "Jr";
            this.jrDataGridViewTextBoxColumn.HeaderText = "Jr";
            this.jrDataGridViewTextBoxColumn.Name = "jrDataGridViewTextBoxColumn";
            // 
            // jaDataGridViewTextBoxColumn
            // 
            this.jaDataGridViewTextBoxColumn.DataPropertyName = "Ja";
            this.jaDataGridViewTextBoxColumn.HeaderText = "Ja";
            this.jaDataGridViewTextBoxColumn.Name = "jaDataGridViewTextBoxColumn";
            // 
            // espesorDataGridViewTextBoxColumn
            // 
            this.espesorDataGridViewTextBoxColumn.DataPropertyName = "Espesor";
            this.espesorDataGridViewTextBoxColumn.HeaderText = "Espesor";
            this.espesorDataGridViewTextBoxColumn.Name = "espesorDataGridViewTextBoxColumn";
            // 
            // relleno1DataGridViewTextBoxColumn
            // 
            this.relleno1DataGridViewTextBoxColumn.DataPropertyName = "Relleno_1";
            this.relleno1DataGridViewTextBoxColumn.HeaderText = "Relleno_1";
            this.relleno1DataGridViewTextBoxColumn.Name = "relleno1DataGridViewTextBoxColumn";
            // 
            // relleno2DataGridViewTextBoxColumn
            // 
            this.relleno2DataGridViewTextBoxColumn.DataPropertyName = "Relleno_2";
            this.relleno2DataGridViewTextBoxColumn.HeaderText = "Relleno_2";
            this.relleno2DataGridViewTextBoxColumn.Name = "relleno2DataGridViewTextBoxColumn";
            // 
            // comentarioDataGridViewTextBoxColumn
            // 
            this.comentarioDataGridViewTextBoxColumn.DataPropertyName = "Comentario";
            this.comentarioDataGridViewTextBoxColumn.HeaderText = "Comentario";
            this.comentarioDataGridViewTextBoxColumn.Name = "comentarioDataGridViewTextBoxColumn";
            // 
            // geotecnicoDataGridViewTextBoxColumn1
            // 
            this.geotecnicoDataGridViewTextBoxColumn1.DataPropertyName = "Geotecnico";
            this.geotecnicoDataGridViewTextBoxColumn1.HeaderText = "Geotecnico";
            this.geotecnicoDataGridViewTextBoxColumn1.Name = "geotecnicoDataGridViewTextBoxColumn1";
            // 
            // fechaEsDataGridViewTextBoxColumn
            // 
            this.fechaEsDataGridViewTextBoxColumn.DataPropertyName = "FechaEs";
            this.fechaEsDataGridViewTextBoxColumn.HeaderText = "FechaEs";
            this.fechaEsDataGridViewTextBoxColumn.Name = "fechaEsDataGridViewTextBoxColumn";
            // 
            // dipcalDataGridViewTextBoxColumn
            // 
            this.dipcalDataGridViewTextBoxColumn.DataPropertyName = "Dip_cal";
            this.dipcalDataGridViewTextBoxColumn.HeaderText = "Dip_cal";
            this.dipcalDataGridViewTextBoxColumn.Name = "dipcalDataGridViewTextBoxColumn";
            // 
            // dipdircalDataGridViewTextBoxColumn
            // 
            this.dipdircalDataGridViewTextBoxColumn.DataPropertyName = "Dip_dir_cal";
            this.dipdircalDataGridViewTextBoxColumn.HeaderText = "Dip_dir_cal";
            this.dipdircalDataGridViewTextBoxColumn.Name = "dipdircalDataGridViewTextBoxColumn";
            // 
            // dipsDataGridViewTextBoxColumn
            // 
            this.dipsDataGridViewTextBoxColumn.DataPropertyName = "Dips";
            this.dipsDataGridViewTextBoxColumn.HeaderText = "Dips";
            this.dipsDataGridViewTextBoxColumn.Name = "dipsDataGridViewTextBoxColumn";
            // 
            // strikeDataGridViewTextBoxColumn
            // 
            this.strikeDataGridViewTextBoxColumn.DataPropertyName = "Strike";
            this.strikeDataGridViewTextBoxColumn.HeaderText = "Strike";
            this.strikeDataGridViewTextBoxColumn.Name = "strikeDataGridViewTextBoxColumn";
            // 
            // stateDataGridViewTextBoxColumn2
            // 
            this.stateDataGridViewTextBoxColumn2.DataPropertyName = "State";
            this.stateDataGridViewTextBoxColumn2.HeaderText = "State";
            this.stateDataGridViewTextBoxColumn2.Name = "stateDataGridViewTextBoxColumn2";
            // 
            // validarDataGridViewTextBoxColumn2
            // 
            this.validarDataGridViewTextBoxColumn2.DataPropertyName = "Validar";
            this.validarDataGridViewTextBoxColumn2.HeaderText = "Validar";
            this.validarDataGridViewTextBoxColumn2.Name = "validarDataGridViewTextBoxColumn2";
            // 
            // entEstructuraBindingSource
            // 
            this.entEstructuraBindingSource.DataSource = typeof(CapaEntidad.EntEstructura);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 24);
            this.label4.TabIndex = 0;
            this.label4.Text = "Estructura";
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.btnEliminarSondaje);
            this.panel7.Controls.Add(this.btnEditarSondaje);
            this.panel7.Controls.Add(this.btnNuevoSondaje);
            this.panel7.Controls.Add(this.dgvSondaje);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Location = new System.Drawing.Point(14, 13);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(666, 193);
            this.panel7.TabIndex = 5;
            // 
            // btnEliminarSondaje
            // 
            this.btnEliminarSondaje.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEliminarSondaje.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnEliminarSondaje.Enabled = false;
            this.btnEliminarSondaje.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnEliminarSondaje.ForeColor = System.Drawing.Color.White;
            this.btnEliminarSondaje.Location = new System.Drawing.Point(544, 4);
            this.btnEliminarSondaje.Name = "btnEliminarSondaje";
            this.btnEliminarSondaje.Size = new System.Drawing.Size(94, 23);
            this.btnEliminarSondaje.TabIndex = 4;
            this.btnEliminarSondaje.Text = "Eliminar sondaje";
            this.btnEliminarSondaje.UseVisualStyleBackColor = false;
            this.btnEliminarSondaje.Click += new System.EventHandler(this.btnEliminarSondaje_Click);
            // 
            // btnEditarSondaje
            // 
            this.btnEditarSondaje.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditarSondaje.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnEditarSondaje.Enabled = false;
            this.btnEditarSondaje.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnEditarSondaje.ForeColor = System.Drawing.Color.White;
            this.btnEditarSondaje.Location = new System.Drawing.Point(454, 4);
            this.btnEditarSondaje.Name = "btnEditarSondaje";
            this.btnEditarSondaje.Size = new System.Drawing.Size(84, 23);
            this.btnEditarSondaje.TabIndex = 3;
            this.btnEditarSondaje.Text = "Editar sondaje";
            this.btnEditarSondaje.UseVisualStyleBackColor = false;
            this.btnEditarSondaje.Click += new System.EventHandler(this.btnEditarSondaje_Click);
            // 
            // btnNuevoSondaje
            // 
            this.btnNuevoSondaje.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNuevoSondaje.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            this.btnNuevoSondaje.ForeColor = System.Drawing.Color.White;
            this.btnNuevoSondaje.Location = new System.Drawing.Point(357, 4);
            this.btnNuevoSondaje.Name = "btnNuevoSondaje";
            this.btnNuevoSondaje.Size = new System.Drawing.Size(91, 23);
            this.btnNuevoSondaje.TabIndex = 2;
            this.btnNuevoSondaje.Text = "Nuevo sondaje";
            this.btnNuevoSondaje.UseVisualStyleBackColor = false;
            this.btnNuevoSondaje.Click += new System.EventHandler(this.btnNuevoSondaje_Click);
            // 
            // dgvSondaje
            // 
            this.dgvSondaje.AllowUserToAddRows = false;
            this.dgvSondaje.AllowUserToDeleteRows = false;
            this.dgvSondaje.AllowUserToResizeColumns = false;
            this.dgvSondaje.AllowUserToResizeRows = false;
            this.dgvSondaje.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSondaje.AutoGenerateColumns = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSondaje.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvSondaje.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvSondaje.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.idUserDataGridViewTextBoxColumn,
            this.clienteDataGridViewTextBoxColumn,
            this.proyectoDataGridViewTextBoxColumn,
            this.geotecnicoDataGridViewTextBoxColumn,
            this.nomsondajeDataGridViewTextBoxColumn,
            this.fechaDataGridViewTextBoxColumn,
            this.profundidadDataGridViewTextBoxColumn,
            this.norteDataGridViewTextBoxColumn,
            this.esteDataGridViewTextBoxColumn,
            this.cotaDataGridViewTextBoxColumn,
            this.maquinaDataGridViewTextBoxColumn,
            this.lineaCoreDataGridViewTextBoxColumn,
            this.dipDataGridViewTextBoxColumn,
            this.azimutDataGridViewTextBoxColumn,
            this.stateDataGridViewTextBoxColumn,
            this.validarDataGridViewTextBoxColumn});
            this.dgvSondaje.DataSource = this.entSondajeBindingSource;
            this.dgvSondaje.Location = new System.Drawing.Point(11, 30);
            this.dgvSondaje.Name = "dgvSondaje";
            this.dgvSondaje.ReadOnly = true;
            this.dgvSondaje.RowHeadersVisible = false;
            this.dgvSondaje.Size = new System.Drawing.Size(642, 153);
            this.dgvSondaje.TabIndex = 1;
            this.dgvSondaje.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSondaje_CellMouseClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // idUserDataGridViewTextBoxColumn
            // 
            this.idUserDataGridViewTextBoxColumn.DataPropertyName = "Id_User";
            this.idUserDataGridViewTextBoxColumn.HeaderText = "Id_User";
            this.idUserDataGridViewTextBoxColumn.Name = "idUserDataGridViewTextBoxColumn";
            this.idUserDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // clienteDataGridViewTextBoxColumn
            // 
            this.clienteDataGridViewTextBoxColumn.DataPropertyName = "Cliente";
            this.clienteDataGridViewTextBoxColumn.HeaderText = "Cliente";
            this.clienteDataGridViewTextBoxColumn.Name = "clienteDataGridViewTextBoxColumn";
            this.clienteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // proyectoDataGridViewTextBoxColumn
            // 
            this.proyectoDataGridViewTextBoxColumn.DataPropertyName = "Proyecto";
            this.proyectoDataGridViewTextBoxColumn.HeaderText = "Proyecto";
            this.proyectoDataGridViewTextBoxColumn.Name = "proyectoDataGridViewTextBoxColumn";
            this.proyectoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // geotecnicoDataGridViewTextBoxColumn
            // 
            this.geotecnicoDataGridViewTextBoxColumn.DataPropertyName = "Geotecnico";
            this.geotecnicoDataGridViewTextBoxColumn.HeaderText = "Geotecnico";
            this.geotecnicoDataGridViewTextBoxColumn.Name = "geotecnicoDataGridViewTextBoxColumn";
            this.geotecnicoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nomsondajeDataGridViewTextBoxColumn
            // 
            this.nomsondajeDataGridViewTextBoxColumn.DataPropertyName = "Nom_sondaje";
            this.nomsondajeDataGridViewTextBoxColumn.HeaderText = "Nom_sondaje";
            this.nomsondajeDataGridViewTextBoxColumn.Name = "nomsondajeDataGridViewTextBoxColumn";
            this.nomsondajeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fechaDataGridViewTextBoxColumn
            // 
            this.fechaDataGridViewTextBoxColumn.DataPropertyName = "Fecha";
            this.fechaDataGridViewTextBoxColumn.HeaderText = "Fecha";
            this.fechaDataGridViewTextBoxColumn.Name = "fechaDataGridViewTextBoxColumn";
            this.fechaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // profundidadDataGridViewTextBoxColumn
            // 
            this.profundidadDataGridViewTextBoxColumn.DataPropertyName = "Profundidad";
            this.profundidadDataGridViewTextBoxColumn.HeaderText = "Profundidad";
            this.profundidadDataGridViewTextBoxColumn.Name = "profundidadDataGridViewTextBoxColumn";
            this.profundidadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // norteDataGridViewTextBoxColumn
            // 
            this.norteDataGridViewTextBoxColumn.DataPropertyName = "Norte";
            this.norteDataGridViewTextBoxColumn.HeaderText = "Norte";
            this.norteDataGridViewTextBoxColumn.Name = "norteDataGridViewTextBoxColumn";
            this.norteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // esteDataGridViewTextBoxColumn
            // 
            this.esteDataGridViewTextBoxColumn.DataPropertyName = "Este";
            this.esteDataGridViewTextBoxColumn.HeaderText = "Este";
            this.esteDataGridViewTextBoxColumn.Name = "esteDataGridViewTextBoxColumn";
            this.esteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cotaDataGridViewTextBoxColumn
            // 
            this.cotaDataGridViewTextBoxColumn.DataPropertyName = "Cota";
            this.cotaDataGridViewTextBoxColumn.HeaderText = "Cota";
            this.cotaDataGridViewTextBoxColumn.Name = "cotaDataGridViewTextBoxColumn";
            this.cotaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // maquinaDataGridViewTextBoxColumn
            // 
            this.maquinaDataGridViewTextBoxColumn.DataPropertyName = "Maquina";
            this.maquinaDataGridViewTextBoxColumn.HeaderText = "Maquina";
            this.maquinaDataGridViewTextBoxColumn.Name = "maquinaDataGridViewTextBoxColumn";
            this.maquinaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lineaCoreDataGridViewTextBoxColumn
            // 
            this.lineaCoreDataGridViewTextBoxColumn.DataPropertyName = "Linea_Core";
            this.lineaCoreDataGridViewTextBoxColumn.HeaderText = "Linea_Core";
            this.lineaCoreDataGridViewTextBoxColumn.Name = "lineaCoreDataGridViewTextBoxColumn";
            this.lineaCoreDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dipDataGridViewTextBoxColumn
            // 
            this.dipDataGridViewTextBoxColumn.DataPropertyName = "Dip";
            this.dipDataGridViewTextBoxColumn.HeaderText = "Dip";
            this.dipDataGridViewTextBoxColumn.Name = "dipDataGridViewTextBoxColumn";
            this.dipDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // azimutDataGridViewTextBoxColumn
            // 
            this.azimutDataGridViewTextBoxColumn.DataPropertyName = "Azimut";
            this.azimutDataGridViewTextBoxColumn.HeaderText = "Azimut";
            this.azimutDataGridViewTextBoxColumn.Name = "azimutDataGridViewTextBoxColumn";
            this.azimutDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // stateDataGridViewTextBoxColumn
            // 
            this.stateDataGridViewTextBoxColumn.DataPropertyName = "State";
            this.stateDataGridViewTextBoxColumn.HeaderText = "State";
            this.stateDataGridViewTextBoxColumn.Name = "stateDataGridViewTextBoxColumn";
            this.stateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // validarDataGridViewTextBoxColumn
            // 
            this.validarDataGridViewTextBoxColumn.DataPropertyName = "Validar";
            this.validarDataGridViewTextBoxColumn.HeaderText = "Validar";
            this.validarDataGridViewTextBoxColumn.Name = "validarDataGridViewTextBoxColumn";
            this.validarDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // entSondajeBindingSource
            // 
            this.entSondajeBindingSource.DataSource = typeof(CapaEntidad.EntSondaje);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "Sondaje";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Location = new System.Drawing.Point(0, 39);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(695, 215);
            this.panel2.TabIndex = 2;
            // 
            // VistaLogueo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 701);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Name = "VistaLogueo";
            this.Text = "VistaLogueo";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCerrar)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCorrida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entCorridaBindingSource)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstructura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entEstructuraBindingSource)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSondaje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entSondajeBindingSource)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnEliminarSondaje;
        private System.Windows.Forms.Button btnEditarSondaje;
        private System.Windows.Forms.Button btnNuevoSondaje;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnEliminarCorrida;
        private System.Windows.Forms.Button btnEditarCorrida;
        private System.Windows.Forms.Button btnNuevaCorrida;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnEliminarEstructura;
        private System.Windows.Forms.Button btnEditarEstructura;
        private System.Windows.Forms.Button btnNuevaEstructura;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pbCerrar;
        public System.Windows.Forms.DataGridView dgvSondaje;
        public System.Windows.Forms.DataGridView dgvCorrida;
        public System.Windows.Forms.DataGridView dgvEstructura;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dipDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idUserDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSondajeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn desdeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hastaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn perforacionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn recuperacionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn validarDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource entCorridaBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSondajeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idCorridaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idUsuarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numestructuraDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn profundidadeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tBDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn alphaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn betaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reftotopDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn condfracturaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jrcDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jrDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn espesorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn relleno1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn relleno2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn comentarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn geotecnicoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaEsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dipcalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dipdircalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dipsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn strikeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stateDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn validarDataGridViewTextBoxColumn2;
        private System.Windows.Forms.BindingSource entEstructuraBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idUserDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clienteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn proyectoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn geotecnicoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomsondajeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn profundidadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn norteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn esteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cotaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maquinaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lineaCoreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dipDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn azimutDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn validarDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource entSondajeBindingSource;
    }
}