#              C:\Python37\python.exe .\planar.py

import matplotlib.pyplot as plt
import mplstereonet
from matplotlib.widgets import CheckButtons

from lectura_datos import leer
from colores import colors,colors2,tipo_estructura

# ARREGLO
arr = []

####################################################
# LEER DATA ARCHIVO #
arr = leer("estructuras")
# LEER DATA ARCHIVO #
####################################################


fig = plt.figure("Diagráma de Planos")
ax = fig.add_subplot(projection='stereonet')
#fig.suptitle("Diagráma de Planos")

#########################################################################
# Plot planes...            #############################################
for i,dat in enumerate(arr):
    
    #   verifica si el tipo de estructura se esta agregando a la leyenda
    if colors2[dat[2]][1]:
        ax.plane(dat[0],dat[1],c=colors2[dat[2]][0])
    else:
        ax.plane(dat[0],dat[1],c=colors2[dat[2]][0],label=dat[2])
        colors2[dat[2]][1] = True
    #   version anterior
    #   ax.plane(dat[0],dat[1],c=colors2[dat[2]][0],label=dat[2])
# Plot planes...            ############################################
########################################################################

ax.legend(loc="upper right",bbox_to_anchor=(1.3,0.95))

########################################################################
#                           CHECKBOX Y EVENTO
#       
#       dibujar un rectangulo entre 0,1  #  lefft, bottom, width, height
rax = plt.axes([0.05, 0.05, 0.15, 0.15])
labels = ["grilla"]
visibility = [True]
check = CheckButtons(rax, labels, visibility)

ax.grid(visibility[0])

def func(label):
    index = labels.index(label)
    #  ACTIVA O DESACTIVA GRID
    visibility[index] = not visibility[index]
    ax.grid(visibility[index])
    plt.draw()
    
check.on_clicked(func)
#                           CHECKBOX Y EVENTO
########################################################################

ax.set_title("Norte",y=1.08)

plt.show()
