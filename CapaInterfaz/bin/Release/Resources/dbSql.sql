CREATE TABLE "Usuario" (
	"Id"	TEXT NOT NULL,
	"Nombre"	TEXT NOT NULL,
	"Apellido"	TEXT NOT NULL,
	"DNI"	TEXT NOT NULL,
	"Cargo"	TEXT NOT NULL,
	"Iniciales"	TEXT NOT NULL,
	"Contrasenia"	TEXT NOT NULL,
	"Rol"	TEXT NOT NULL,
	PRIMARY KEY("Id")
);
CREATE TABLE "Sondaje" (
	"Id"	TEXT NOT NULL,
	"Id_User"	TEXT NOT NULL,
	"Cliente"	TEXT NOT NULL,
	"Proyecto"	TEXT NOT NULL,
	"Geotecnico"	TEXT NOT NULL,
	"Nom_Sondaje"	TEXT NOT NULL,
	"Fecha"	TEXT,
	"Profundidad"	REAL,
	"Norte"	REAL NOT NULL,
	"Este"	REAL NOT NULL,
	"Cota"	REAL NOT NULL,
	"Maquina"	TEXT,
	"Linea_Core"	TEXT NOT NULL,
	"Dip"	REAL,
	"Azimut"	REAL,
	"State" TEXT,
	PRIMARY KEY("Id")
);
CREATE TABLE "Estructura" (
	"Id"	TEXT NOT NULL,
	"Id_Sondaje"	TEXT NOT NULL,
	"Id_Corrida"	TEXT NOT NULL,
	"Id_Usuario"	TEXT NOT NULL,
	"Num_Estructura"	TEXT NOT NULL,
	"Profundidad_e"	REAL NOT NULL,
	"Tipo_e"	TEXT NOT NULL,
	"T_B"	REAL NOT NULL,
	"Alpha"	REAL NOT NULL,
	"Beta"	REAL NOT NULL,
	"Ref_to_top"	REAL,
	"Cond_fractura"	REAL,
	"Jrc"	REAL,
	"Jr"	REAL,
	"Ja"	REAL,
	"Espesor"	REAL,
	"Relleno_1"	TEXT,
	"Relleno_2"	TEXT,
	"Comentario"	TEXT,
	"Geotecnico"	TEXT NOT NULL,
	"FechaEs"	TEXT,
	"Dip_cal"	REAL,
	"Dip_dir_cal"	REAL,
	"Dips"	REAL,
	"Strike"	REAL,
	"State" TEXT,
	PRIMARY KEY("Id")
);
CREATE TABLE "Corrida" (
	"Id"	TEXT NOT NULL,
	"IdUser"	TEXT NOT NULL,
	"Id_Sondaje"	TEXT NOT NULL,
	"Desde"	REAL NOT NULL,
	"Hasta"	REAL NOT NULL,
	"Perforacion" REAL NOT NULL,
	"Recuperacion" REAL not NULL,
	"State" TEXT,
	PRIMARY KEY("Id")
);

CREATE TABLE "Auditoria" (
	"Id"	TEXT NOT NULL,
	"Id_User"	TEXT NOT NULL,
	"Nombre"	TEXT NOT NULL,
	"Apellido"	TEXT NOT NULL,
	"Elemento"	TEXT NOT NULL,
	"Accion"	TEXT NOT NULL,
	"Fecha"	TEXT NOT NULL,
	PRIMARY KEY("Id")
);

INSERT INTO "Usuario" ("Id","Nombre","Apellido","DNI","Cargo","Iniciales","Contrasenia","Rol") VALUES ('1','Leonars Jheferson','Villanueva Mendoza','71042037','','LJVM','123456','Geotecnico');
INSERT INTO "Usuario" ("Id","Nombre","Apellido","DNI","Cargo","Iniciales","Contrasenia","Rol") VALUES ('2','Juan','Castillo','71042038','','JC','654321','Geotecnico');
INSERT INTO "Usuario" ("Id","Nombre","Apellido","DNI","Cargo","Iniciales","Contrasenia","Rol") VALUES ('3','Pedro','Bustamante','71042039','','PB','789456','Supervisor');