import numpy as np

colors = [
["grey","Jn"],
["green","Vn"],
["blue","Vnll"],
["red","F-10"],
["skyblue","F+10"],
["cyan","Fl"],
["peru","Sr"],
["darkred","Js"],
["green","Fo"],
["lightgreen","Bd"],
["yellowgreen","Cn"],
["darkgreen","Dk"],
["yellow","Sc"],
["magenta","N"],
["pink","Uc"],
["violet","Se"],
["oragne","Q"],
["brown","R"],
["darkblue","S"],
["black","T"],
["beige","U"],
["gold","V"]
]

colors2 = {
"Jn":["grey",False],
"Vn":["green",False],
"Vnll":["blue",False],
"F-10":["red",False],
"F+10":["skyblue",False],
"Fl":["cyan",False],
"Sr":["peru",False],
"Js":["darkred",False],
"Fo":["green",False],
"Bd":["lightgreen",False],
"Cn":["yellowgreen",False],
"Dk":["darkgreen",False],
"Sc":["yellow",False],
"Uc":["pink",False],
"Se":["violet",False]
}

tipo_estructura=["Jn","Vn","Vnll","F-10","F+10","Fl","Sr","Js","Fo","Bd","Cn","Dk","Sc","Uc","Se"]
