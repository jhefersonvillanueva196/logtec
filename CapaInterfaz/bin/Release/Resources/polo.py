#              C:\Python37\python.exe .\rose.py

import numpy as np
import mplstereonet
import matplotlib.pyplot as plt

from matplotlib.widgets import CheckButtons

from lectura_datos import leer
#%matplotlib inline

strikes = []
dips = []

arr = leer("estructuras")

for a in arr:
  strikes.append(a[0])
  dips.append(a[1])


#fig = plt.figure("algo",figsize=(5,5))
fig = plt.figure("Concentración de Polos")

ax = fig.add_subplot( projection='stereonet')

ax.pole(strikes, dips, c='k', marker='.', markersize=8)
ax.density_contourf(strikes, dips, measurement='poles', cmap='Blues')
ax.set_title('Norte', y=1.08, fontsize=15)


########################################################################
#                           CHECKBOX Y EVENTO
#       
#       dibujar un rectangulo entre 0,1  #  lefft, bottom, width, height
rax = plt.axes([0.05, 0.05, 0.15, 0.15])
labels = ["grilla"]
visibility = [False]
check = CheckButtons(rax, labels, visibility)

ax.grid(visibility[0])

def func(label):
    index = labels.index(label)
    #  ACTIVA O DESACTIVA GRID
    visibility[index] = not visibility[index]
    ax.grid(visibility[index])
    plt.draw()
    
check.on_clicked(func)
#                           CHECKBOX Y EVENTO
########################################################################




plt.show()