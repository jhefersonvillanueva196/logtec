#              C:\Python37\python.exe .\planar.py

from pathlib import Path
from tkinter import Y
import matplotlib.pyplot as plt
import mplstereonet
import os
from matplotlib.widgets import CheckButtons

from lectura_datos import leer
from colores import colors,colors2,tipo_estructura

archivo = str()
posicion = int()
suma = int()
sondaje = str()

for i in range(4):
    
  archivo='estructuras' + str(i+1) + '.txt'
    
  band = os.path.exists(archivo)
  
  if(band):
    suma += 1

if suma == 1:
  posicion = 111
if suma == 2:
  posicion = 121
if suma == 3:
  posicion = 221
if suma == 4:
  posicion = 221

fig = plt.figure("Diagráma de Planos", figsize=(12,8))

for i in range(suma):

  sondaje = 'Sondaje '+ str(i + 1)
  
  # ARREGLO
  arr = [] 
    
  ####################################################
  # LEER DATA ARCHIVO #
  archivo='estructuras' + str(i+1)
  arr = leer(archivo)
  # LEER DATA ARCHIVO #
  ####################################################

  ax = plt.subplot(posicion, projection='stereonet')

    #########################################################################
    # Plot planes...            #############################################
  for i,dat in enumerate(arr):
        
        #   verifica si el tipo de estructura se esta agregando a la leyenda
    if colors2[dat[2]][1]:
      ax.plane(dat[0],dat[1],c=colors2[dat[2]][0])
    else:
      ax.plane(dat[0],dat[1],c=colors2[dat[2]][0],label=dat[2])
      colors2[dat[2]][1] = True
        #   version anterior
        #   ax.plane(dat[0],dat[1],c=colors2[dat[2]][0],label=dat[2])
    # Plot planes...            ############################################
    ########################################################################

  for i,dat in enumerate(arr):
    colors2[dat[2]][1] = False
  
  ax.legend(loc=0,bbox_to_anchor=(1.3,0.95))

  ax.grid()

  ax.set_title(sondaje, y=1.08)
    
  posicion += 1

fig.tight_layout()


plt.show()
