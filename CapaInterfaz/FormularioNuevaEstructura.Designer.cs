﻿
namespace CapaInterfaz
{
    partial class FormularioNuevaEstructura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormularioNuevaEstructura));
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbValidar = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cboJr = new System.Windows.Forms.ComboBox();
            this.cboJa = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.pbRelleno2 = new System.Windows.Forms.PictureBox();
            this.pbRelleno1 = new System.Windows.Forms.PictureBox();
            this.pbJa = new System.Windows.Forms.PictureBox();
            this.pbJr = new System.Windows.Forms.PictureBox();
            this.pbJrc = new System.Windows.Forms.PictureBox();
            this.pbCondicionFractura = new System.Windows.Forms.PictureBox();
            this.pbBeta = new System.Windows.Forms.PictureBox();
            this.pbAlpha = new System.Windows.Forms.PictureBox();
            this.cboRelleno2 = new System.Windows.Forms.ComboBox();
            this.cboRelleno1 = new System.Windows.Forms.ComboBox();
            this.cboJrc = new System.Windows.Forms.ComboBox();
            this.cboCondicionFractura = new System.Windows.Forms.ComboBox();
            this.cboTipoEstructura = new System.Windows.Forms.ComboBox();
            this.txtDesde = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtComentario = new System.Windows.Forms.TextBox();
            this.txtRefToTop = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.btnCrear = new System.Windows.Forms.Button();
            this.txtEspesor = new System.Windows.Forms.TextBox();
            this.txtBeta = new System.Windows.Forms.TextBox();
            this.txtAlpha = new System.Windows.Forms.TextBox();
            this.txtTB = new System.Windows.Forms.TextBox();
            this.txtProfundidadEstructura = new System.Windows.Forms.TextBox();
            this.txtNumeroEstructura = new System.Windows.Forms.TextBox();
            this.txtHasta = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tips = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRelleno2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRelleno1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJrc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCondicionFractura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBeta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAlpha)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel1.Controls.Add(this.cbValidar);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.cboJr);
            this.panel1.Controls.Add(this.cboJa);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.pbRelleno2);
            this.panel1.Controls.Add(this.pbRelleno1);
            this.panel1.Controls.Add(this.pbJa);
            this.panel1.Controls.Add(this.pbJr);
            this.panel1.Controls.Add(this.pbJrc);
            this.panel1.Controls.Add(this.pbCondicionFractura);
            this.panel1.Controls.Add(this.pbBeta);
            this.panel1.Controls.Add(this.pbAlpha);
            this.panel1.Controls.Add(this.cboRelleno2);
            this.panel1.Controls.Add(this.cboRelleno1);
            this.panel1.Controls.Add(this.cboJrc);
            this.panel1.Controls.Add(this.cboCondicionFractura);
            this.panel1.Controls.Add(this.cboTipoEstructura);
            this.panel1.Controls.Add(this.txtDesde);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.txtComentario);
            this.panel1.Controls.Add(this.txtRefToTop);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.btnCerrar);
            this.panel1.Controls.Add(this.btnCrear);
            this.panel1.Controls.Add(this.txtEspesor);
            this.panel1.Controls.Add(this.txtBeta);
            this.panel1.Controls.Add(this.txtAlpha);
            this.panel1.Controls.Add(this.txtTB);
            this.panel1.Controls.Add(this.txtProfundidadEstructura);
            this.panel1.Controls.Add(this.txtNumeroEstructura);
            this.panel1.Controls.Add(this.txtHasta);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cbValidar
            // 
            resources.ApplyResources(this.cbValidar, "cbValidar");
            this.cbValidar.Name = "cbValidar";
            this.cbValidar.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Name = "label23";
            // 
            // cboJr
            // 
            this.cboJr.DropDownHeight = 90;
            this.cboJr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboJr.FormattingEnabled = true;
            resources.ApplyResources(this.cboJr, "cboJr");
            this.cboJr.Items.AddRange(new object[] {
            resources.GetString("cboJr.Items"),
            resources.GetString("cboJr.Items1"),
            resources.GetString("cboJr.Items2"),
            resources.GetString("cboJr.Items3"),
            resources.GetString("cboJr.Items4"),
            resources.GetString("cboJr.Items5"),
            resources.GetString("cboJr.Items6"),
            resources.GetString("cboJr.Items7")});
            this.cboJr.Name = "cboJr";
            // 
            // cboJa
            // 
            this.cboJa.DropDownHeight = 90;
            this.cboJa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboJa.FormattingEnabled = true;
            resources.ApplyResources(this.cboJa, "cboJa");
            this.cboJa.Items.AddRange(new object[] {
            resources.GetString("cboJa.Items"),
            resources.GetString("cboJa.Items1"),
            resources.GetString("cboJa.Items2"),
            resources.GetString("cboJa.Items3"),
            resources.GetString("cboJa.Items4"),
            resources.GetString("cboJa.Items5"),
            resources.GetString("cboJa.Items6"),
            resources.GetString("cboJa.Items7"),
            resources.GetString("cboJa.Items8"),
            resources.GetString("cboJa.Items9"),
            resources.GetString("cboJa.Items10"),
            resources.GetString("cboJa.Items11"),
            resources.GetString("cboJa.Items12"),
            resources.GetString("cboJa.Items13"),
            resources.GetString("cboJa.Items14"),
            resources.GetString("cboJa.Items15"),
            resources.GetString("cboJa.Items16"),
            resources.GetString("cboJa.Items17"),
            resources.GetString("cboJa.Items18"),
            resources.GetString("cboJa.Items19"),
            resources.GetString("cboJa.Items20"),
            resources.GetString("cboJa.Items21")});
            this.cboJa.Name = "cboJa";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Name = "label22";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Name = "label21";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Name = "label20";
            // 
            // pbRelleno2
            // 
            this.pbRelleno2.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            resources.ApplyResources(this.pbRelleno2, "pbRelleno2");
            this.pbRelleno2.Name = "pbRelleno2";
            this.pbRelleno2.TabStop = false;
            this.pbRelleno2.Click += new System.EventHandler(this.pbRelleno2_Click);
            // 
            // pbRelleno1
            // 
            this.pbRelleno1.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            resources.ApplyResources(this.pbRelleno1, "pbRelleno1");
            this.pbRelleno1.Name = "pbRelleno1";
            this.pbRelleno1.TabStop = false;
            this.pbRelleno1.Click += new System.EventHandler(this.pbRelleno1_Click);
            // 
            // pbJa
            // 
            this.pbJa.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            resources.ApplyResources(this.pbJa, "pbJa");
            this.pbJa.Name = "pbJa";
            this.pbJa.TabStop = false;
            this.pbJa.Click += new System.EventHandler(this.pbJa_Click);
            // 
            // pbJr
            // 
            this.pbJr.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            resources.ApplyResources(this.pbJr, "pbJr");
            this.pbJr.Name = "pbJr";
            this.pbJr.TabStop = false;
            this.pbJr.Click += new System.EventHandler(this.pbJr_Click);
            // 
            // pbJrc
            // 
            this.pbJrc.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            resources.ApplyResources(this.pbJrc, "pbJrc");
            this.pbJrc.Name = "pbJrc";
            this.pbJrc.TabStop = false;
            this.pbJrc.Click += new System.EventHandler(this.pbJrc_Click);
            // 
            // pbCondicionFractura
            // 
            this.pbCondicionFractura.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            resources.ApplyResources(this.pbCondicionFractura, "pbCondicionFractura");
            this.pbCondicionFractura.Name = "pbCondicionFractura";
            this.pbCondicionFractura.TabStop = false;
            this.pbCondicionFractura.Click += new System.EventHandler(this.pbCondicionFractura_Click);
            // 
            // pbBeta
            // 
            this.pbBeta.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            resources.ApplyResources(this.pbBeta, "pbBeta");
            this.pbBeta.Name = "pbBeta";
            this.pbBeta.TabStop = false;
            this.pbBeta.Click += new System.EventHandler(this.pbBeta_Click);
            // 
            // pbAlpha
            // 
            this.pbAlpha.Image = global::CapaInterfaz.Properties.Resources.pregunta;
            resources.ApplyResources(this.pbAlpha, "pbAlpha");
            this.pbAlpha.Name = "pbAlpha";
            this.pbAlpha.TabStop = false;
            this.pbAlpha.Click += new System.EventHandler(this.pbAlpha_Click);
            // 
            // cboRelleno2
            // 
            this.cboRelleno2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRelleno2.FormattingEnabled = true;
            this.cboRelleno2.Items.AddRange(new object[] {
            resources.GetString("cboRelleno2.Items"),
            resources.GetString("cboRelleno2.Items1"),
            resources.GetString("cboRelleno2.Items2"),
            resources.GetString("cboRelleno2.Items3"),
            resources.GetString("cboRelleno2.Items4"),
            resources.GetString("cboRelleno2.Items5"),
            resources.GetString("cboRelleno2.Items6"),
            resources.GetString("cboRelleno2.Items7"),
            resources.GetString("cboRelleno2.Items8"),
            resources.GetString("cboRelleno2.Items9"),
            resources.GetString("cboRelleno2.Items10"),
            resources.GetString("cboRelleno2.Items11"),
            resources.GetString("cboRelleno2.Items12"),
            resources.GetString("cboRelleno2.Items13"),
            resources.GetString("cboRelleno2.Items14"),
            resources.GetString("cboRelleno2.Items15"),
            resources.GetString("cboRelleno2.Items16"),
            resources.GetString("cboRelleno2.Items17")});
            resources.ApplyResources(this.cboRelleno2, "cboRelleno2");
            this.cboRelleno2.Name = "cboRelleno2";
            // 
            // cboRelleno1
            // 
            this.cboRelleno1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRelleno1.FormattingEnabled = true;
            this.cboRelleno1.Items.AddRange(new object[] {
            resources.GetString("cboRelleno1.Items"),
            resources.GetString("cboRelleno1.Items1"),
            resources.GetString("cboRelleno1.Items2"),
            resources.GetString("cboRelleno1.Items3"),
            resources.GetString("cboRelleno1.Items4"),
            resources.GetString("cboRelleno1.Items5"),
            resources.GetString("cboRelleno1.Items6"),
            resources.GetString("cboRelleno1.Items7"),
            resources.GetString("cboRelleno1.Items8"),
            resources.GetString("cboRelleno1.Items9"),
            resources.GetString("cboRelleno1.Items10"),
            resources.GetString("cboRelleno1.Items11"),
            resources.GetString("cboRelleno1.Items12"),
            resources.GetString("cboRelleno1.Items13"),
            resources.GetString("cboRelleno1.Items14"),
            resources.GetString("cboRelleno1.Items15"),
            resources.GetString("cboRelleno1.Items16"),
            resources.GetString("cboRelleno1.Items17")});
            resources.ApplyResources(this.cboRelleno1, "cboRelleno1");
            this.cboRelleno1.Name = "cboRelleno1";
            // 
            // cboJrc
            // 
            this.cboJrc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboJrc.FormattingEnabled = true;
            this.cboJrc.Items.AddRange(new object[] {
            resources.GetString("cboJrc.Items"),
            resources.GetString("cboJrc.Items1"),
            resources.GetString("cboJrc.Items2"),
            resources.GetString("cboJrc.Items3"),
            resources.GetString("cboJrc.Items4"),
            resources.GetString("cboJrc.Items5"),
            resources.GetString("cboJrc.Items6"),
            resources.GetString("cboJrc.Items7"),
            resources.GetString("cboJrc.Items8"),
            resources.GetString("cboJrc.Items9"),
            resources.GetString("cboJrc.Items10")});
            resources.ApplyResources(this.cboJrc, "cboJrc");
            this.cboJrc.Name = "cboJrc";
            // 
            // cboCondicionFractura
            // 
            this.cboCondicionFractura.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCondicionFractura.FormattingEnabled = true;
            this.cboCondicionFractura.Items.AddRange(new object[] {
            resources.GetString("cboCondicionFractura.Items"),
            resources.GetString("cboCondicionFractura.Items1"),
            resources.GetString("cboCondicionFractura.Items2"),
            resources.GetString("cboCondicionFractura.Items3"),
            resources.GetString("cboCondicionFractura.Items4"),
            resources.GetString("cboCondicionFractura.Items5"),
            resources.GetString("cboCondicionFractura.Items6"),
            resources.GetString("cboCondicionFractura.Items7"),
            resources.GetString("cboCondicionFractura.Items8"),
            resources.GetString("cboCondicionFractura.Items9"),
            resources.GetString("cboCondicionFractura.Items10"),
            resources.GetString("cboCondicionFractura.Items11"),
            resources.GetString("cboCondicionFractura.Items12"),
            resources.GetString("cboCondicionFractura.Items13")});
            resources.ApplyResources(this.cboCondicionFractura, "cboCondicionFractura");
            this.cboCondicionFractura.Name = "cboCondicionFractura";
            // 
            // cboTipoEstructura
            // 
            this.cboTipoEstructura.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoEstructura.FormattingEnabled = true;
            this.cboTipoEstructura.Items.AddRange(new object[] {
            resources.GetString("cboTipoEstructura.Items"),
            resources.GetString("cboTipoEstructura.Items1"),
            resources.GetString("cboTipoEstructura.Items2"),
            resources.GetString("cboTipoEstructura.Items3"),
            resources.GetString("cboTipoEstructura.Items4"),
            resources.GetString("cboTipoEstructura.Items5"),
            resources.GetString("cboTipoEstructura.Items6"),
            resources.GetString("cboTipoEstructura.Items7"),
            resources.GetString("cboTipoEstructura.Items8"),
            resources.GetString("cboTipoEstructura.Items9"),
            resources.GetString("cboTipoEstructura.Items10"),
            resources.GetString("cboTipoEstructura.Items11"),
            resources.GetString("cboTipoEstructura.Items12"),
            resources.GetString("cboTipoEstructura.Items13"),
            resources.GetString("cboTipoEstructura.Items14"),
            resources.GetString("cboTipoEstructura.Items15"),
            resources.GetString("cboTipoEstructura.Items16")});
            resources.ApplyResources(this.cboTipoEstructura, "cboTipoEstructura");
            this.cboTipoEstructura.Name = "cboTipoEstructura";
            this.cboTipoEstructura.SelectedValueChanged += new System.EventHandler(this.cboTipoEstructura_SelectedValueChanged);
            // 
            // txtDesde
            // 
            resources.ApplyResources(this.txtDesde, "txtDesde");
            this.txtDesde.Name = "txtDesde";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // txtComentario
            // 
            resources.ApplyResources(this.txtComentario, "txtComentario");
            this.txtComentario.Name = "txtComentario";
            // 
            // txtRefToTop
            // 
            resources.ApplyResources(this.txtRefToTop, "txtRefToTop");
            this.txtRefToTop.Name = "txtRefToTop";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            resources.ApplyResources(this.btnCerrar, "btnCerrar");
            this.btnCerrar.ForeColor = System.Drawing.Color.White;
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // btnCrear
            // 
            this.btnCrear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(146)))), ((int)(((byte)(190)))));
            resources.ApplyResources(this.btnCrear, "btnCrear");
            this.btnCrear.ForeColor = System.Drawing.Color.White;
            this.btnCrear.Name = "btnCrear";
            this.btnCrear.UseVisualStyleBackColor = false;
            this.btnCrear.Click += new System.EventHandler(this.btnCrear_Click);
            // 
            // txtEspesor
            // 
            resources.ApplyResources(this.txtEspesor, "txtEspesor");
            this.txtEspesor.Name = "txtEspesor";
            this.txtEspesor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEspesor_KeyPress);
            this.txtEspesor.Leave += new System.EventHandler(this.txtEspesor_Leave);
            // 
            // txtBeta
            // 
            resources.ApplyResources(this.txtBeta, "txtBeta");
            this.txtBeta.Name = "txtBeta";
            this.txtBeta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBeta_KeyPress);
            this.txtBeta.Leave += new System.EventHandler(this.txtBeta_Leave);
            // 
            // txtAlpha
            // 
            resources.ApplyResources(this.txtAlpha, "txtAlpha");
            this.txtAlpha.Name = "txtAlpha";
            this.txtAlpha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAlpha_KeyPress);
            this.txtAlpha.Leave += new System.EventHandler(this.txtAlpha_Leave);
            // 
            // txtTB
            // 
            resources.ApplyResources(this.txtTB, "txtTB");
            this.txtTB.Name = "txtTB";
            // 
            // txtProfundidadEstructura
            // 
            resources.ApplyResources(this.txtProfundidadEstructura, "txtProfundidadEstructura");
            this.txtProfundidadEstructura.Name = "txtProfundidadEstructura";
            this.txtProfundidadEstructura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtProfundidadEstructura_KeyPress);
            this.txtProfundidadEstructura.Leave += new System.EventHandler(this.txtProfundidadEstructura_Leave);
            // 
            // txtNumeroEstructura
            // 
            resources.ApplyResources(this.txtNumeroEstructura, "txtNumeroEstructura");
            this.txtNumeroEstructura.Name = "txtNumeroEstructura";
            // 
            // txtHasta
            // 
            resources.ApplyResources(this.txtHasta, "txtHasta");
            this.txtHasta.Name = "txtHasta";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // FormularioNuevaEstructura
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormularioNuevaEstructura";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRelleno2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRelleno1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbJrc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCondicionFractura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBeta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAlpha)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Button btnCrear;
        private System.Windows.Forms.TextBox txtEspesor;
        private System.Windows.Forms.TextBox txtBeta;
        private System.Windows.Forms.TextBox txtAlpha;
        private System.Windows.Forms.TextBox txtTB;
        private System.Windows.Forms.TextBox txtProfundidadEstructura;
        private System.Windows.Forms.TextBox txtNumeroEstructura;
        private System.Windows.Forms.TextBox txtHasta;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtComentario;
        private System.Windows.Forms.TextBox txtRefToTop;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtDesde;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cboRelleno2;
        private System.Windows.Forms.ComboBox cboRelleno1;
        private System.Windows.Forms.ComboBox cboJrc;
        private System.Windows.Forms.ComboBox cboCondicionFractura;
        private System.Windows.Forms.ComboBox cboTipoEstructura;
        private System.Windows.Forms.PictureBox pbRelleno2;
        private System.Windows.Forms.PictureBox pbRelleno1;
        private System.Windows.Forms.PictureBox pbJa;
        private System.Windows.Forms.PictureBox pbJr;
        private System.Windows.Forms.PictureBox pbJrc;
        private System.Windows.Forms.PictureBox pbCondicionFractura;
        private System.Windows.Forms.PictureBox pbBeta;
        private System.Windows.Forms.PictureBox pbAlpha;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cboJa;
        private System.Windows.Forms.ComboBox cboJr;
        private System.Windows.Forms.ToolTip tips;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.CheckBox cbValidar;
    }
}