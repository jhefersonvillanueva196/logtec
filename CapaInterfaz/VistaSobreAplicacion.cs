﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaInterfaz
{
    public partial class VistaSobreAplicacion : Form
    {
        public VistaSobreAplicacion()
        {
            InitializeComponent();
            txtVersion.Text = Application.ProductName + Application.ProductVersion;
        }

        private void Link_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://geotekhperu.com/");
        }
    }
}
