import numpy as np
import mplstereonet
import matplotlib.pyplot as plt
import os

from lectura_datos import leer


# ARREGLO
strikes = []
dips = []

archivo = str()
posicion = int()
suma = int()
sondaje = str()

for i in range(4):
    
  archivo='estructuras' + str(i+1) + '.txt'
    
  band = os.path.exists(archivo)
    
  if(band):
    suma += 1

if suma == 1:
  posicion = 111
if suma == 2:
  posicion = 121
if suma == 3:
  posicion = 221
if suma == 4:
  posicion = 221

fig = plt.figure("Diagráma de Rosetas", figsize=(12,8))

for i in range(suma):
    
  arr = []
  sondaje = 'Sondaje '+ str(i + 1)
  archivo = 'estructuras'+ str(i + 1)
  arr = leer(archivo)
  
  for a in arr:
    strikes.append(a[0])
    dips.append(a[1])
  
  bin_edges = np.arange(-5, 366, 10)
  number_of_strikes, bin_edges = np.histogram(strikes, bin_edges)

  number_of_strikes[0] += number_of_strikes[-1]

  half = np.sum(np.split(number_of_strikes[:-1], 2), 0)
  two_halves = np.concatenate([half, half])

  ax = fig.add_subplot(posicion, projection='polar')

  ax.bar(np.deg2rad(np.arange(0, 360, 10)), two_halves, 
         width=np.deg2rad(10), bottom=0.0, color='.8', edgecolor='k')
  ax.set_theta_zero_location('N')
  ax.set_theta_direction(-1)
  ax.set_thetagrids(np.arange(0, 360, 10), labels=np.arange(0, 360, 10))
  ax.set_rgrids(np.arange(1, two_halves.max() + 1, 2), angle=0, weight= 'black')
  ax.set_title(sondaje, y=1.10)
  
  posicion += 1
  strikes.clear()
  dips.clear()
  
fig.tight_layout()

plt.show()