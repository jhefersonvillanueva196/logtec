#              C:\Python37\python.exe .\rose.py

from asyncio.windows_events import NULL
import numpy as np
import mplstereonet
import matplotlib.pyplot as plt
import os
from matplotlib.widgets import CheckButtons

from lectura_datos import leer

strikes = []
dips = []

archivo = str()
posicion = int()
suma = int()
sondaje = str()

for i in range(4):
    
  archivo='estructuras' + str(i+1) + '.txt'
  
  band = os.path.exists(archivo)
    
  if(band):
    suma += 1

if suma == 1:
  posicion = 111
if suma == 2:
  posicion = 121
if suma == 3:
  posicion = 221
if suma == 4:
  posicion = 221


fig = plt.figure("Concentración de Polos", figsize=(12,8))


for i in range(suma):

  arr = []
  sondaje = 'Sondaje '+ str(i + 1)
  archivo = 'estructuras'+ str(i + 1)
  arr = leer(archivo)

  for a in arr:
    strikes.append(a[0])
    dips.append(a[1])
  
  ax = plt.subplot(posicion, projection='stereonet')

  ax.pole(strikes, dips, c='k', marker='.', markersize=8)
  ax.density_contourf(strikes, dips, measurement='poles', cmap='Blues')
  ax.set_title(sondaje, y=1.05)
  ax.grid(False)

  
  
  posicion += 1
  strikes.clear()
  dips.clear()

fig.tight_layout() 

plt.show()