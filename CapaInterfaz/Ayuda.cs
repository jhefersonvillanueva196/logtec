﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaInterfaz
{
    public partial class Ayuda : Form
    {
        public string nombre;
        public Ayuda(string nombre)
        {
            InitializeComponent();
            cargarIMG(nombre);
        }

        public void cargarIMG(string nombre)
        {

            pbCondicionFractura.Visible = false;
            pbJrc.Visible = false;
            pbJr.Visible = false;
            pbJa.Visible = false;
            pbAlpha.Visible = false;
            pbBeta.Visible = false;
            pbRelleno1.Visible = false;
            pbRelleno2.Visible = false;
            if (pbCondicionFractura.Name.Equals(nombre))
            {
                this.Size = new Size(550, 309);
                pbCondicionFractura.Size = new Size(550, 309);
                pbCondicionFractura.Location = new Point(0, 0);
                pbCondicionFractura.Visible = true;
            }
            else if (pbJrc.Name.Equals(nombre))
            {
                this.Size = new Size(550, 309);
                pbJrc.Size = new Size(550, 309);
                pbJrc.Location = new Point(0, 0);
                pbJrc.Visible = true;
            }
            else if (pbJr.Name.Equals(nombre))
            {
                this.Size = new Size(550, 309);
                pbJr.Size = new Size(550, 309);
                pbJr.Location = new Point(0, 0);
                pbJr.Visible = true;
            }
            else if (pbJa.Name.Equals(nombre))
            {
                this.Size = new Size(550, 309);
                pbJa.Size = new Size(550, 309);
                pbJa.Location = new Point(0, 0);
                pbJa.Visible = true;
            }
            else if (pbAlpha.Name.Equals(nombre))
            {
                this.Size = new Size(550, 309);
                pbAlpha.Size = new Size(550, 309);
                pbAlpha.Location = new Point(0, 0);
                pbAlpha.Visible = true;
            }
            else if (pbBeta.Name.Equals(nombre))
            {
                this.Size = new Size(550, 309);
                pbBeta.Size = new Size(550, 309);
                pbBeta.Location = new Point(0, 0);
                pbBeta.Visible = true;
            }
            else if (pbRelleno1.Name.Equals(nombre))
            {
                this.Size = new Size(550, 309);
                pbRelleno1.Size = new Size(550, 309);
                pbRelleno1.Location = new Point(0, 0);
                pbRelleno1.Visible = true;
            }
            else if (pbRelleno2.Name.Equals(nombre))
            {
                this.Size = new Size(550, 309);
                pbRelleno2.Size = new Size(550, 309);
                pbRelleno2.Location = new Point(0, 0);
                pbRelleno2.Visible = true;
            }

        }

        private void Ayuda_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.Close();
        }

    }
}